import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

def IQR(x):
    N = len(x)
    y = np.sort(x)
    i1 = int(np.round(N/4))
    i3 = int(np.round(3*N/4))
    Q3 = y[i3]
    Q1 = y[i1]
    return Q3 - Q1

def Freedman_Diaconis_Width(x):
    return IQR(x) * 2 / np.power(len(x), 1/3)

def prepare_histograms(samples, N_bins=75, scale=1, weights=None, bins=None):
    N_obs = samples.shape[-1]
    hist1d = {}
    hist2d = {}

    if bins is None:
        bins = {}
        n = (N_bins-1)//2
        for i, s in enumerate(samples.T):
            bins[i] = (np.arange(-n, n) - .5) * Freedman_Diaconis_Width(s) * scale + np.median(s)

    for i, s in enumerate(samples.T):
        hist1d = np.histogram(s, bins=bins[i], density=True, weights=weights)

    for i in range(N_obs):
        for j in range(i, N_obs):
            hist2d[(i,j)] = np.histogram2d(samples[:, i], samples[:, j],
                bins=[bins[i], bins[j]], weights=weights)
            
    return {
        'bins': bins,
        'hist1d': hist1d,
        'hist2d': hist2d
    }

def shrink(b):
    d = np.median(np.diff(b))
    return b[:-1] + d/2

def std(x, weights=None, **kwargs):
    if weights is None:
        return np.std(x, **kwargs)
    else:
        μ = np.average(x, weights=weights, **kwargs)
        return np.sqrt(np.average((x-μ)**2, weights=weights, **kwargs))

def corner_comparison_plot(all_samples, N_obs=None, N_bins=200, bins=None,
                           axlabels=None, levels=None, runlabels=None, lims=None,
                           rotation_x=None, rotation_y=None, rotation=45, legend_kwargs=None,
                           weights=None, npix=2, scale=1
                           ):

    # consistency checks

    all_samples = [np.array(_) for _ in all_samples]
    assert np.all(np.equal(all_samples[0].shape[-1], [_.shape[-1] for _ in all_samples])), "Inconsistent number of sample dimensions"
    
    if N_obs is None:
        N_obs = all_samples[0].shape[-1]
    assert N_obs <= all_samples[0].shape[-1], "Incorrect number of dimensions"

    if axlabels is None:
        axlabels = [''] * N_obs

    if weights is None:
        weights = [None for _ in all_samples]

    assert len(axlabels) >= N_obs, "Insufficient number of labels for panels"

    if levels is None:
        levels = [1,2,3]

    bins_input = bins

    levels = np.exp(-(np.array(np.sort(levels)[::-1]))**2/2)

    f, ax = plt.subplots(N_obs, N_obs)

    if rotation_x is None:
        rotation_x = rotation
    if rotation_y is None:
        rotation_y = rotation

    for i in range(N_obs):
        for j in range(N_obs):
            plt.sca(ax[j][i])
            if i == 0 and j > 0:
                plt.ylabel(axlabels[j])
                for l in ax[j][i].get_yticklabels():
                    l.set_rotation(rotation_y)
            if j == N_obs-1:
                plt.xlabel(axlabels[i])
                for l in ax[j][i].get_xticklabels():
                    l.set_rotation(rotation_x)
            if i > 0 or j == 0:
                ax[j][i].set_yticks([])
            if j != N_obs-1:
                ax[j][i].set_xticks([])
            if j < i:
                ax[j][i].set_frame_on(False)

    handles = []

    for c, (samples, w) in enumerate(zip(all_samples, weights)):
        hists = prepare_histograms(samples, N_bins=N_bins, scale=scale, weights=w, bins=bins_input)
        bins = hists['bins']

        for i in range(N_obs):
            for j in range(N_obs):
                plt.sca(ax[j][i])
                if j < i:
                    continue
                if i == j:
                    h = plt.hist(samples[:,i], density=True,
                                 histtype='step', bins=bins[i], weights=w)
                    if i == 0:
                        handles.append(h)
                else:
                    density = (lambda x : x / np.max(x))(gaussian_filter(hists['hist2d'][(i,j)][0].T, npix))
                    plt.contour(shrink(bins[i]), shrink(bins[j]), density, levels, colors=f'C{c}', )

    if runlabels is not None:
        if legend_kwargs is None:
            legend_kwargs = {}
        legend_kwargs = dict(loc=(.765, .35)) | legend_kwargs
        f.legend(handles, labels=runlabels, **legend_kwargs)

    def set_lims(lims):
        for i in range(N_obs):
            for j in range(N_obs):
                plt.sca(ax[j][i])
                plt.xlim(*lims[i])
                if i != j:
                    plt.ylim(*lims[j])

    if lims is None:
        μμ = [np.average(_, weights=__, axis=0) for _, __ in zip(all_samples, weights)]
        σσ = [std(_, weights=__, axis=0) for _, __ in zip(all_samples, weights)]
        μ = np.mean(μμ, axis=0)
        σ = np.maximum(np.max(σσ, axis=0), np.std(μμ, axis=0))
        lims = np.array([μ - 5 * σ, μ + 5*σ]).T

    set_lims(lims)

    f.tight_layout(h_pad=0, w_pad=0)
    return f, ax
