import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

class ColorBarHelper:
    def __init__(self, arr, *args, log=False, **kwargs):
        self.cmap = plt.cm.get_cmap(*args, **kwargs)
        if not log:
            self.norm = mpl.colors.Normalize(np.min(arr), np.max(arr))
        else:
            self.norm = mpl.colors.LogNorm(np.min(arr), np.max(arr))
        self.sm = mpl.cm.ScalarMappable(cmap=self.cmap, norm=self.norm)
        self.sm.set_array([])

    def colorbar(self, *args, **kwargs):
        return plt.colorbar(self.sm, *args, **kwargs)

    def __call__(self, *args, **kwargs):
        # plt.gca()._sci(self.sm)
        return self.cmap(self.norm(*args, **kwargs))

def echelle(ν, l, Δν, e_ν=None, **kwargs):
    '''
    I make echelle diagrams so often that I might as well save time
    '''
    for ll in np.unique(l):
        m = (l == ll)
        if e_ν is not None:
            plt.errorbar(ν[m] % Δν, ν[m] - ν[m] % Δν, xerr=e_ν[m], yerr=e_ν[m],
                         **{'c': f'C{int(ll)}', 'fmt': '^', 'markerfacecolor': 'none', **kwargs})
        else:
            plt.plot(ν[m] % Δν, ν[m] - ν[m] % Δν, **{'marker': 'o', 'c': f'C{int(ll)}', 'ls': '', **kwargs})

def errorscatter(x, y, *args, xerr=None, yerr=None, c=None, fmt='none', zlabel=None, clb=None, cax=None,
                 cbar_kwargs=None, **kwargs):
    sc = plt.scatter(x, y, *args, c=c, **kwargs)
    if cbar_kwargs is None:
        cbar_kwargs = {}
    if clb is None:
        if cax is None:
            clb = plt.colorbar(sc, label=zlabel, **cbar_kwargs)
        else:
            clb = plt.colorbar(sc, label=zlabel, cax=cax, **cbar_kwargs)
    er = plt.errorbar(x, y, *args, xerr=xerr, yerr=yerr, fmt=fmt, c='black')
    [q.set_color(clb.cmap(clb.norm(np.array(c)))) for q in er[-1]]
    return clb, er

def cdf(x, x0, x1, ax=None, weights=None, **kwargs):
    if ax is None:
        ax = plt
    if weights is None:
        weights = np.ones_like(x)
    m = np.argsort(x)
    t = np.array(x)[m]
    w = weights[m] / np.sum(weights)
    return ax.step([x0, *t, x1], [0, *np.cumsum(w), 1], where='post', **kwargs)

