subroutine tpk_lookup(rho, E, n, T, P, Cp, grad_ad, xkappa, dtdr, dtde, dpdr, dpde)
        use eos
        implicit none

        integer :: i, n
        double precision, intent(in), dimension(n) :: rho, E
        double precision, intent(out), dimension(n) :: T, P, Cp, grad_ad, xkappa, dtdr, dtde, dpdr, dpde

        call eos_init()
        call eos_info()

        do i = 1,n
            call gasdat(rho(i), E(i), T(i), P(i), Cp(i), grad_ad(i), xkappa(i), dtdr(i), dtde(i), dpdr(i), dpde(i))
        end do
        return
end subroutine
