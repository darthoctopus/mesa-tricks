#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Wrapper for OPAL EOS (via provided TPK table) for Joel Tanner's RHD code.
TPK must be in the working directory for this to work.
'''

import numpy as np
from tpk import tpk_lookup

def tpk_eos(rho, E):
    _ = np.array
    assert _(rho).shape == _(E).shape
    shape = _(rho).shape
    r, e = rho.flatten(), E.flatten()

    res = tpk_lookup(r, e)
    names = ['T', 'P', 'C_p', 'grad_ad', 'xkappa', 'dtdr', 'dtde', 'dpdr', 'dpde']

    return {n: np.ascontiguousarray(__).reshape(shape) for n, __ in zip(names, res)}