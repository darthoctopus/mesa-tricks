module eos

  private

  public :: eos_init
  public :: eos_info
  public :: gasdat   ! calculates a variety of data from the EOS table
  public :: gasp     ! returns only the gas pressure.
  private :: perm, cuba, cubb, cubtwo

  ! Private variables for reading and storing the TPK.yck file.
  integer, parameter :: nrdim = 33
  integer, parameter :: nedim = 64
  integer, parameter :: nredim = 16 * nrdim * nedim
  integer :: ndens, nenerg
  real(kind=8), dimension(nedim) :: etablg
  real(kind=8), dimension(nrdim) :: rhotlg
  real(kind=8), dimension(nrdim,nedim) :: ttablg, ptablg, xktalg
  real(kind=8) :: ycdmin, ycdstep, ycemin, ycestep

  ! Private variables for setting up the splines.
  integer :: nr, ne, necell, nrcell, nrho, nrhom1, nen, nenm1, iparsp
  real(kind=8) :: dx, dy
  real(kind=8), dimension(nredim) :: bt, bp, bkap
  real(kind=8), dimension(nrdim,nedim) :: ddr, dde, d2drde

  contains

  !----------------------------------------------------------------------------!
  ! PUBLIC                                                                     !
  !----------------------------------------------------------------------------!


  subroutine eos_info()
    implicit none

  end subroutine eos_info


  subroutine eos_init()
    ! Initializes the equation of state module.  Reading the simulation
    ! EOS table (TPK.yck) is done only once at the start.
    implicit none
    integer :: l

    ! Read in the combined opacity and equation of state table.
    open(unit=2,file='TPK.yck',status='old',form='formatted')
    read(2,'(f10.2,e21.14,f10.2,e21.14,2i5)') ycdmin,ycdstep, ycemin,ycestep, ndens, nenerg
    if(ndens.ne.nrdim)stop 'nrdim'
    do l=1,nrdim
      rhotlg(l)=ycdmin+ycdstep*float(l-1)
    enddo 
    if(nenerg.ne.nedim)stop 'nedim'
    do l=1,nedim
      etablg(l)=ycemin+ycestep*float(l-1)
    enddo
    read(2,11)ttablg
    read(2,11)ptablg
    read(2,11)xktalg
    close(2)
    11 format(1x,6e13.6)

    iparsp=1

    ! Define table size.
    nrho = nrdim
    nen  = nedim
    nrhom1 = nrho - 1 
    nenm1  = nen - 1 

    ! Boundary values for temperature at density boundaries. 
    do ne = 1, nen 
      dx = rhotlg (2) - rhotlg (1) 
      dy = ttablg (2,ne) - ttablg (1,ne)
      ddr (1,ne) = dy / dx
      dx = rhotlg (nrho) - rhotlg (nrhom1)
      dy = ttablg (nrho,ne) - ttablg (nrhom1,ne)
      ddr (nrho,ne) = dy / dx
    enddo

    ! boundary values for temperature at energy boundaries.
    do nr = 1, nrho
      dx = etablg (2) - etablg (1) 
      dy = ttablg (nr,2) - ttablg (nr,1)
      dde (nr,1) = dy / dx
      dx = etablg (nen) - etablg (nenm1)
      dy = ttablg (nr,nen) - ttablg (nr,nenm1)
      dde (nr,nen) = dy / dx
    enddo

    ! Boundary values for temperature at corners of grid.
    d2drde(1,1)      = (ddr(1,2)      - ddr (1,1))        / (etablg(2)   - etablg(1))
    d2drde(nrho,1)   = (ddr(nrho,2)   - ddr(nrho,1))      / (etablg(2)   - etablg(1))
    d2drde(1,nen)    = (ddr(1,nen)    - ddr(1,nenm1))     / (etablg(nen) - etablg(nenm1))
    d2drde(nrho,nen) = (ddr(nrho,nen) - ddr (nrho,nenm1)) / (etablg(nen) - etablg(nenm1))

    ! compute spline interpolation coefficient matrix at.
    call cubtwo (iparsp, nrho, nen, rhotlg, etablg, ttablg, ddr, dde, d2drde, bt) 

    !  boundary values for pressure at density boundaries.
    do ne = 1, nen
      dx = rhotlg (2) - rhotlg (1)
      dy = ptablg (2,ne) - ptablg (1,ne)
      ddr (1,ne) = dy / dx
      dx = rhotlg (nrho) - rhotlg (nrhom1)
      dy = ptablg (nrho,ne) - ptablg (nrhom1,ne)
      ddr (nrho,ne) = dy / dx
    enddo

    ! boundary values for pressure at energy boundaries.
    do nr = 1, nrho
      dx = etablg (2) - etablg (1)
      dy = ptablg (nr,2) - ptablg (nr,1)
      dde (nr,1) = dy / dx
      dx = etablg (nen) - etablg (nenm1)
      dy = ptablg (nr,nen) - ptablg (nr,nenm1)
      dde (nr,nen) = dy / dx
    enddo

    ! boundary values for pressure at corners of grid.
    d2drde(1,1)      = (ddr(1,2)      - ddr(1,1))        / (etablg(2)   - etablg(1))
    d2drde(nrho,1)   = (ddr(nrho,2)   - ddr(nrho,1))     / (etablg(2)   - etablg(1))
    d2drde(1,nen)    = (ddr(1,nen)    - ddr(1,nenm1))    / (etablg(nen) - etablg(nenm1))
    d2drde(nrho,nen) = (ddr(nrho,nen) - ddr(nrho,nenm1)) / (etablg(nen) - etablg(nenm1))

    ! compute spline interpolation coefficient matrix ap.
    call cubtwo (iparsp, nrho, nen, rhotlg, etablg, ptablg, ddr, dde, d2drde, bp)

    ! boundary values for opacity at density boundaries.
    do ne = 1, nen
      dx = rhotlg (2) - rhotlg (1)
      dy = xktalg (2,ne) - xktalg (1,ne)
      ddr (1,ne) = dy / dx
      dx = rhotlg (nrho) - rhotlg (nrhom1)
      dy = xktalg (nrho,ne) - xktalg (nrhom1,ne)
      ddr (nrho,ne) = dy / dx
    enddo

    ! boundary values for opacity at energy boundaries.
    do nr = 1, nrho
      dx = etablg (2) - etablg (1)
      dy = xktalg (nr,2) - xktalg (nr,1)
      dde (nr,1) = dy / dx
      dx = etablg (nen) - etablg (nenm1)
      dy = xktalg (nr,nen) - xktalg (nr,nenm1)
      dde (nr,nen) = dy / dx
    enddo

    ! boundary values for opacity at corners of grid.
    d2drde(1,1)      = (ddr(1,2)      - ddr(1,1))        / (etablg(2)   - etablg(1))
    d2drde(nrho,1)   = (ddr(nrho,2)   - ddr(nrho,1))     / (etablg(2)   - etablg(1))
    d2drde(1,nen)    = (ddr(1,nen)    - ddr(1,nenm1))    / (etablg(nen) - etablg(nenm1))
    d2drde(nrho,nen) = (ddr(nrho,nen) - ddr(nrho,nenm1)) / (etablg(nen) - etablg(nenm1))

    ! compute spline interpolation coefficient matrix akap.
    call cubtwo (iparsp, nrho, nen, rhotlg, etablg, xktalg, ddr, dde, d2drde, bkap)
   
    return
  end subroutine eos_init


  function gasp(density, energy)
    real(kind=8), intent(in) :: density
    real(kind=8), intent(in) :: energy
    real(kind=8) :: gasp
    real(kind=8) :: temp, press, cp, gradad, xkappa, dtdr, dtde, dpdr, dpde
    call gasdat( density, energy, temp, press, cp, gradad, xkappa &
                     , dtdr, dtde, dpdr, dpde )
    gasp = press
  end function gasp


  subroutine gasdat( rho, energy, temp, press, cp, gradad, xkappa &
                   , dtdr, dtde, dpdr, dpde )
    implicit none
    real(kind=8) :: rho, energy, temp, press, cp, gradad, xkappa
    real(kind=8) :: dtdr, dtde, dpdr, dpde
    integer :: i, j, k, l
    integer :: kp1,kp2,kp3,kp4,kp5,kp6,kp7,kp8,kp9
    integer :: kp10,kp11,kp12,kp13,kp14,kp15,kp16
    real(kind=8) :: rholg, enerlg, eorho
    real(kind=8) :: tempt, presst, xkappt
    real(kind=8) :: dtd1, dtd2, dpd1, dpd2, dddd, ddddtr


    ! convert energy density to energy per unit mass, and convert input
    ! data to logarithmic scale.
    eorho=energy/rho
    rholg = log(rho)
    enerlg = log(eorho)


      ! find grid cell in which data point is located
      nrcell = 1
      do 110 nr = 2, nrho
         if (rholg .ge. rhotlg (nr)) goto 110
         nrcell = nr - 1
         goto 120
  110 continue 
      nrcell = nrho - 1
  
  120 necell = 1
      do 130 ne = 2, nen 
         if (enerlg .ge. etablg (ne)) goto 130
         necell = ne - 1
         goto 200
  130 continue 
      necell = nen - 1

    ! Compute values.
200 k    = 16 * (nrdim * (necell-1) + nrcell-1)
    kp1  = k + 1
    kp2  = k + 2
    kp3  = k + 3
    kp4  = k + 4
    kp5  = k + 5
    kp6  = k + 6
    kp7  = k + 7
    kp8  = k + 8
    kp9  = k + 9
    kp10 = k + 10
    kp11 = k + 11
    kp12 = k + 12
    kp13 = k + 13
    kp14 = k + 14
    kp15 = k + 15
    kp16 = k + 16

    if( (rhotlg(1).gt.rholg)  .or. (rholg.gt.rhotlg(nrho)) .or.   &
        (etablg(1).gt.enerlg) .or. (enerlg.gt.etablg(nen)) ) then
      !print*, 'GASDAT: warning! out of table'
      !if (rhotlg(1) .gt. rholg)    print*, 'density too low' 
      !if (rholg .gt. rhotlg(nrho)) print*, 'density too high' 
      !if (etablg(1) .gt. enerlg)   print*, 'energy too low' 
      !if (enerlg .gt. etablg(nen)) print*, 'energy too high'
      !if (rhotlg(1) .gt. rholg) then
      !  rho = exp(rhotlg(1))*1.05
      !  rholg = log(rho)
      !endif
      !if (rholg .gt. rhotlg(nrho)) then
      !  rho = exp(rhotlg(nrho))*0.95
      !  rholg = log(rho)
      !endif
      !if (etablg(1) .gt. enerlg) then
      !  energy = exp(etablg(1))*rho*1.05
      !  enerlg = log(energy/rho)
      !endif
      !if (enerlg .gt. etablg(nen)) then
      !  energy = exp(etablg(nen))*rho*0.95
      !  enerlg = log(energy/rho)
      !endif
      write(6,221) 'GASDAT: Error. Out of table.'
      !write(6,222) 'Density Range:   ', rhotlg(1), rholg, rhotlg(nrho)
      !write(6,222) 'Energy Range:    ', etablg(1), enerlg, etablg(nen)
      write(6,222) 'Density Range:   ', exp(rhotlg(1)), exp(rholg), exp(rhotlg(nrho))
      write(6,222) 'Energy Range:    ', exp(etablg(1)), exp(enerlg), exp(etablg(nen))
      write(6,220) 'Density Cell: ', nrcell, rhotlg(nrcell),rholg,rhotlg(nrcell+1)
      write(6,220) 'Energy Cell:  ', necell, etablg(necell),enerlg,etablg(necell+1)
      stop 'GASDAT: out of table ... crash'
    endif
    220 format ( A, I3.3, 2X, E14.6, 2X, E14.6, 2X, E14.6 )
    221 format ( A, 2X, I3.3, 1X, I3.3, 1X, I3.3 )
    222 format ( A, 2X, E14.6, 2X, E14.6, 2X, E14.6 )
    dx = rholg - rhotlg (nrcell)
    dy = enerlg - etablg (necell)

    tempt = (     bt(kp1)  + dx*(bt(kp2)  + dx*(bt(kp3)  + dx*bt(kp4)))     &
            + dy*(bt(kp5)  + dx*(bt(kp6)  + dx*(bt(kp7)  + dx*bt(kp8)))     &
            + dy*(bt(kp9)  + dx*(bt(kp10) + dx*(bt(kp11) + dx*bt(kp12)))    &
            + dy*(bt(kp13) + dx*(bt(kp14) + dx*(bt(kp15) + dx*bt(kp16)))))) )
    temp = exp(tempt)

    presst = (     bp(kp1)  + dx*(bp(kp2)  + dx*(bp(kp3)  + dx*bp(kp4)))     &
             + dy*(bp(kp5)  + dx*(bp(kp6)  + dx*(bp(kp7)  + dx*bp(kp8)))     &
             + dy*(bp(kp9)  + dx*(bp(kp10) + dx*(bp(kp11) + dx*bp(kp12)))    &
             + dy*(bp(kp13) + dx*(bp(kp14) + dx*(bp(kp15) + dx*bp(kp16)))))) )
    press = exp(presst)

    xkappt = (     bkap(kp1)  + dx*(bkap(kp2)  + dx*(bkap(kp3)  + dx*bkap(kp4)))     &
             + dy*(bkap(kp5)  + dx*(bkap(kp6)  + dx*(bkap(kp7)  + dx*bkap(kp8)))     &
             + dy*(bkap(kp9)  + dx*(bkap(kp10) + dx*(bkap(kp11) + dx*bkap(kp12)))    &
             + dy*(bkap(kp13) + dx*(bkap(kp14) + dx*(bkap(kp15) + dx*bkap(kp16)))))) )
    xkappa = exp(xkappt)

    dtd1 = ( bt(kp2) + dy*(bt(kp6) + dy*(bt(kp10) + dy*bt(kp14)))           &
           + 2.0*dx*(bt(kp3) + dy*(bt(kp7) + dy*(bt(kp11) + dy*bt(kp15)))   &
           + 1.5*dx*(bt(kp4) + dy*(bt(kp8) + dy*(bt(kp12) + dy*bt(kp16))))) )

    dtd2 = ( bt(kp5) + dx*(bt(kp6) + dx*(bt(kp7) + dx*bt(kp8)))               &
           + 2.0*dy*(bt(kp9) + dx*(bt(kp10) + dx*(bt(kp11) + dx*bt(kp12)))    &
           + 1.5*dy*(bt(kp13) + dx*(bt(kp14) + dx*(bt(kp15) + dx*bt(kp16))))) )

    dpd1 = ( bp(kp2) + dy*(bp(kp6) + dy*(bp(kp10) + dy*bp(kp14)))         & 
         + 2.0*dx*(bp(kp3) + dy*(bp(kp7) + dy*(bp(kp11) + dy*bp(kp15)))   &
         + 1.5*dx*(bp(kp4) + dy*(bp(kp8) + dy*(bp(kp12) + dy*bp(kp16))))) )

    dpd2 = ( bp(kp5) + dx*(bp(kp6) + dx*(bp(kp7) + dx*bp(kp8))) +             &
             2.0*dy*(bp(kp9) + dx*(bp(kp10) + dx*(bp(kp11) + dx*bp(kp12))) +  &
             1.5*dy*(bp(kp13) + dx*(bp(kp14) + dx*(bp(kp15) + dx*bp(kp16))))) )

    dtdr = (dtd1-dtd2)*temp/rho
    dtde = dtd2*temp/energy
    dpdr = (dpd1-dpd2)*press/rho
    dpde = dpd2*press/energy
    dddd = dtde * dpdr - dtdr*dpde
    ddddtr = dddd + temp/rho*dpde*dpde
    cp = ddddtr / (dddd * rho * dtde)
    gradad = press/rho*dtde*dpde/ddddtr

    return
  end subroutine gasdat

!  subroutine eos_pressure(d, e, vz00h, vxh00, vy0h0, p)
!    implicit none
!    real(kind=8), intent(in) :: 
!
!  end subroutine eos_pressure


  !----------------------------------------------------------------------------!
  ! PRIVATE                                                                    !
  !----------------------------------------------------------------------------!


      subroutine perm (intmod, ivj, m, m1, n, n1, p, u, dx, zx)
!c***********************************************************************
!c
!c    ==================================
!c    last update:  13-jun-1989 05:04:53 
!c    ==================================
!c
!c - reads in u-values in a row along which spline-interpolation
!c   shall be performed
!c
!c***********************************************************************
!c
!c      parameter (idim = 33, jdim = 64, mdim = 64)
!cc
!c      dimension p(idim,jdim), u(idim,jdim), ord(mdim), unb(mdim)
!c      dimension dx(1), zx(1)
!c
      implicit none
      integer idim
      integer jdim
      integer mdim
      parameter (idim = 33)
      parameter (jdim = 64)
      parameter (mdim = 64)
      integer intmod
      integer ivj
      integer m
      integer m1
      integer n
      integer n1
      real*8 p(idim,jdim)
      real*8 u(idim,jdim)
      real*8 ord(mdim)
      real*8 unb(mdim)
      real*8 dx(idim)
      real*8 zx(idim)
      integer i, j

      do 4 j=1,m,m1
      do 2 i=1,n
!c
!c - ivj=0  read in x-direction
!c   ivj=1  read in y-direction
!c
      if(ivj.ne.0) goto 1
!c
      ord(i)=u(i,j)
      unb(i)=p(i,j)
      goto 2
!c
    1 ord(i)=u(j,i)
      unb(i)=p(j,i)
    2 continue
!c
!c - using auxiliary vector zx and known boundary values of unb,
!c   now solve interpolation problem
!c
      call cubb(intmod,n1,dx,ord,unb,zx)
!c
      do 3 i=2,n1
!c
!c - assign solution unb to one row of array p(i,j)
!c
      if(ivj.eq.0) p(i,j)=unb(i)
      if(ivj.ne.0) p(j,i)=unb(i)
!c
    3 continue
    4 continue
!c
      return
      end subroutine perm


      subroutine cuba(n1,dx,z)
!c*******************************************************************
!c
!c - cuba creates auxiliary vector z(i) needed by cubb
!c
!c      dimension dx(1), z(1)
!c
      implicit none
      integer idim
      integer jdim
      parameter (idim = 33, jdim = 64)
      integer n1
      real*8 dx(idim)
      real*8 z(idim)
      integer j1
      real*8 h1, h2
      integer k

      z(1)=0.
      j1=1
      h1=dx(1)
!c
      do 1 k=2,n1
      h2=dx(k)
      z(k)=1./(2.*(h1+h2)-h1*h1*z(j1))
      j1=k
      h1=h2
    1 continue
!c
      return
      end subroutine cuba


      subroutine cubb (intmod, n1, dx, y, y1, z)
!c***********************************************************************
!c
!c    ==================================
!c    last update:  18-jun-1989 11:55:43 
!c    ==================================
!c
!c    cubb solves a one-dimensional spline interpolation problem for
!c    a data set with n = (n1+1) data points. in general auxilary vector
!c    z previously computed by cuba is required. the subroutines returns
!c    the derivatives y1 = d(y)/d(x) for all inner points. derivatives
!c    at boundaries y1(1) and y1(n) have to be computed before and passed
!c    to this subroutine.
!c
!c    parameter intmod determines method of computing derivatives.
!c    intmod = 0:   from all data values (general method)
!c    intmod = 1:   y1(i) assumed equal to inclination of secant passing
!c                  through data points (i-1) and (i+1)
!c    intmod = 2:   y1(i) assumed equal to inclination of parabola
!c                  passing through data points (i-1), i, and (i+1).
!c                  [ results are identical to those of method (1) if
!c                    grid spacing dx(i) is constant. ]
!c
!c***********************************************************************
!c
!c
!c      dimension dx(1), y(1), y1(1), z(1)
!c
      implicit none
      integer idim
      integer jdim
      integer mdim
      parameter (idim = 33)
      parameter (jdim = 64)
      parameter (mdim = 64)
      integer intmod
      integer n1
      real*8 dx(idim)
      real*8 y(mdim)
      real*8 y1(mdim)
      real*8 z(idim)

      real*8 h1, h2
      real*8 r1, r2
      real*8 dx1, dx2
      real*8 dy1, dy2
      real*8 dd
      real*8 f1, f2
      integer n2, j1, k

!c--- select method of computing derivatives y1 ---
      if (intmod .eq. 1) goto 10
      if (intmod .eq. 2) goto 20
!c
!c
!c=======================================================================
!c--- derivatives computed from all spline coefficients ---
!c=======================================================================
!c
!c--- check whether at least three grid point are present ---
      if (n1 .ge. 3) goto 100
      if (n1 .eq. 2) goto 50
      return
!c
!c--- only three grid points present ---
   50 h1 = dx(1)
      h2 = dx(2)
      r1 = 3.0 * h1 * h1 * ( y(2) - y(1) )
      r2 = 3.0 * h2 * h2 * ( y(3) - y(2) )
      y1(2) = z(2) * ( r1 + r2 - h1 * y1(1) - h2 * y1(3) )
      return
!c
!c--- four or more grid points present ---
  100 n2 = n1 - 1
      h1 = dx(1)
      r1 = 3.0 * h1 * h1 * ( y(2) - y(1) )
      j1 = 1
!c
!c--- forward substitution employing auxilary vector z ---
      do 102 k=2,n2
         h2 = dx(k)
         r2 = 3.0 * h2 * h2 * ( y(k+1) - y(k) )
         y1(k) = z(k) * ( r1 + r2 - h1 * y1(j1) )
         j1 = k
         h1 = h2
         r1 = r2
  102 continue
      h2 = dx(n1)
      r2 = 3.0 * h2 * h2 * ( y(n1+1) - y(n1) )
      y1(n1) = z(n1) * ( r1 + r2 - h1 * y1(j1) - h2 * y1(n1+1) )
!c
!c--- backward solution for derivatives y1 ---
      do 103 k=n2,2,-1
         y1(k) = y1(k) - z(k) * dx(k) * y1(k+1)
  103 continue
      return
!c
!c
!c=======================================================================
!c--- derivatives assumed equal to inclination of secant ---
!c=======================================================================
!c
!c--- check whether at least three grid points are present ---
   10 if (n1 .lt. 2) return

!c--- compute derivatives y1 ---
      do 15 k=2,n1
         y1(k) = ( y(k+1) - y(k-1) ) / ( 1.0/dx(k) + 1.0/dx(k-1) )
   15 continue
      return
!c
!c
!c=======================================================================
!c--- derivatives assumed equal to those of fitting parabola ---
!c=======================================================================
!c
!c--- check whether at leasr three grid points are present ---
   20 if (n1 .lt. 2) return
!c
!c--- compute derivatives y1 ---
      do 25 k=2,n1
         dx1 = 1.0 / dx(k-1)
         dx2 = 1.0 / dx(k)
         dd = dx1 + dx2
         f1 = dx2 / dd
         f2 = dx1 / dd
         dy1 = y(k) - y(k-1)
         dy2 = y(k+1) - y(k)
         y1(k) = f1 * dy1 / dx1 + f2 * dy2 / dx2
   25 continue
!c
      return
      end subroutine cubb
!c


      subroutine cubtwo (intmod, n, m, x, y, u, p, q, r, a)
!c***********************************************************************
!c
!c    ==================================
!c    last update:  13-jun-1989 04:57:53 
!c    ==================================
!c
!c---cubtwo is a subroutine for two-dimensional cubic spline-
!c---interpolation of a function f(x,y).
!c---for details see: h. spaeth, spline-algorithmen
!c---                 oldenbourg-vlg., 2.aufl. 1978
!c
!c - n: number of grid points in x direction
!c - m: number of grid points in y direction
!c - x(i): x-coordinate of point pgrid(i,j)
!c - y(j): y-coordinate of point pgrid(i,j)
!c - u(i,j) = f(x(i),y(j))
!c - p(i,j) = df(x(i),y(j))/dx
!c - q(i,j) = df(x(i),y(j))/dy
!c - r(i,j) = d2f(x(i),y(j))/dxdy
!c - a(k1,k2,i,j): spline-coefficients in grid cell (i,j)
!c
!c - cubtwo needs u(i,j) at all grid points, p(i,j) at
!c   i=1 and i=n, q(i,j) at j=1 and j=m, r(i,j) at corners
!c   (1,1), (1,m), (n,1), (n,m).
!c - to obtain boundary values for p, q, and r, previous
!c - call of intkof is required
!c
!c - formula for a(k1,k2,i,j):
!c   a(k1,k2,i,j)=b(dx)*c(u,p,q,r)*trans(e(dy))
!c
!c - cray version, december 1985
!c
!c***********************************************************************
!c
!c      parameter (idim = 33, jdim = 64)
!c
!c      dimension x(idim), y(jdim)
!c      dimension u(idim,jdim), p(idim,jdim), q(idim,jdim), r(idim,jdim)
!c      dimension a(4,4,idim,jdim), dx(idim), dy(jdim), zx(idim), zy(jdim)
!c
      implicit none
      integer idim
      integer jdim
      parameter (idim = 33, jdim = 64)
      integer intmod
      integer n
      integer m
      real*8 x(idim)
      real*8 y(jdim)
      real*8 u(idim,jdim)
      real*8 p(idim,jdim)
      real*8 q(idim,jdim)
      real*8 r(idim,jdim)
      real*8 a(4,4,idim,jdim)

      integer n1, m1
      integer i, j
      real*8 dx(idim)
      real*8 dy(jdim)
      real*8 zx(idim)
      real*8 zy(jdim)
      real*8 b11, b12, b13, b14
      real*8 b21, b22, b23, b24
      real*8 b31, b32, b33, b34
      real*8 b41, b42, b43, b44
      real*8 e11, e12, e13, e14
      real*8 e21, e22, e23, e24
      real*8 e31, e32, e33, e34
      real*8 e41, e42, e43, e44
      real*8 c11, c12, c13, c14
      real*8 c21, c22, c23, c24
      real*8 c31, c32, c33, c34
      real*8 c41, c42, c43, c44
      real*8 d11, d12, d13, d14
      real*8 d21, d22, d23, d24
      real*8 d31, d32, d33, d34
      real*8 d41, d42, d43, d44
      integer ivj, i1, j1
      real*8 h

      n1=n-1
      m1=m-1
!c
!c - compute dx(i)
!c
      do 1 i=1,n1
      dx(i)=1.0/(x(i+1)-x(i))
    1 continue
!c
!c - compute dy(j)
!c
      do 2 j=1,m1
      dy(j)=1.0/(y(j+1)-y(j))
    2 continue
!c
!c - set up fixed upper half of matrices b and e
!c
      b11=1.0
      b12=0.0
      b13=0.0
      b14=0.0
      b21=0.0
      b22=1.0
      b23=0.0
      b24=0.0
      e11=1.0
      e12=0.0
      e13=0.0
      e14=0.0
      e21=0.0
      e22=1.0
      e23=0.0
      e24=0.0
!c
!c
!c - now compute matrix c
!c   ivj=0 computation in x-direction
!c   ivj=1 computation in y-direction
!c
      if(n.gt.2 .and. m.gt.2) goto 5
      write(6,10)
   10 format(//1x,44hcubtwo: m or n .le. 2 , aborting program ]]]/)
      stop 'cubtwo e'
!c
!c - prepare vector zx
!c
    5 if(intmod.eq.0)call cuba(n1,dx,zx)
!c
!c - compute p(i,j) at inner points
!c
      ivj=0
      call perm(intmod,ivj,m,1,n,n1,p,u,dx,zx)
!c
!c - prepare vector zy
      if(intmod.eq.0)call cuba(m1,dy,zy)
!c
!c - compute q(i,j) at inner points
!c
      ivj=1
      call perm(intmod,ivj,n,1,m,m1,q,u,dy,zy)
!c
!c
!c - compute r(i,1) and r(i,m)
!c
      ivj=0
      call perm(intmod,ivj,m,m1,n,n1,r,q,dx,zx)
!c
!c
!c - compute r(i,j) at inner points
!c
      ivj=1
      call perm(intmod,ivj,n,1,m,m1,r,p,dy,zy)
!c
!c
      do 16 i=1,n1
      i1=i+1
!c
!c - complete matrix b
!c
      h=dx(i)
!c
      b31=-3.0*h*h
      b32=-2.0*h
      b33= 3.0*h*h
      b34=-h
      b41= 2.0*h*h*h
      b42= h*h
      b43=-2.0*h*h*h
      b44= h*h
!c
      do 15 j=1,m1
      j1=j+1
!c
!c - complete matrix e
!c
      h=dy(j)
!c
      e31=-3.0*h*h
      e32=-2.0*h
      e33= 3.0*h*h
      e34=-h
      e41= 2.0*h*h*h
      e42= h*h
      e43=-2.0*h*h*h
      e44= h*h
!c
!c - set up matrix c
!c
      c11=u(i,j)
      c12=q(i,j)
      c21=p(i,j)
      c22=r(i,j)
      c13=u(i,j1)
      c14=q(i,j1)
      c23=p(i,j1)
      c24=r(i,j1)
      c31=u(i1,j)
      c32=q(i1,j)
      c41=p(i1,j)
      c42=r(i1,j)
      c33=u(i1,j1)
      c34=q(i1,j1)
      c43=p(i1,j1)
      c44=r(i1,j1)
!c
!c - multiply matrices b and c
!c
      d11=b11*c11+b12*c21+b13*c31+b14*c41
      d21=b21*c11+b22*c21+b23*c31+b24*c41
      d31=b31*c11+b32*c21+b33*c31+b34*c41
      d41=b41*c11+b42*c21+b43*c31+b44*c41
      d12=b11*c12+b12*c22+b13*c32+b14*c42
      d22=b21*c12+b22*c22+b23*c32+b24*c42
      d32=b31*c12+b32*c22+b33*c32+b34*c42
      d42=b41*c12+b42*c22+b43*c32+b44*c42
      d13=b11*c13+b12*c23+b13*c33+b14*c43
      d23=b21*c13+b22*c23+b23*c33+b24*c43
      d33=b31*c13+b32*c23+b33*c33+b34*c43
      d43=b41*c13+b42*c23+b43*c33+b44*c43
      d14=b11*c14+b12*c24+b13*c34+b14*c44
      d24=b21*c14+b22*c24+b23*c34+b24*c44
      d34=b31*c14+b32*c24+b33*c34+b34*c44
      d44=b41*c14+b42*c24+b43*c34+b44*c44
!c
!c - compute spline coefficient a(k1,k2,i,j)
!c
!c - multiply matrix d=(b*c) with transverse of matrix e
!c
!c
      a(1,1,i,j)=d11*e11+d12*e12+d13*e13+d14*e14
      a(2,1,i,j)=d21*e11+d22*e12+d23*e13+d24*e14
      a(3,1,i,j)=d31*e11+d32*e12+d33*e13+d34*e14
      a(4,1,i,j)=d41*e11+d42*e12+d43*e13+d44*e14
      a(1,2,i,j)=d11*e21+d12*e22+d13*e23+d14*e24
      a(2,2,i,j)=d21*e21+d22*e22+d23*e23+d24*e24
      a(3,2,i,j)=d31*e21+d32*e22+d33*e23+d34*e24
      a(4,2,i,j)=d41*e21+d42*e22+d43*e23+d44*e24
      a(1,3,i,j)=d11*e31+d12*e32+d13*e33+d14*e34
      a(2,3,i,j)=d21*e31+d22*e32+d23*e33+d24*e34
      a(3,3,i,j)=d31*e31+d32*e32+d33*e33+d34*e34
      a(4,3,i,j)=d41*e31+d42*e32+d43*e33+d44*e34
      a(1,4,i,j)=d11*e41+d12*e42+d13*e43+d14*e44
      a(2,4,i,j)=d21*e41+d22*e42+d23*e43+d24*e44
      a(3,4,i,j)=d31*e41+d32*e42+d33*e43+d34*e44
      a(4,4,i,j)=d41*e41+d42*e42+d43*e43+d44*e44
!c
   15 continue
   16 continue
!c
      return
      end subroutine cubtwo

end module eos
