#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Convenience functions for manipulating hydro-sim snapshots
produced by Joel Tanner's RHD code

NOTE THAT ORDER OF OPERATIONS MATTERS: e.g. computing Γ1 from horizontal
averages (instead of pointwise before horizontally averaging) gives highly
unphysical values (>2). All of the following functions are intended to return
pointwise (i.e. 3D) estimates of various thermodynamic quantities than can then be
safely horizontally averaged.

I've actually written these in such a way that they can also be applied to 1D
arrays. However, I still maintain that doing so is not recommended.
'''

import numpy as np
from .io import h_avg

def Pturb(hydro):
    '''
    Turbulent pressure ~ ρ⟨v_z²⟩
    '''
    return hydro['rho'] * ((hydro['vz'] - h_avg(hydro['vz']))**2)

def Thermo_Jac(hydro):
    r'''
    Jacobian determinant that appears in the below quantities since all of the EOS
    quantities in the OPAL table are with respect to ρ, e basis.

    This is the Jacobian $\partial (P, T) / \partial(ρ, e)$.
    Accordingly, it has units (P T / ρ e), which is useful for checking the
    dimensional consistency of some of the expressions below.
    '''
    return hydro['dpdr'] * hydro['dtde'] - hydro['dpde'] * hydro['dtdr']    

def Γ1(hydro):
    r'''
    Γ1 = (partial log P / partial log rho) at constant entropy.

    Typeset:
    $$\Gamma_1 = {c_p \over c_v} \left.{\partial \ln P \over \partial \ln \rho}\right|_{T}
    = {\rho \over P} c_p \left[\left.{\partial P\over \partial \rho}\right|_{e}
    \left.{\partial T\over \partial e}\right|_{\rho} -
    \left.{\partial P\over \partial e}\right|_{\rho}
    \left.{\partial T\over \partial \rho}\right|_{e}\right].$$
    Note that the TPK table returns C_p (i.e. per unit mass) = c_p / ρ.
    '''
    return hydro['rho']**2 / hydro['P'] * hydro['C_p'] * Thermo_Jac(hydro)

def grad(hydro, z, INCLUDE_PTURB=1):
    r'''
    Superadiabatic temperature gradient:
    \nabla = d (log T) / d (log P).

    Typeset:
    $$\nabla = {P \over T}{\partial T \over \partial z}/{\partial P \over \partial z}$$
    '''
    return ((hydro['P'] + Pturb(hydro) * INCLUDE_PTURB) / hydro['T'] *
            np.gradient(hydro['T'], z, axis=-1) / np.gradient(hydro['P'], z, axis=-1))

def N2(hydro, g, z, INCLUDE_PTURB=1):
    r'''
    Brunt-Väisälä Frequency:
    N^2 = g ((d log P/dr)/Γ1 - (d log ρ / dr))

    Typeset:
    $$N^2 = g \left({1 \over \Gamma_1}{d \log P \over d z}- {d \log \rho \over d z}\right)$$
    '''
    return g * (np.gradient(hydro['P'] + Pturb(hydro) * INCLUDE_PTURB, z, axis=-1)
        /(Γ1(hydro) * hydro['P']) - np.gradient(hydro['rho'], z, axis=-1) / hydro['rho'])

def δ(hydro):
    r'''
    Equation of state quantity:
    partial log ρ / partial log T at constant P.

    Typeset:
    $$\delta = - {T \over \rho} \left.{\partial \rho \over \partial T}\right|_P
    = - {T \over \rho} \left.\left.{\partial P \over \partial E}\right|_\rho\right/
    \left(\left.{\partial T \over \partial \rho}\right|_E
    \left.{\partial P \over \partial E}\right|_\rho -
    \left.{\partial T \over \partial E}\right|_\rho
    \left.{\partial P \over \partial \rho}\right|_E\right)$$
    '''
    return hydro['T'] / hydro['rho'] * hydro['dpde'] / Thermo_Jac(hydro)

def e_k(hydro):
    '''
    Kinetic energy density in the turbulent velocity field: need to subtract this
    from the total energy in the hydro snapshot to obtain the gas energy density,
    which is what the TPK table needs.
    '''
    return 1/2 * hydro['rho'] * (hydro['vx']**2 + hydro['vy']**2 + hydro['vz']**2)
