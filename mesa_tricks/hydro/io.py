#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
I/O functions for manipulating hydro-sim snapshots
produced by Joel Tanner's RHD code
'''

import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits

NAMES = ['vmx', 'vmy', 'vmz', 'erg', 'rho', 'kap']

def read_atm(ID, pattern=None):
    if pattern is None:
        pattern = "{dir}/grid_{name}_{ID}.fits"
    data = {}
    for name in NAMES:
        fname = pattern.format(name=name, ID=ID)
        data[name] = fits.getdata(fname).transpose(2,1,0)
    h = fits.getheader(fname)
    return h, to_cgs(data, h)

def to_cgs(data, h):
    # Zone-centered velocities, scaled to cgs units
    for i, k in enumerate(['x', 'y', 'z']):
        data[f'v{k}'] = (np.roll(data[f'vm{k}'], 1, i) + data[f'vm{k}']) / 2 / data['rho'] * h['vscl']
    
    # cgs units for other quantities
    data['rho'] = data['rho'] * h['dscl'] / h['dmin']
    data['erg'] = data['erg'] * h['pscl'] / h['dmin']
    return data

def gen_z(hydro, h):
    '''
    Construct array of z-axis coordinates given data and FITS header
    '''
    if type(hydro) ==  np.ndarray:
    # NOTE: we don't want to check parent class here.
    # in particular `astropy.io.fits.fitsrec.FITS_rec` is a subclass of ndarray
        shape = hydro.shape
    else:
        shape = hydro[hydro.names[0]].shape
    return np.arange(shape[-1]) * h['ZSCL'] * h['DMIN']

def h_avg(f):
    '''
    Horizontal average (to project to 1D)
    '''
    return np.mean(f, axis=(0, 1))