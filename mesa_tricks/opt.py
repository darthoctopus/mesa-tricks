#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Convenience functions for optimisation
'''

import numpy as np
import scipy.interpolate as inter
from scipy.optimize import least_squares
from scipy.signal import argrelextrema

from .utils import validate

# Actual optimisation

def Gauss_Newton(fun, x0, **kwargs):
    """Stateful Gauss-Newton Solver: order of functional
    evaluation is arranged so that last function call leaves
    the `tracks` directory populated with the MESA configuration
    and simulation history/profile files associated with
    formal minimal costs.

    As far as possible I've tried to replicate the output interfaces
    of e.g. scipy.optimize.least_squares.

    One could of course use least_squares without modification
    but empirically it seems setting an absolute rather than relative
    diff_step works better for this problem.
    """
    x = np.array(x0)
    maxiter = kwargs.pop('maxiter', 100)
    D = np.array([kwargs.pop('diff_step', 1e-3)] * len(x0))
    alpha = kwargs.pop('alpha', 1)
    atol = kwargs.pop('atol', 1e-10)

    c = fun(x)
    best = 1/2 * c @ c
    bestres = c
    bestx = x

    for i in range(maxiter):

        # displacement vectors
        V = np.diag(D)

        #Jacobian
        DC = np.array([fun(x+v) - c for v in V])
        J = (np.linalg.pinv(V) @ DC).T
        print(J)
        x = x - alpha * np.linalg.pinv(J) @ c

        c = fun(x)
        if 1/2 * c @ c < best:
            best = 1/2 * c @ c
            bestx = x
            bestres = c

        if best < atol:
            break

    if 1/2 * c @ c > best:
        fun(bestx)

    return {'x': bestx, 'fun': bestres, 'jac': J, 'nfev': i+1}

# Interpreting the output

def var_cov(J, w=None):
    '''
    Compute a variance-covariance matrix from a Gauss-Newton
    or Levenberg-Marquadt Jacobian matrix.
    '''
    if w is None:
        w = np.identity(len(J))

    # Here w is the diagonal matrix containing weights
    # (e.g. 1/σ^2 for uncorrelated measurement errors)
    # The variance-covariance matrix is H^-1, and H = J^T J
    # where J is the Gauss-Newton modified Jacobian
    return np.linalg.inv(J.T @ w @ J)

def Jac_stderr(Jac, W=None):
    '''
    Estimate standard errors from the variance covariance matrix
    '''
    return np.sqrt(np.diagonal(var_cov(Jac, W)))

def Δν_fit(ν, n, w=None, σ_ν=None):
    '''
    Estimate Δν from least-squares fitting of ν against n (assuming same l)

    '''

    _ = np.array
    validate(ν, n, w=w, σ_ν=σ_ν)

    def cost(p):
        Δν, α = p
        C = _(ν) - Δν * (_(n) + α)
        return C * (np.sqrt(w) if w is not None else 1) / (_(σ_ν) if σ_ν is not None else 1)

    ls = least_squares(cost, [np.median(np.diff(ν)), 0])

    Δν = ls['x'][0]
    stderr = Jac_stderr(ls['jac'])[0]
    ## Reduced chi-squared for weighted least-squares: df computed as sum of weights -
    # Note that scipy.optimize.least_squares adds a factor of 0.5 to cost function
    rchisquared = 2 * ls['cost']/(np.sum(w) if w is not None else (len(n) - 1))
    return Δν, stderr * np.sqrt(rchisquared), ls
