#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy
'''
Various EOS routines, built around MESA numerics and data.
'''

import os
from os.path import join
import numpy as np
from .mesa import dt, pt, de
from .opal import get_pure_opal
from .opal2001 import opal2001 as opal2001

MESA_DIR = os.environ['MESA_DIR']
MESA_NAMES = ['lnPgas', 'lnE', 'lnS', 'mu', 'lnfree_e', 'eta', 'grad_ad', 'chiRho', 'chiT', 'Cp',
              'Cv', 'dE_dRho', 'dS_dT', 'dS_dRho', 'gamma1', 'gamma3']

# TODO: find a better way of including default Zfracs that doesn't require this much boilerplate
# I've tried setting these Zfracs to the ones that came with the OPAL documentation at
# https://opalopacity.llnl.gov/pub/opal/type1data/GN93/ascii/GN93hz but the results only change
# in the 6th decimal place, vs. discrepancies in the 3rd/4th decimal place for MESA vs pure OPAL

def mesa_eos_DT(
                Z: float, X: float, ρ, T,
                Zfrac_C=0.173312e0,
                Zfrac_N=0.053177e0,
                Zfrac_O=0.482398e0,
                Zfrac_Ne=0.098675e0
    ):
    '''
    Equation of state routine using density and temperature as input variables

    Inputs
    ------

    Z, X: Metal and hydrogen mass fractions
    ρ (ndarray): Density (cgs units: g/cm**3)
    T: Temperature (cgs units: Kelvin)


    Outputs
    -------

    results: A dict with keys given by MESA_NAMES

    '''
    assert np.isscalar(Z) and np.isscalar(X), "X and Z must be scalars"
    assert np.asarray(ρ).shape == np.asarray(T).shape, "ρ and T must be the same shape"
    shape = np.asarray(ρ).shape
    return {name: _.reshape(shape) for name, _ in zip(MESA_NAMES,
                                                      dt(MESA_DIR, Z, X, np.asarray(ρ).flatten(),
                                                         np.asarray(T).flatten(),
                                                         Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne))}

def mesa_eos_PT(
                Z: float, X: float, P, T,
                Zfrac_C=0.173312e0,
                Zfrac_N=0.053177e0,
                Zfrac_O=0.482398e0,
                Zfrac_Ne=0.098675e0
    ):
    '''
    Equation of state routine using density and temperature as input variables

    Inputs
    ------

    Z, X: Metal and hydrogen mass fractions
    P: Pressure (cgs units: baryes)
    T: Temperature (cgs units: Kelvin)


    Outputs
    -------

    results: A dict with keys given by MESA_NAMES. Density is also included.

    '''
    assert np.isscalar(Z) and np.isscalar(X), "X and Z must be scalars"
    assert np.asarray(P).shape == np.asarray(T).shape, "ρ and T must be the same shape"
    shape = np.asarray(P).shape
    ρ, res = pt(MESA_DIR, Z, X, np.asarray(P).flatten(), np.asarray(T).flatten(),
                Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne)
    out = {name: _.reshape(shape) for name, _ in zip(MESA_NAMES, res)}
    out['Rho'] = ρ.reshape(shape)
    return out

def mesa_eos_DE(
                Z: float, X: float, ρ, E, log10T_guess=4,
                Zfrac_C=0.173312e0,
                Zfrac_N=0.053177e0,
                Zfrac_O=0.482398e0,
                Zfrac_Ne=0.098675e0
    ):
    '''
    Equation of state routine using density and temperature as input variables

    Inputs
    ------

    Z, X (float): Metal and hydrogen mass fractions
    ρ (ndarray): Density (cgs units: g/cm**3)
    E (ndarray): Specific internal energy (cgs units: erg/g)
    log10T_guess (float): Initial guess for the temperature (in Kelvin; default 4)

    Outputs
    -------

    results: A dict of numpy arrays with keys given by MESA_NAMES. Temperature is also included.

    '''
    assert np.isscalar(Z) and np.isscalar(X), "X and Z must be scalars"
    assert np.asarray(ρ).shape == np.asarray(E).shape, "ρ and T must be the same shape"

    if np.isscalar(log10T_guess):
        log10T_guess = log10T_guess * np.ones_like(ρ)
    else:
        assert np.asarray(ρ).shape == np.asarray(log10T_guess).shape

    shape = np.asarray(ρ).shape
    T, res = de(MESA_DIR, Z, X, np.asarray(ρ).flatten(), np.asarray(E).flatten(),
                Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne, log10T_guess)
    out = {name: _.reshape(shape) for name, _ in zip(MESA_NAMES, res)}
    out['T'] = T.reshape(shape)
    return out


# OPAL EOS interpolation: f2py wrapper using MESA libraries
# Most of the heavy lifting is done with codes provided by MESA and the OPAL release
# adapted from $MESA/eos/test/opal_eval.f

DATA_DIR = join(MESA_DIR, 'eos/eosDT_builder/eos_input_data')
OPAL_NAMES = ['logPgas', 'logE', 'logS', 'chiRho', 'chiT', 'Cp', 'Cv', 'dE_dRho', 'dS_dT',
              'dS_dRho', 'mu', 'logNe', 'gamma1', 'gamma3', 'grad_ad']

def opal_eos(Z, X, ρ, T, prad=True, data_dir=DATA_DIR, dps=2):
    '''
    OPAL EOS, derived from interpolating vs. tables
    The provided data files only exist at fixed Z (0, 0.02, 0.04 and a few others),
    and will return errors if the relevant data files do not exist.

    This particular set of OPAL routines was stolen from MESA r12115.

    Inputs:
    Z (ndarray): Metal mass fraction (wrt GN93 chemical mixture since this is OPAL)
    X (ndarray): Hydrogen mass fraction
    ρ (ndarray): Density (in cgs units)
    T (ndarray): Temperature (in K)
    prad (bool): Whether or not to include radiation pressure corrections
    data_dir (string): Folder containing data files
    dps (int): decimal places in data filename (e.g. with dps=2, data files match EOS5_data_%02dz,
                corresponding to EOS5_data_02z for Z = 0.02).

    Outputs:
    results: a dict with columns specified by OPAL_NAMES. I reproduce the relevant comments
    from the opal_lib FORTRAN file below:

!            results(1) is the pressure in megabars (10**12dyne/cm**2)
!            results(2) is energy in 10**12 ergs/gm. Zero is zero T6
!            results(3) is the entropy in units of energy/T6
!            results(4) is dE/dRHO at constant T6
!            results(5) is the specific heat, dE/dT6 at constant V.
!            results(6) is dlogP/dlogRho at constant T6.
!                   Cox and Guil1 eq 9.82
!            results(7) is dlogP/dlogT6 at constant Rho.
!                   Cox and Guil1 eq 9.81
!            results(8) is gamma1. Eqs. 9.88 Cox and Guili.
!            results(9) is gamma2/(gamma2-1). Eqs. 9.88 Cox and Guili
!            results(10) is mu_M
!            results(11) is log_Ne

    These refer to the outputs of the actual OPAL EOS table; the file
    opal.f transforms these to obtain some derived quantities and also converts
    to cgs units.
    '''

    v = np.vectorize(lambda Z, X, ρ, T: get_pure_opal(Z, X, ρ, T, data_dir, prad, dps))

    return {name: _ for name, _ in zip(OPAL_NAMES, v(Z, X, ρ, T))}

def opal2001_eos(Z, X, ρ, T):
    '''
    OPAL2001 EOS, derived from interpolating vs. tables

    This particular set of OPAL routines was stolen from YREC (with an adaptor to fit the MESA routines).
    The Fortran code will look for a file named "eosfile" in the **WORKING DIRECTORY**.

    Inputs:
    Z (ndarray): Metal mass fraction (wrt GN93 chemical mixture since this is OPAL)
    X (ndarray): Hydrogen mass fraction
    ρ (ndarray): Density (in cgs units)
    T (ndarray): Temperature (in K)

    Outputs:
    results: a dict with columns specified by OPAL_NAMES
    '''

    v = np.vectorize(lambda Z, X, ρ, T: opal2001(X, Z, ρ, T))

    return {name: _ for name, _ in zip(OPAL_NAMES, v(Z, X, ρ, T)) if not np.all(_ == 0)}