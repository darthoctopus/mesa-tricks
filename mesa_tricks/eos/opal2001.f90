subroutine opal2001(X, Z, rho, T, results)
      implicit real*8(a-h,o-z)
      implicit logical*4(L)

      double precision, intent(in) :: X, Z, rho, T
      double precision, intent(out) :: results(15)
      COMMON/Eeos/ ESACT,EOS(10)

        call esac(X, Z, T / 1d6, rho, 9, 1)

        P = eos(1) * 1d12
        dPdD = eos(5) * P / rho
		dPdT = eos(6) * P / T

        E = eos(2) * 1d12

        S = eos(3) * 1d6
        dSdT = eos(4) * 1d6 / T

        gamma_1 = eos(7)
        g2g21 = eos(8)
        gamma_3 = eos(9) + 1

        results(1) = LOG(P) / LOG(1d1) ! log P
        results(2) = LOG(E) / LOG(1d1) ! log E
        results(3) = LOG(S) / LOG(1d1)! log S
        results(4) = eos(5) ! chi_rho
        results(5) = eos(6) ! chi_T

        Cv = results(5) * P / (rho * T * eos(7)/eos(8)) ! C&G 9.93
        Cp = Cv*gamma_1/eos(5)

        results(6) = Cp
        results(7) = Cv

        results(8) = 0
        results(9) = dSdT
        results(10) = 0

        results(11) = 0
        results(12) = 0

        results(13) = gamma_1
        results(14) = gamma_3
        results(15) = 1/g2g21 ! grad_ad

end subroutine opal2001