subroutine DT(dir, Z_in, X_in, Rho_in, T_in, Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne, res, k)
    ! use eos_def, only : num_eos_basic_results
    use mesa_eos

    implicit none

    !f2py integer, intent(aux) :: num_eos_basic_results
    integer :: k
    double precision, intent(in) :: Z_in, X_in, Rho_in(k), T_in(k)
    double precision, intent(in) :: Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne
    ! double precision, intent(out) :: res(num_eos_basic_results, k)
    ! Somehow f2py can't handle this — as of mesa r12115 nv == 16
    double precision, intent(out) :: res(16, k)
    character (len=*) :: dir

    ! write(*,*) k
    ! dir = '/opt/mesa'

    call EOSDT(dir, Z_in, X_in, Rho_in, T_in, Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne, res)

end subroutine DT

subroutine PT(dir, Z_in, X_in, P_in, T_in, Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne, Rho_out, res, k)
    use mesa_eos

    implicit none

    integer :: k
    double precision, intent(in) :: Z_in, X_in, P_in(k), T_in(k)
    double precision, intent(in) :: Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne
    double precision, intent(out) :: res(16, k), Rho_out(k)
    character (len=*) :: dir

    call EOSPT(dir, Z_in, X_in, P_in, T_in, Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne, Rho_out, res)

end subroutine PT

subroutine DE(dir, Z_in, X_in, Rho_in, E, Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne, log10T_guess, T_out, res, k)
    use mesa_eos

    implicit none

    integer :: k
    double precision, intent(in) :: Z_in, X_in, Rho_in(k), E(k), log10T_guess(k)
    double precision, intent(in) :: Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne
    double precision, intent(out) :: res(16, k), T_out(k)
    character (len=*) :: dir

    call EOSDE(dir, Z_in, X_in, Rho_in, E, Zfrac_C, Zfrac_N, Zfrac_O, Zfrac_Ne, log10T_guess, T_out, res)

end subroutine DE