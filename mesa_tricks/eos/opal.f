

      subroutine test_get_pure_opal
         double precision ::  
     >         Z, X, rho, temp,
     >         logPgas, logE, logS, chiRho, chiT, 
     >         Cp, Cv, dE_dRho, dS_dT, dS_dRho, 
     >         mu, logNe, gamma1, gamma3, grad_ad
         character (len=256) :: data_dir
         integer :: ierr
         logical :: include_radiation
         
         include 'formats.dek'
         
         data_dir = '/opt/mesa/eos/eosDT_builder/eos_input_data'
         Z = 0.02d0
         X = 0.72d0
         rho = 1d-3
         temp = 1d5
         include_radiation = .true.
         ierr = 0
         call get_pure_opal(
     >         Z, X, rho, temp, data_dir, include_radiation, 2,
     >         logPgas, logE, logS, chiRho, chiT, 
     >         Cp, Cv, dE_dRho, dS_dT, dS_dRho, 
     >         mu, logNe, gamma1, gamma3, grad_ad,
     >         ierr)
         if (ierr /= 0) then
            write(*,*) 'failed in get_pure_opal'
            stop 1
         end if
         
         write(*,1) 'Z', Z
         write(*,1) 'X', X
         write(*,1) 'rho', rho
         write(*,1) 'temp', temp
         write(*,*)
         write(*,1) 'logPgas', logPgas
         write(*,1) 'logE', logE
         write(*,1) 'logS', logS
         write(*,1) 'chiRho', chiRho
         write(*,1) 'chiT', chiT
         write(*,1) 'Cp', Cp
         write(*,1) 'Cv', Cv
         write(*,1) 'dE_dRho', dE_dRho
         write(*,1) 'dS_dT', dS_dT
         write(*,1) 'dS_dRho', dS_dRho
         write(*,1) 'mu', mu
         write(*,1) 'logNe', logNe
         write(*,1) 'gamma1', gamma1
         write(*,1) 'gamma3', gamma3
         write(*,1) 'grad_ad', grad_ad
         write(*,*)
         
      
      end subroutine test_get_pure_opal


      subroutine get_pure_opal(
     >         Z, X, den, temp, data_dir, include_radiation, dps,
     >         logPgas, logE, logS, chiRho, chiT, 
     >         Cp, Cv, dE_dRho, dS_dT, dS_dRho, 
     >         mu, logNe, gamma1, gamma3, grad_ad,
     >         ierr)
         use const_def
         use crlibm_lib
         double precision, intent(in) :: Z ! the desired Z
         double precision, intent(in) :: X ! the desired X         
         double precision, intent(in) :: den ! the density            
         double precision, intent(in) :: temp ! the temperature
         character (len=*), intent(in) :: data_dir
         logical, intent(in) :: include_radiation
         integer, intent(in) :: dps
         double precision, intent(out) ::  
     >         logPgas, logE, logS, chiRho, chiT, 
     >         Cp, Cv, dE_dRho, dS_dT, dS_dRho, 
     >         mu, logNe, gamma1, gamma3, grad_ad
         integer, intent(out) :: ierr
         
         integer, parameter :: num_results = 11
         double precision :: opal_results(num_results)
         double precision t6, r 
         integer :: irad
         double precision :: pout, dpoutdd, dpoutdt, eout, deoutdd,
     >      sout, dsoutdd, dsoutdt, pgas_out, prad_out

         double precision log_free_e, log_free_e0, log_free_e1, Prad
         
         include 'formats.dek'
         
         t6 = temp * 1d-6
         r = den
         if (include_radiation) then
            irad = 1
         else
            irad = 0
         end if
         call opal_eos (X, Z, t6, r, num_results, 
     >      irad, dps, opal_results, pgas_out, prad_out, data_dir, ierr)
         if (ierr /= 0) then
            !return
            
            write(*,*) 'opal_eos returned ierr', ierr
            write(*,*) ' opal_only', opal_only
            write(*,*) ' alfa', alfa
            write(*,*) ' beta', beta
            write(*,*) ' X', X
            write(*,*) ' Z', Z
            write(*,*) 't6', t6
            write(*,*) ' r', r
            stop 1
         end if
         
         pout    = opal_results(1) * 1d12
         dpoutdd = opal_results(6) * pout / den
         dpoutdt = opal_results(7) * pout / temp
   
         eout    = opal_results(2) * 1d12
         ! NOTE: rather than use the opal tabulated value for cv,
         ! it is better to calculate cv from other things.
         deoutdd = opal_results(4) * 1d12
   
         sout    = opal_results(3) * 1d6
         dsoutdd = (deoutdd - pout / (den*den)) / temp
         dsoutdt = opal_results(5) * 1d6 / temp

         mu = opal_results(10)
         logNe = opal_results(11)
         
         if (include_radiation .and. Prad >= pout) then
            write(*,*) 'Prad >= pout'
            stop 'opal_scvh_driver'
         end if
         
         logPgas = log10_cr(pgas_out)
         logE = log10_cr(eout)
         logS = log10_cr(sout)
         chiRho = dpoutdd*den/pout
         chiT = dpoutdt*temp/pout
         gamma1 = opal_results(8)
         ! gam1 / (gam3 - 1) = gam2 / (gam2 - 1)
         ! gam2 / (gam2 - 1) = opal_results(9)
         gamma3 = 1 + opal_results(8)/opal_results(9)
         Cv = chiT * pout / (den * temp * opal_results(8)/opal_results(9)) ! C&G 9.93
         Cp = Cv*gamma1/chiRho
         grad_ad = 1/opal_results(9)
         dE_dRho = deoutdd
         dS_dRho = dsoutdd
         dS_dT = dsoutdt
         log_free_e = logNe - log10_cr(avo) - logRho


      end subroutine get_pure_opal