program sample_eos
	use eos_def
	use mesa_eos

	double precision :: res(num_eos_basic_results, 2)
	double precision, dimension(2) :: d_dlnRho_const_T, d_dlnT_const_Rho
	call Sample
	call EOSDT('/opt/mesa', 0.02d0, 0.70d0, [1d2, 1d2], [2d8, 2d8], 0.173312d0, 0.053177d0, 0.482398d0, 0.098675d0, &
		res)
	write(*,'(e20.12)') res
end