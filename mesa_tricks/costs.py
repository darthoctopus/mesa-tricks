#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Various cost functions to be used as χ^2 for an asteroseismic optimisation routine
'''

import numpy as np
from scipy.interpolate import UnivariateSpline
from scipy.optimize import bisect
import matplotlib.pyplot as plt

from tqdm.auto import tqdm
from .science import bg_fit, bg_matrix
from .science.ratios import r02, e_r02
from .utils import validate, match_modes
from .utils.coupling import new_modes

def bgcost(ν_obs, e_obs, ν_model, inertia, l=None, l0_only=False, trace=False, **kwargs):
    '''
    Returns reduced χ² associated with BG14 surface correction.

    Inputs:
    ν_obs, e_obs, ν_model, inertia (array): Relevant physical quantities (must be same length)
    l (optional, array): mode degree
    l0_only (bool): If true, will fit coefficients only against l=0 modes, but return cost for all modes
    trace (bool): whether or not to print debug information

    Outputs:
    (χ², p), where
    Χ² (float) is the reduced χ² associated with the data, and
    p (array) is a vector containing the best-fit parameters (by default dimensionful).
    '''

    validate(ν_obs, ν_model, inertia, None, e_obs)

    if l0_only and l is not None:
        assert len(l) == len(ν_obs), "l must be supplied for all modes"
        m0 = (l == 0)

    else:
        m0 = slice(None, None)

    j, _, residual = bg_fit(ν_obs[m0], ν_model[m0], inertia[m0], σ_ν=e_obs[m0], **kwargs)
    dof = len(ν_obs) - len(j['x']) - 1
    if dof < 1:
        χ2 = np.inf
    else:
        χ2 = np.sum((residual(ν_obs, ν_model, inertia) / e_obs)**2) / dof

    if trace:
        # Make echelle diagram
        Δν = np.median(np.diff(ν_obs[l == 0]))
        for ll in np.unique(l):
            m = (l == ll)
            plt.errorbar(ν_obs[m] % Δν, ν_obs[m], xerr=e_obs[m],
                yerr=e_obs[m], c=f"C{int(ll)}", fmt='o', zorder=-1, label=f'$l={ll}$')
            plt.plot(ν_model[m] % Δν, ν_model[m], '^', c=f"C{int(ll)}", markerfacecolor='none')
        ν_c = ν_obs - residual(ν_obs, ν_model, inertia)
        plt.plot(ν_c % Δν, ν_c, '.', c='black')

    return χ2, j['x']

def ε_cost(ν_obs, e_obs, ν_model, n_p, l, Δν, M_max=None,
    trace=False, force_mod=False, return_callable=False):

    '''
    Reduced χ² cost associated with phase-matching algorithm (to ensure that
    mode frequency discrepancies are indeed due to the surface term)
    '''

    # General idea: with
    # ν ~ Δν(n + l/2 + ε),
    # for 2 models that disagree only at the surface,
    # the function ε_1 - ε2 should be only a function of frequency.
    # In principle this can be used as a check of how well the interiors
    # agree, without suffering from the surface effect.
    # See Roxburgh 2016

    if M_max is None:
        M_max = sum(l == 0)

    ε_obs = ν_obs/Δν - n_p - l/2
    ε_model = ν_model/Δν - n_p - l/2
    s = e_obs / Δν

    # I construct one interpolant for each value of l

    F = np.ones_like(ν_obs) * np.nan
    for ll in np.unique(l):
        m = (l == ll) & (~np.isnan(ν_model)) & (~np.isnan(ε_model)) & (s > 0)
        try:
            k = min(3, len(ν_model[m]) - 1)
            if k < 1:
                continue
            ε_ = UnivariateSpline(ν_model[m], ε_model[m], s=0, k=k)
            F[m] = ε_(ν_obs[m]) - ε_obs[m]

            if force_mod:
                # Deliberately suppress off-by-one errors in mode ID
                F[m] -= np.round(np.nanmean(F[m]))

            if trace:
                # Make ε-difference diagram
                plt.errorbar(ν_obs[m], F[m], yerr=s[m], fmt='.', label=f"$l={ll}$")
        except OSError as e:
            tqdm.write(f'error computing ε for l = {ll}: {e}')

    best = np.inf
    p_best = None
    m = (~np.isnan(F)) & (s > 0)
    for M in range(1, M_max):
        dof = len(ν_obs[m]) - M - 1
        if dof < 1:
            break
        p = np.polynomial.chebyshev.chebfit(ν_obs[m], F[m], M, w=1/s[m])
        χ2 = np.sum(((F[m] - np.polynomial.chebyshev.chebval(ν_obs[m], p))/s[m])**2) / dof
        if χ2 < best:
            best = χ2
            p_best = p

    if trace:
        νν = np.linspace(np.min(ν_obs), np.max(ν_obs))
        try:
            plt.plot(νν, np.polynomial.chebyshev.chebval(νν, p_best), c='black')
        except ValueError:
            pass

    best = (best if p_best is not None else np.inf)

    if not return_callable:
        return best

    return best, lambda x: np.polynomial.chebyshev.chebval(x, p_best)

def ε_mixed_cost(ν_model, E, ν_obs, e_obs, l_obs, Δν, ν_π, E_π, l_π, n_π,
                 M_max=None, trace=False, force_mod=False, interp_Q=True,
                 ax=None, return_callable=False):
    '''
    Reduced χ² cost associated with phase-matching algorithm (to ensure that
    mode frequency discrepancies are indeed due to the surface term),
    modified to accommodate mixed modes.

    Requires separate computation of mixed and π modes.
    '''

    assert len(E) == len(ν_model) == len(ν_obs) == len(l_obs), "Must provide mode inertiae for model modes"
    assert len(E_π) == len(ν_π) == len(l_π) == len(n_π), "Must provide model π modes"

    if M_max is None:
        M_max = sum(l_obs == 0)

    if ax is None:
        ax = plt

    ε_π = ν_π/Δν - n_π - l_π/2
    s = e_obs / Δν
    e_ε = np.copy(s)

    # inertia interpolants

    if interp_Q == "radial":
        # radial p-modes
        m = (l_obs == 0)
        logE_ = UnivariateSpline(ν_model[m], np.log(E[m]), s=0)
        def E_(ν):
            return np.exp(logE_(ν))
    elif interp_Q == "radial_pi":
        # radial π-modes
        m = (l_π == 0)
        logE_ = UnivariateSpline(ν_π[m], np.log(E_π[m]), s=0)
        def E_(ν):
            return np.exp(logE_(ν))
    else:
        # all π-modes
        m = np.argsort(ν_π)
        logE_ = UnivariateSpline(ν_π[m], np.log(E_π[m]), s=0)
        def E_(ν):
            return np.exp(logE_(ν))

    # I construct one interpolant for each value of l

    F = np.ones_like(ν_obs) * np.nan
    for ll in np.unique(l_obs):
        m_π = (l_π == ll) & (~np.isnan(ν_π)) & (~np.isnan(ε_π))
        m_obs = (l_obs == ll) & (~np.isnan(ν_obs)) & (s > 0)
        try:
            k = min(3, len(ν_π[m_π]) - 1)
            if k < 1:
                continue
            ε_ = UnivariateSpline(ν_π[m_π], ε_π[m_π], s=0, k=k)

            if interp_Q:
                Q = E[m_obs] / E_(ν_obs[m_obs])
            else:
                # pull out nearest π mode for each observed mode
                ii = [np.argmin((_ - ν_π[m_π])**2) for _ in ν_model[m_obs]]
                Q = np.array([e / E_π[m_π][i] for i, e in zip(ii, E[m_obs])])

            # This differs from the definition in the paper by overall sign
            # to keep quantities positive

            F[m_obs] = (ν_model[m_obs] - ν_obs[m_obs]) / Δν * Q - (ε_(ν_model[m_obs]) - ε_(ν_obs[m_obs]))
            e_ε[m_obs] *= Q

            if force_mod:
                # Deliberately suppress off-by-one errors in mode ID
                F[m_obs] -= np.round(np.nanmean(F[m_obs]))

            if trace:
                # Make ε-difference diagram
                ax.errorbar(ν_obs[m_obs], F[m_obs], yerr=e_ε[m_obs], fmt='.', label=f"$l={ll}$")
        except OSError as e:
            tqdm.write(f'error computing ε for l = {ll}: {e}')

    best = np.inf
    p_best = None
    m = (~np.isnan(F)) & (e_ε > 0) #& (F < 1)
    for M in range(1, M_max):
        dof = len(ν_obs[m]) - M - 1
        if dof < 1:
            break
        p = np.polynomial.chebyshev.chebfit(ν_obs[m], F[m], M, w=1/e_ε[m])
        χ2 = np.sum(((F[m] - np.polynomial.chebyshev.chebval(ν_obs[m], p))/e_ε[m])**2) / dof
        if χ2 < best:
            best = χ2
            p_best = p

    if trace:
        νν = np.linspace(np.min(ν_obs), np.max(ν_obs))
        try:
            ax.plot(νν, np.polynomial.chebyshev.chebval(νν, p_best), c='black', zorder=-1)
        except ValueError:
            pass

        print(f"M = {M}, N₀={M_max}, χ² = {best}")

    best = (best if p_best is not None else np.inf)

    if not return_callable:
        return best

    return best, lambda x: np.polynomial.chebyshev.chebval(x, p_best)

def bg_matrix_cost(ν_obs, e_obs, A, D, ν_π, E_π, params, l=1, diag_only=True, **kwargs):

    '''
    Reduced χ² cost associated with applying a correction of the form
    of Ball and Gizon 2014, to π-modes of a particular degree, as reflected
    in the coupled mixed modes of that degree.

    ν_obs, e_obs should be modes of a single degree.
    '''

    V = np.zeros_like(A)
    N_π = len(ν_π)
    if diag_only:
        V[:N_π, :N_π] = np.diag(np.diag(bg_matrix(*params, **kwargs)(ν_π, E_π)))
    else:
        V[:N_π, :N_π] = bg_matrix(*params, **kwargs)(ν_π, E_π)

    r, ν = matrix_residuals(ν_obs, e_obs, A + V, D, ν_π, ν_π, l=l, **kwargs)
    return np.sum(r**2), len(r), ν

def ε_matrix_cost(ν_obs, e_obs, Δν, A, D, ν_π, n_π, F_, l=1, **kwargs):

    '''
    Reduced χ² cost associated with applying frequency differences found by
    phase-matching algorithm, to π-modes of a particular degree, as reflected
    in the coupled mixed modes of that degree.

    ν_obs, e_obs should be modes of a single degree.
    '''

    ε = ν_π/Δν - n_π - l/2
    ε_ = UnivariateSpline(ν_π, ε, s=0, ext='raise')

    def G(ν_surf, ν_π):
        return (ν_surf - ν_π) / Δν + ε_(ν_π) - ε_(ν_surf) + F_(ν_surf)
    def correct(ν):
        try:
            return bisect(lambda x: G(x, ν), ν - Δν/4, ν + Δν/4)
        except ValueError:
            return np.nan

    ν_π_new = np.array([correct(ν) for ν in ν_π])
    r, ν = matrix_residuals(ν_obs, e_obs, A, D, ν_π, ν_π_new, l=l, **kwargs)
    return np.sum(r**2), len(r), ν, correct

def matrix_residuals(ν_obs, e_obs, A, D, ν_π, ν_π_new, l=1, ξr=None, ξh=None, M=None, **kwargs):
    '''
    Given a coupling matrix and a set of π modes (corrected and uncorrected),
    compute the mixed modes corresponding to the corrected π modes, and then
    compute the reduced χ² with respect to the observed set of modes.
    '''

    N_π = len(ν_π)

    # do the π-mode correction where applicable
    if not np.all(np.isclose(ν_π, ν_π_new)):
        for i in range(N_π):
            if not np.isnan(ν_π_new[i]):
                A[i, i] += (ν_π[i] / 1e6 * 2 * np.pi)**2
                A[i, i] -= (ν_π_new[i] / 1e6 * 2 * np.pi)**2

    ν, ζ, E = new_modes(A, D, N_π, ξh=ξh, ξr=ξr, M=M)
    q = np.argsort(ν)
    ν, ζ = (ν[q], ζ[q])
    if E is not None:
        E = E[q]
    else:
        E = np.ones_like(ν) * np.nan

    matched = match_modes(modes_obs={'ν': ν_obs, 'e_ν': e_obs, 'l': (0 * ν_obs + l).astype(int)},
                          modes_model={'ν': ν, 'E': E, 'l': (ν * 0 + l).astype(int), 'n_p': ν * 0},
                          force_mod=False)

    r = (matched['ν_obs'] - matched['ν_model'])/matched['e_obs']
    return r, ν

def asymmetric_penalty(x, F=10):
    return np.sum(np.where(x>0, x/F, x)**2)

def low_n_cost(ν_obs, e_obs, ν_model, n=5, **kwargs):
    '''
    Prior: frequency differences between observed and model frequencies
    take the same signs as those seen in the sun (where low-n modes are
    less affected by the surface term than high-n ones).
    '''

    l = np.argsort(ν_obs)
    dof = len(ν_model[:n]) - 1
    if dof < 1:
        return np.inf
    return asymmetric_penalty(((ν_model - ν_obs)/e_obs)[l][:n], **kwargs) / dof

def ratio_cost(ν_obs, e_obs, ν_model, ll, n_p, ratio=r02, err=e_r02, no_l1=False, **kwargs):
    '''
    Differences between r_02
    '''

    ν0_obs, r_obs = ratio(ν_obs, ll, n_p, no_l1=no_l1)
    err_obs = err(ν_obs, ll, n_p, e_obs, no_l1=no_l1)
    ν0_model, r_model = ratio(ν_model, ll, n_p, no_l1=no_l1)

    m = (~np.isnan(r_model)) & (~np.isnan(r_obs))

    if sum(m) < 2:
        return np.nan

    r_ = UnivariateSpline(ν0_model[m], r_model[m], s=0, k=min(3, sum(m)-1), ext=1)
    dof = sum(m) - 1

    r_int = r_(ν0_obs)
    m &= (r_int != 0)
    return np.sum(((r_obs - r_int)/err_obs)[m]**2) / dof
