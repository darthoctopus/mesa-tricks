#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Phase integrals (Ong and Basu 2019b)
'''

import numpy as np
from scipy.interpolate import interp1d, UnivariateSpline
from scipy.integrate import solve_ivp
from scipy.optimize import bisect
from astropy import constants as c

from . import prune_double_points, cs, safe_cumulative_trapezoid

# Gravitational Constant
G = c.G.cgs.value

# Some utility functions to get acoustic radii and derivatives
def tt(p):
    return safe_cumulative_trapezoid(1/cs(p), prune_double_points(p['r']))

def ddt(f, p, spline=False):
    if not spline:
        return np.asarray(cs(p) * np.gradient(f, prune_double_points(p['r'])))
    u = UnivariateSpline(prune_double_points(p['r']), f, s=0, k=2)
    return cs(p) * u(p['r'], 1, ext=0)

def reg(δ, ωt):
    return np.where(ωt < 1e-100, 1, 1 - δ / ωt)

class PhaseProblem:
    '''
    Computes phase function (from the method of undetermined phases)
    Works surprisingly well despite the coarseness of the approximation.
    See Calogero 1963, Babikov 1976, Zhaba 2016 (mostly concerned with
    scattering problems for Schrödinger's equation in spherically symmetric systems.)

    In more detail: with an eigenvalue equation ν_nl = 1/2T (n + l/2 + ε),
    ε can be expressed as 1/π(α - δ), with each of α and δ obtained by solving
    an initial value problem exhibiting boundary-layer behaviour near the
    surface and the centre. See introduction of Ong & Basu (2019b) for details.

    The acoustic potential here is derived from an expression in Gough (1993)
    in Sturm-Liouville form (eq. 4.1.6), which also appears in JCD's lecture
    notes (eq. 4.57). This is is valid for radial oscillations *without* requiring
    the use of the Cowling approximation. Boundary condition is that of Roxburgh 1994.
    '''

    def __init__(self, p, f=0.5, inter_opts=None, **kwargs):
        '''
        Pass this a GYRE object
        '''
        # Immutable numerical options
        self.f = f
        self.inter_opts = inter_opts

        # Stellar model (this can be changed)
        self._p = p
        self.t = tt(p)
        self.Vt2_, self.V = self.construct_V(p, inter_opts=inter_opts)
        self.inter_opts = inter_opts

    @classmethod
    def construct_V(klass, p, inter_opts=None):
        '''
        Upon reduction of the oscillation equations to Schrödinger form,
        the mode cavity is defined by an acoustic potential V which is
        singular at t = 0.
        We define wt and Vt², both of which are dimensionless quantities.
        This ensures regularity of the numerical scheme at the origin.
        '''

        def wt(p, simple=True):
            Γ1 = p['Γ1']
            g = np.array(G * p['m'] / p['r']**2)
            g[0] = 0
            t = tt(p)
            t1 = 1/4 * t * ddt(Γ1, p, spline=False)/Γ1
            t2 = -cs(p) * t * p['N2'] / 4 / g
            t2[0] = 0
            t3 = -(Γ1 + 1)/4 * g / cs(p) * t
            t4 = 2 * cs(p) * t / p['r']
            t4[0] = 2

            return np.asarray(t1 + t2 + t3 + t4)

        def Vt2(p, simple=True):
            t = tt(p)
            Γ1 = p['Γ1']
            g = np.array(G * p['m'] / p['r']**2)
            c = cs(p)
            g[0] = 0
            t1 = wt(p, simple=False)**2
            t3 = (t * ddt(wt(p, simple=False), p) - wt(p, simple=False))
            t4 = (3 * Γ1 - 4) * g / p['r'] * t**2
            t4[0] = 0
            t5 = 3 * c / p['r'] * ddt(Γ1, p, spline=False)/Γ1 * t**2
            t5[0] = 0
            return np.asarray(t1 + t3 + t4 - t5)

        opts = {'kind': 'cubic', 'fill_value':'extrapolate'}
        if inter_opts is not None:
            opts = {**opts, **inter_opts}

        t = tt(p)

        Vt2_ = interp1d(t, Vt2(p), **opts)
        V = np.vectorize(lambda t: Vt2_(t)/t**2)
        return Vt2_, V
    @property
    def p(self):
        '''
        GYRE-compatible pandas dataframe representing internal structure of the star
        '''
        return self._p
    @p.setter
    def p(self, val):
        self._p = val
        self.t = tt(self._p)
        self.Vt2_, self.V = self.construct_V(val, inter_opts=self.inter_opts)

    '''
    Quantities actually used in the computation of the phase function
    '''

    def B(self, ω, T, **opts):
        '''
        Boundary condition from Roxburgh and Vorontsov 1994,
        assuming isothermal stratification; T is found
        at the temperature minimum
        '''
        p = self.p
        g = np.array(G * p['m'] / p['r']**2)
        g[0] = 0
        c_s = np.array(cs(p))
        BB = np.array(np.arctan(
            c_s / ω * (
                - 1/2 * g / c_s**2
                + (ω**2 / p['Γ1'] - p['N2']/2) / g
                + (4 - p['Γ1']) / p['Γ1'] / p['r']
            )
        ))
        opts = {'kind': 'cubic', 'fill_value':'extrapolate'}
        if self.inter_opts is not None:
            opts = {**opts, **self.inter_opts}
        B_ = interp1d(self.t, BB, **opts)
        return B_(T)

    def α(self, ω, **kwargs):
        out_α = self.integrate_α(ω, **kwargs)
        return out_α['y'][0][-1]

    def δ(self, ω, **kwargs):
        out_δ = self.integrate_δ(ω, **kwargs)
        return out_δ['y'][0][-1]

    @classmethod
    def integrate_phase(klass, V, y, z, x0, X, δ0, **kwargs):
        def fun(x, δ):
            return V(y(x)) * np.sin(z(x) - δ)**2

        return solve_ivp(fun, [x0, X], [δ0], **kwargs)

    def integrate_α(self, ω, T=None, V=None, α0=None,
                    max_step_f=0.001, t0=None, **kwargs):
        if V is None:
            V = self.V
        if T is None:
            T = np.max(self.t)
        if α0 is None:
            α0 = self.B(ω, T) - np.pi/2
        ωT = ω * T

        y_ = lambda ωt: ωt / ω
        z_ = lambda ωt: ωt - ωT
        V_ = lambda y: V(y) / ω**2
        kwargs = {'max_step': max_step_f * ωT, **kwargs}
        return self.integrate_phase(V_, y_, z_, ωT, ωT*(1*self.f), α0, **kwargs)

        # def fun_α(ωt, α, ω=ω):
        #     return V(ωt/ω) * np.sin(ωt - ωT - α)**2 / ω**2
        # kwargs = {'max_step': max_step_f * ωT, **kwargs}
        # return solve_ivp(fun_α, [ωT, ωT*(1-self.f)], [α0], **kwargs)

    def integrate_δ(self, ω, T=None, V=None, δ0=0.,
                    max_step_f=0.001, t0=None, **kwargs):
        if V is None:
            V = self.V
        if T is None:
            T = np.max(self.t)
        if t0 is None:
            t0 = self.t[1]
        # From a series expansion, we find that for small t, δ ~ 2ωt.
        # We use this to start solving from a small distance out rather than
        # at t=0.
        ωT = ω * T

        y_ = lambda ωt: ωt / ω
        z_ = lambda ωt: ωt
        V_ = lambda y: V(y) / ω**2
        kwargs = {'max_step': max_step_f * ωT, **kwargs}
        return self.integrate_phase(V_, y_, z_, ω * t0, ωT*self.f, 2 * t0, **kwargs)

        # def fun_δ(ωt, δ, ω=ω):
        #     # return Vt2_(ωt/ω) * np.sinc((ωt - δ)/np.pi)**2 * reg(δ, ωt)**2
        #     return V(ωt/ω) * np.sin(ωt - δ)**2 / ω**2
        # kwargs = {'max_step': max_step_f * ωT, **kwargs}
        # return solve_ivp(fun_δ, [ω * self.t[1], ωT*self.f], [2 * self.t[1]], **kwargs)

    def ε(self, ω, **kwargs):
        α = self.α(ω, **kwargs)
        δ = self.δ(ω, **kwargs)
        return -(α - δ) / np.pi


def ε(p, ω, **kwargs):
    problem = PhaseProblem(p, **kwargs)
    return problem.ε(ω, **kwargs)

def bounded_smooth(ε, λ=1):
    '''
    The function ε(ν) computed above is strictly defined only on the eigenvalues ν_nl.
    By assumption, the asymptotic relation holds well enough that two consecutive ε
    should not differ by more than 1. Were we to analytically continue it to real ν,
    it should in principle also not oscillate too wildly.

    λ is an oversampling factor (set it to len(ν_cont) / len(ν_eigs) in principle)
    '''

    return ε - np.concatenate([[0], np.cumsum(np.round(λ * np.diff(ε)))]) / λ
