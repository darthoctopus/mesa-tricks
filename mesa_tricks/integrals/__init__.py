#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Common things used for integral estimation
'''

import numpy as np
from scipy.integrate import trapezoid, cumulative_trapezoid

# import matplotlib.pyplot as plt

def cs(p):
    """Adiabatic Sound Speed"""
    return np.sqrt(p['Γ1'] * p['P'] / p['ρ'])

def H(p, obs):
    """
    Scale height of an observable obs from numerical differentiation
    """
    return -1/np.gradient(np.log(p[obs]), np.array(p['r']))

def prune_double_points(x, ε=1e-2):
    '''
    For HB models in particular, GYRE outputs double points to represent discontinuities.
    Under the assumption that these are legitimate, I shift these double points each by
    ±ε (on each side of the discontinuity) to maintain monotonicity in the input variable.
    '''
    y = np.copy(x)
    m = np.diff(x) == 0
    y[1:][m] += ε
    y[:-1][m] -= ε
    return y

def acoustic_T(p, **kwargs):
    c = np.array(cs(p))
    return trapezoid(y=1/c, x=prune_double_points(p['r']), **kwargs)

# Laplacian matrix associated with a given domain

def Laplacian(t):
    N = len(t)
    L = np.zeros((N,N))

    for i in range(1, N-1):
        x1 = t[i-1] - t[i]
        x2 = t[i+1] - t[i]
        L[i, i-1] = 1 / x1 / (x1 - x2) * 2
        L[i, i+1] = -1 / x2 /(x1 - x2) * 2
        L[i, i] = (1/x2 - 1/x1) / (x1 - x2) * 2

    # first and last row by hand
    x1 = t[1] - t[0]
    x2 = t[2] - t[0]
    L[0, 1] = 1/x1 /(x1-x2) * 2
    L[0, 2] = -1/x2 /(x1-x2) * 2
    L[0, 0] = (1/x2 - 1/x1) / (x1 - x2) * 2

    x1 = t[-3] - t[-1]
    x2 = t[-2] - t[-1]
    L[-1, -3] = 1/x1 /(x1-x2) * 2
    L[-1, -2] = -1/x2 /(x1-x2) * 2
    L[-1, -1] = (1/x2 - 1/x1) / (x1 - x2) * 2
    
    return L

def safe_cumulative_trapezoid(f, r, f0=None):
    ff = np.copy(f)
    if r[0] == 0:
        if np.isnan(ff[0]):
            ff[0] = ff[1]
        return cumulative_trapezoid(ff, r, initial=0)
    else:
        if f0 is None:
            f0 = ff[0]
        out = cumulative_trapezoid([f0, *ff], [0, *r])
        return out

def green_function_integral(r, f, l, R=None, grad=False):
    r'''
    Integral against point-source kernel.

    Let's say we have F(r, θ, φ) = f(r) Y_lm(θ, φ).
    We wish to compute the volume integral against the Coulomb Green's function
    \int \mathrm d^3 x (F(r',θ',φ') / |x - x'|) === Q(r) Y_lm(θ, φ).
    This computes Q using the trapezoid rule given f and l;
    it turns out that there is no m dependence.
    '''

    if R is None:
        R = r[-1]

    x = r / R

    if not grad:
        # integral from 0 to r
        I1 = np.power(x, -1-l) * safe_cumulative_trapezoid(f * np.power(x, l+2), x)
        # integral from r to R
        I2 = np.power(x, l)    * (lambda y: y[-1] - y)(safe_cumulative_trapezoid(f * np.power(x, 1-l), x))

        return 4 * np.pi/ (2 * l + 1) * (I1 + I2) * R**2
    else:
        # integral from 0 to r
        I1 = -(1+l) * np.power(x, -2-l) * safe_cumulative_trapezoid(f * np.power(x, l+2), x)
        # integral from r to R
        I2 = l * np.power(x, l-1)       * (lambda y: y[-1] - y)(safe_cumulative_trapezoid(f * np.power(x, 1-l), x))

        return 4 * np.pi/ (2 * l + 1) * (I1 + I2) * R