import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from astropy import constants as c

from . import prune_double_points, Laplacian, safe_cumulative_trapezoid
from .ε import PhaseProblem

def ff(p):
    '''
    Buoyancy radius, defined analogously to the acoustic radius
    in order to reduce the Sturm-Liouville problem to Schrödinger form.
    '''

    Z = np.array(np.sqrt(p['N2']) / p['r'])
    return safe_cumulative_trapezoid(Z, prune_double_points(p['r']))

def N2_automask(gyre, mask, plot=False):
    ω0 = np.sqrt(np.max(gyre['N2'][mask]))
    i0 = np.argmax(np.array(gyre['N2'][mask]))
    
    # generate a mask corresponding to the appropriate g-mode cavity
    mm = np.array(gyre['N2'] > 0)
    mask = np.zeros_like(mm)
    mask[i0] = True
    # we now iterate outwards to generate the appropriate mask
    in_bound = True
    out_bound = True
    for i in range(1, len(gyre)):
        if not in_bound and not out_bound:
            break
        if i0 - i >= 0 and mm[i0 - i] and in_bound:
            mask[i0 - i] = True
        else:
            in_bound = False

        if i0 + i < len(gyre) and mm[i0 + i] and out_bound:
            mask[i0 + i] = True
        else:
            out_bound = False
            
    if plot:
        plt.plot(gyre['r'], gyre['N2'] / ω0**2)
        plt.plot(gyre['r'], gyre['N2'] > 0)
        plt.plot(gyre['r'], mask)
        plt.show()
        
    return mask

class BuoyancyPhaseProblem(PhaseProblem):
    '''
    Computes phase functions (from method of undetermined phases) for
    buoyancy modes (i.e. γ-modes), using the same method (of undetermined
    phases) as we did for π-modes in the ε_p submodule. The maths is the same,
    just executed differently. Also, we need both Cowling's approximation
    *and* to assume ω² « S_l² to recover a Sturm-Liouville problem, although
    I am uncertain as to how well the method of undetermined phases potentially
    holds up for calculations without the latter assumption (i.e. frequency-
    dependent buoyancy potential).

    This formulation is from Unno et al eq. 16.8; notably we do not necessarily require
    that ω² « N² within the mode cavity.
    '''

    def __init__(self, p, l, f=0.5, inter_opts=None, mask=None, automask=True,
        remesh=100, **kwargs):
        '''
        Pass this a GYRE object, as usual. We need some special handling
        to deal with the fact that the g-mode cavity is now finite in extent.
        '''

        # Immutable numerical options
        self.f = f
        self.inter_opts = inter_opts

        if mask is None:
            mask = slice(None, None)

        self.seedmask = mask
        self.automask = automask
        self._l = l
        self.Λ = np.sqrt(l * (l + 1))
        self.remesh = remesh

        # Stellar model (this can be changed)
        self.p = p

        # For the buoyancy problem I do not attempt regularisation
        self.inter_opts = inter_opts

    @classmethod
    def construct_V(klass, p, l, mask=None, inter_opts=None, remesh=100, reduced=True):
        '''
        Acoustic potential for the γ-mode cavity, in terms of the 
        buoyancy radial coordinate f.
        '''

        if mask is None:
            mask = slice(None, None)

        if inter_opts is None:
            inter_opts = {}

        inter_opts = {'k': 3, 's': 0, **inter_opts}

        Λ = np.sqrt(l * (l + 1))
        f = ff(p)[mask] * Λ

        q = 0 if l == 1 and reduced else 1

        if remesh:
            f_new = np.linspace(np.min(f), np.max(f), remesh)
        else:
            f_new = f

        g = np.array(c.G.cgs.value * p['m'] / p['r']**2)
        g[0] = 0
        cs2 = np.array(p['Γ1'] * p['P'] / p['ρ'])

        N_ = UnivariateSpline(f, np.sqrt(p['N2'])[mask], **inter_opts)
        r_ = UnivariateSpline(f, p['r'][mask], **inter_opts)
        cs2_ = UnivariateSpline(f, cs2[mask], **inter_opts)

        V0_ = lambda f: 1/N_(f)**2 + r_(f)**2 / cs2_(f) / Λ**2 * q
        Z_ =  lambda f: N_(f) / r_(f)

        η = np.array((g/cs2) * q - (p['N2']/g)) # - d log h / d r
        η[0] = 0
        η_ = UnivariateSpline(f, η[mask], **inter_opts)

        ZZ = Z_(f_new)
        ZZ[0] = ZZ[1]
        up = np.gradient(ZZ, f_new) / ZZ / 2 + η_(f_new) / ZZ / Λ / 2
        upp = np.gradient(up, f_new)

        U_ = UnivariateSpline(f_new, up**2 + upp, **inter_opts)

        return lambda f: V0_(f) + U_(f)

    @property
    def l(self):
        return self._l
    
    @l.setter
    def l(self, val):
        self._l = val
        self.V = self.construct_V(self.p, val,
        	inter_opts=self.inter_opts, remesh=self.remesh)

    @property
    def p(self):
        '''
        GYRE-compatible pandas dataframe representing internal structure of the star
        '''
        return self._p

    @p.setter
    def p(self, val):

        if not self.automask:
            for i0, r in enumerate(val['N2']):
                if i0 and r<0:
                    i0 -= 1
                    break
            mask = slice(None, i0)
        else:
            mask = N2_automask(val, self.seedmask)

        self._p = val
        self.f0 = ff(val)[mask]
        self.V = self.construct_V(val, self.l, mask=mask,
        	inter_opts=self.inter_opts, remesh=self.remesh)

    def integrate_α(self, ω, F=None, V=None, α0=0,
                    max_step_f=0.001, **kwargs):
        if V is None:
            V = self.V
        if F is None:
            F = np.max(self.f0) * self.Λ
        
        Fdω = F / ω

        y_ = lambda fdω: fdω * ω
        z_ = lambda fdω: fdω - Fdω
        V_ = lambda y: V(y) * ω**2
        kwargs = {'max_step': max_step_f * Fdω, **kwargs}
        return self.integrate_phase(V_, y_, z_, Fdω,
            Fdω - self.f * (Fdω - self.f0[0]/ω*self.Λ), α0, **kwargs)

    def integrate_δ(self, ω, F=None, V=None, δ0=None, f0=None,
                    max_step_f=0.001, **kwargs):
        if V is None:
            V = self.V
        if F is None:
            F = np.max(self.f0) * self.Λ

        # From a series expansion, we find that for small f, δ ~ Λ² f/ω, if
        # the inner turning point is at f = r = 0.
        # We use this to start solving from a small distance out rather than
        # at t=0.

        if f0 is None:
            f0 = self.f0[1] * self.Λ

        if δ0 is None:
            if np.min(self.f0) == 0:
                δ0 = self.Λ**2 * f0
            else:
                δ0 = 0.

        Fdω = F / ω

        y_ = lambda fdω: fdω * ω
        z_ = lambda fdω: fdω
        V_ = lambda y: V(y) * ω**2
        kwargs = {'max_step': max_step_f * Fdω, **kwargs}
        return self.integrate_phase(V_, y_, z_, f0, Fdω * self.f, δ0, **kwargs)

def I_ξ_ψ_interpolant(gyre, mask=None, **kwargs):
    '''
    Returns an interpolant for the integrating factor
    I = r^2 sqrt(h_1 h_2 N / r)
    '''

    if mask is None:
        mask = slice(None, None)

    r0 = np.array(gyre['r'])
    N = np.nan_to_num(np.sqrt(gyre['N2'].values))
    ρ = np.nan_to_num(gyre['ρ'].values)

    N_ = UnivariateSpline(r0, N, **kwargs)
    ρ_ = UnivariateSpline(r0, ρ, **kwargs)

    def I_(r):
        return np.sqrt(r**3 * N_(r) * ρ_(r))

    return I_

def eig(gyre, l, N_grid=100, N_eig=5, mask=None, automask=False,
    reduced=False, eigenvectors=False, **kwargs):
    '''
    Eigenfrequencies of γ-modes with respect to the Cowling approximation
    '''

    if mask is None:
        mask = slice(None, None)

    if not automask:
        for i0, r in enumerate(gyre['N2']):
            if i0 and r<0:
                i0 -= 1
                break
        mask = slice(None, i0)
    else:
        mask = N2_automask(gyre, mask)

    Λ = np.sqrt(l * (l + 1))

    f = ff(gyre)[mask] * Λ
    f_new = np.linspace(np.min(f), np.max(f), N_grid)

    L = Laplacian(f_new)
    V_ = BuoyancyPhaseProblem.construct_V(gyre, l, mask=mask, remesh=N_grid, reduced=reduced, inter_opts=kwargs)
    V = V_(f_new)

    mm = slice(1, None)
    λ, U = np.linalg.eig(L[mm, mm] - np.diag(V[mm]))

    s = np.argsort(-1/np.sqrt(-λ))
    ν = np.real(1/np.sqrt(-λ[s]) * 1e6 / (2 * np.pi))

    if not eigenvectors:
        return ν[:N_eig]

    kwargs = {'k': 1, 's': 0, 'ext': 1, **kwargs}

    # construct eigenfunctions subject to appropriate coordinate transformation
    # and integrating factors.

    r0 = np.array(gyre['r'][mask])
    r_ = UnivariateSpline(f, r0, **kwargs)
    r = r_(f_new)

    I0 = I_ξ_ψ_interpolant(gyre, mask, **kwargs)

    def I(r):
        return 1/I0(r)

    ψ_ = [UnivariateSpline(r[mm], u, **kwargs) for u in U.T[s][:N_eig]]

    def wrap(j):
        def ξ(r):
            return j(r) * I(r)
        return ξ

    ξ_ = [wrap(j) for j in ψ_]
    return ν[:N_eig], ξ_