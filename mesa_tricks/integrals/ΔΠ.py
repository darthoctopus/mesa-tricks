#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Estimators for ΔΠ
'''

import numpy as np
from scipy.integrate import trapezoid, cumulative_trapezoid
from . import prune_double_points

def ΔΠ0_as(profile, mask=None):
    r"""
    Period spacing ΔΠ0 = 2π² / \int dr N / r , as computed from the asymptotic relation.
    """

    if mask is None:
        mask = slice(None, None)
    r = np.array(profile['r'])
    Z = np.nan_to_num(np.sqrt(profile['N2']) / r)
    Z[0] = Z[1]
    I = trapezoid(Z[mask], r[mask])
    return 2 * np.pi**2 / I

def get_ν_γ(gyre, n=1, ε=1/4, l=1, mask=None, automask=True, plot=False):
    '''
    Compute γ-mode frequencies (useful for avoided crossings).

    This is based on the asymptotic relation but in the low-n_g regime, where
    special care needs to be taken. This is kind of the "anti-asymptotic" regime.

    In particular, since the acoustic potential (basically just BV) looks locally quadratic,
    I use Hermite functions to approximate the g-mode eigenfunctions. Accordingly,
    the phases are a little weird. I might also have made some sign errors in my derivation.
    However, empirically these combinations of quantum numbers and phases work:

    n = 0, ε = 1/2
    n = 1, ε = 1/4
    n = 2, ε = 0
    n = 3, ε = -1/4

    These work for l=1 (with the exception of the first one); for l=2 these capture every
    other avoided crossing (because the classical dependence here is as ΔΠ = ΔΠ0/Λ, although
    clearly that is not working well here in the low-n_g limit). A better approximation might
    be to use Gough's modified buoyancy frequency, which has sphericity and f-mode
    discriminant terms.

    Generically, we should not even expect ε_g to be constant as the star ages.
    '''

    if mask is None:
        mask = slice(None, None)

    ω0 = np.sqrt(np.max(gyre['N2'][mask]))
    if automask:
        i0 = np.argmax(gyre['N2'][mask])
        # generate a bitmask corresponding to the appropriate g-mode cavity
        mm = np.array(gyre['N2'] > 0)
        mask = np.zeros_like(mm)
        mask[i0] = True
        # we now iterate outwards, terminating at the endpoints of the cavity
        in_bound = True
        out_bound = True
        for i in range(1, len(gyre)):
            if not in_bound and not out_bound:
                break
            if i0 - i >= 0 and mm[i0 - i] and in_bound:
                mask[i0 - i] = True
            else:
                in_bound = False
            
            if i0 + i < len(gyre) and mm[i0 + i] and out_bound:
                mask[i0 + i] = True
            else:
                out_bound = False
                
        if plot:
            plt.plot(gyre['r'], gyre['N2'] / ω0**2)
            plt.plot(gyre['r'], gyre['N2'] > 0)
            plt.plot(gyre['r'], mask)
            plt.show()
        
    ΔΠ = ΔΠ0_as(gyre[mask])
    P_min = 2 * np.pi / ω0
    P = P_min + ΔΠ * (n + ε)
    return 1e6 / P