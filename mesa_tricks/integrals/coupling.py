#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Analytic coupling matrices of mixed modes from integrals over the isolated π and γ
eigenfunctions in the sense of Aizenman+ 1977 and Ong & Basu 2020, intended for use
with GYRE v6 and up.
'''

import numpy as np
from scipy.interpolate import UnivariateSpline
from scipy.integrate import trapz
from astropy import constants as c
from . import prune_double_points, green_function_integral

def coupling_matrices(info, gyre, l, π, γ, x_atm=0.9, simple_γ=10, inter_opts=None):
    '''
    Given a GYRE structure, a degree, and two Eigensystem objects (one each for π and γ modes),
    compute the coupling matrices which yield mixed modes as the solutions to the generalised
    Hermitian eigenvalue problem of the form

    A c_n = - ω_n^2 D c_n

    Note that these accommodate GYRE's sign conventions for the π modes (amplitude positive at surface).
    You might get banded matrices with rows/columns of alternating sign otherwise.

    Options:

    - x_atm (float): radius above which to ignore N² even if it supports g-mode propagation
    - simple_γ (int): do not attempt to compute diagonal γ-mode matrix elements for modes with n_g below this value
    - inter_opts (dict): keyword arguments to pass to UnivariateSpline
    '''

    if inter_opts is None:
        inter_opts = {}

    inter_opts = {**dict(k=3, s=0, ext='zeros'), **inter_opts}

    # set up required interpolants

    Λ2 = l * (l + 1)
    G = c.G.cgs.value

    r0 = prune_double_points(np.array(gyre.r))
    R = float(info['R'])
    N2 = np.array(gyre.N2)
    N2_ = UnivariateSpline(r0, N2, **inter_opts)
    g_ = UnivariateSpline(r0, np.where(r0 > 0, G * gyre['m']/gyre['r']**2, 0), **inter_opts)

    # set up infrastructure for matrices
    N_π = len(π.ξ)
    Fππ = np.mean(π.I)

    if γ is not None:
        N_γ = len(γ.ξ)
        Fγγ = γ.I
        Fπγ = np.sqrt(Fππ * Fγγ)
    else:
        N_γ = 0

    Ntot = N_π + N_γ

    A = np.zeros((Ntot, Ntot))
    D = np.identity(Ntot)
    r = π.r
    ρ = π.ρ0

    # π self-interaction

    N2 = N2_(r)
    s = ((r / R) < x_atm) & (N2 > 0)

    for i in range(N_π):
        for j in range(i, N_π):
            # D[i,j] = trapz((4 * np.pi * r**2 * ρ * (π.ξ[i] * π.ξ[j] + Λ2 * π.ξh[i] * π.ξh[j])), r) / Fππ * (-1)**(i+j)
            A[i,j] = -trapz((4 * np.pi * r**2 * ρ * (N2 * π.ξ[i] * π.ξ[j]))[s], r[s]) / Fππ * (-1)**(i+j)
            if i == j:
                A[i,j] -= π.ω[i]**2
            else:
                A[j,i] = A[i,j]


    # γ self-interaction
    if γ is not None:

        r = γ.r
        ρ = γ.ρ0

        f0 = γ.ρ0 / γ.Γ1 / γ.P0
        g = g_(r)
        N2 = N2_(r)
        N2g = N2/g
        N2g[0] = 0

        s = (r > 0) & (N2 > 0)
        s_atm = ((r/R) > x_atm) & (N2 > 0)

        divρξ = [-γ.ξ[i] * N2g * ρ for i in range(N_γ)]
        Pcs = [γ.P[i] * ρ /γ.Γ1/γ.P0 for i in range(N_γ)]
        ρ1 = [Pcs[i] - divρξ[i] for i in range(N_γ)]

        for i in range(N_γ):
            A[N_π + i, N_π + i] = -γ.ω[i]**2 # better than nothing

        # modified wave operator (see discussion in Ong, Basu, Roxburgh 2021 in prep)

        QQ = [ρ * G*green_function_integral(r, -divρξ[i], l, grad=False) for i in range(N_γ)]
        gQQ = [g*divρξ[i] + ρ * G*green_function_integral(r, -divρξ[i], l, grad=True) for i in range(N_γ)]


        # perturbation only (gives similar results)

        # QQ = [ρ * G*green_function_integral(r, Pcs[i], l, grad=False) for i in range(N_γ)]
        # gQQ = [ρ * G*green_function_integral(r, Pcs[i], l, grad=True) for i in range(N_γ)]

        for i in range(N_γ - simple_γ):
            for j in range(N_γ - simple_γ):
                D[N_π+i,N_π+j] = trapz((4 * np.pi * r**2 * ρ * (γ.ξ[i] * γ.ξ[j] + Λ2 * γ.ξh[i] * γ.ξh[j])), r) / np.sqrt(Fγγ[i] * Fγγ[j])
                A[N_π+i,N_π+j] = trapz((4 * np.pi * r**2 * (gQQ[j] * γ.ξ[i] + Λ2 * QQ[j] * γ.ξh[i] / r))[s], r[s]) / np.sqrt(Fγγ[i] * Fγγ[j])

        # we now perform an unfortunately somewhat ad-hoc correction for nonorthogonality of
        # the γ-mode basis eigenfunctions. by ansatz we assert that the off-diagonal self-interaction terms
        # should be zero, since their values are close to the failure of the orthogonality relation
        # to close numerically. Those off-diagonal values that we do obtain should then yield estimates of
        # the integration error.

        Δ = np.zeros(N_γ)
        S = A[-N_γ:, -N_γ:]
        Δ[1:] += np.diag(S[1:])
        Δ[:-1] += np.diag(S[:,1:])
        Δ[1:-1] /= 2
        Δ1 = -γ.ω**2 - (np.diag(S) - Δ)

        Δ = np.zeros(N_γ)
        S = D[-N_γ:, -N_γ:]
        Δ[1:] += np.diag(S[1:])
        Δ[:-1] += np.diag(S[:,1:])
        Δ[1:-1] /= 2

        A[N_π:, N_π:] = 0
        for i in range(N_γ):
            A[N_π+i,N_π+i] = -γ.ω[i]**2 * (1 - Δ[i]) - Δ1[i]
            # A[N_π+i,N_π+i] -= -γ.ω[i]**2 * Δ[i]

        D[N_π:, N_π:] = np.identity(N_γ)

        # While this is close, it is unfortunately still insufficient to yield
        # γ-mode frequencies of sufficient accuracy to use for quantitative purposes,
        # compared to the frequency measurement error of 1 part in ~1e4. In particular,
        # if we truncate evaluation of the matrix at a finite number of terms (we can't
        # solve for all the g-modes), we necessarily commit some systematic error.
        #  However, we have to stop somewhere. Further corrections will have to be
        # approximate, and performed downstream.

    # cross terms

    B = np.zeros((N_π, N_γ))
    Δ = np.zeros((N_π, N_γ))

    # for validation purposes we compare the two different definitions
    # of the cross terms R_πγ in the coupling matrix, specified separately by the
    # R_π and R_γ wave operators. In my test runs I found the error to be
    # about one part in 1e2, which is then our estimate of both integration error
    # (small) as well as errors in the assumptions going into the formulation
    # (e.g. the provided boundary conditions don't actually yield an orthogonal
    # basis set). The latter dominates here, since this doesn't change much with more
    # sophisticated integration schemes or when oversampling the model.

    diff = np.zeros((N_π, N_γ))

    # s = ((r / R) < x_atm) & (N2g > 0) & (r > 0)
    s = (r > 0)
    # s = slice(None, None)

    # Vector components at the surface, for the purposes
    # of computing pseudoinertiae of the coupled modes

    ξrR = np.zeros(N_π + N_γ)
    ξhR = np.zeros(N_π + N_γ)

    for i in range(N_π):
        ξπr = UnivariateSpline(prune_double_points(π.r), π.ξ[i], **inter_opts)
        ξπh = UnivariateSpline(prune_double_points(π.r), π.ξh[i], **inter_opts)

        ξrR[i] = ξπr(R) / np.sqrt(Fππ) * (-1)**(i)
        ξhR[i] = ξπh(R) / np.sqrt(Fππ) * (-1)**(i)

        ξπr = ξπr(r)
        ξπh = ξπh(r)

        for j in range(N_γ):
            Δ[i,j] = trapz((4 * np.pi * r**2 * ρ * (ξπr * γ.ξ[j] + Λ2 * ξπh * γ.ξh[j]))[s], r[s]) / Fπγ[j] * (-1)**(i)
            B[i,j] = -trapz((4 * np.pi * r**2 * ρ * (N2_(r) * ξπr * γ.ξ[j]))[s], r[s]) / Fπγ[j] * (-1)**(i)
            B[i,j] -= Δ[i,j] * π.ω[i]**2
            diff[i,j] = B[i,j] - trapz((4 * np.pi * r**2 * (gQQ[j] * ξπr + Λ2 * QQ[j] * ξπh / r))[s], r[s]) / Fπγ[j] * (-1)**(i)

    if γ is not None:
        i_γ = np.argmin((γ.r - R)**2)

        for i in range(N_γ):
            ξrR[N_π + i] = γ.ξ[i][i_γ] / np.sqrt(Fγγ[i])
            ξhR[N_π + i] = γ.ξh[i][i_γ] / np.sqrt(Fγγ[i])

    D[:N_π, N_π:] = Δ
    D[N_π:, :N_π] = Δ.T

    A[:N_π, N_π:] = B
    A[N_π:, :N_π] = B.T

    # explicit symmetrisation

    A = 1/2*(A + A.T)
    D = 1/2*(D + D.T)

    L = np.copy(A)

    # approximate single-coupling matrix of the form of Ong & Basu 2020
    if γ is not None:
        for i in range(N_π):
            for j in range(N_γ):
                L[i, N_π + j] += Δ[i,j] * γ.ω[j]**2
                L[N_π + j, i] = L[i, N_π + j]

    # return results

    return {'A': A, 'D': D, 'L': L, 'diff': diff, 'ξr': ξrR, 'ξh': ξhR}

