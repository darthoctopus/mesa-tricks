#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
An asymptotic estimator for Δν (Ong and Basu 2019a)
'''

import numpy as np
from astropy import constants as c
from scipy.interpolate import interp1d
from scipy.integrate import quad
from scipy.optimize import bisect
from . import cs

def νac_isothermal(p):
    """
    Acoustic cutoff frequency: I use the simplified expression
    (which assumes an isothermal atmosphere)
    from Viani+ 2017 rather than the full one.

    Moreover, we can also use the analytic expression for H if we're
    assuming an isothermal atmosphere, as assert that H = H_p = P / (ρ g)

    Units of microhertz; We assume inputs are in cgs
    """
    g = np.nan_to_num(c.G.cgs *  p['m']/p['r']**2)
    Hρ = np.array(p['P'] / (g * p['ρ']))
    return 1e6 * np.array(cs(p) / (4 * np.pi * Hρ)) + 0j

def simple_Δν_quad(ν, profile, info, **kwargs):
    '''
    Compute the l=0 asymptotic estimator in Ong & Basu 2018
    using an adaptive Gauss-Kronrod scheme (to handle integrable
    singularities properly at the endpoints)
    '''
    R = np.float(info['R'])
    c = np.array(cs(profile), dtype=np.float128)
    νaclist = np.nan_to_num(np.array(νac_isothermal(profile), dtype=np.float128))
    r = np.array(profile['r'], dtype=np.float128)

    ω = 2 * np.pi * ν / 1e6 #in cgs units

    c_ = interp1d(r, c, **kwargs)
    νac_ = interp1d(r, νaclist, **kwargs)

    def Radicand1(r):
        return np.nan_to_num(1 - νac_(r)**2/ν**2) + 0j

    def Integrand1(r):
        return np.nan_to_num(1 / np.sqrt(Radicand1(r)) / c_(r))

    rmin = np.min(r)

    MID = np.nanargmax(Radicand1(r[(r/R) < 1])[1:])+1

    try:
        outerlim = bisect(Radicand1, r[MID], R)
    except:
        raise

    innerlim = rmin

    mm = np.nanargmin(Radicand1(r[:MID]))
    if np.sign(Radicand1(r[mm])) != np.sign(Radicand1(r[MID])):
        innerlim = bisect(Radicand1, r[mm], r[MID])

    T = quad(Integrand1, innerlim, outerlim, limit=1000)
    Δν1 = (1 / 2 / T[0]) * 1e6

    return Δν1