#!/usr/bin/env python3

## Author: Joel Ong <joelong@hawaii.edu>
## Institute for Astronomy, University of Hawaiʻi at Mānoa

'''
Various inversion kernels. I have chosen to cast these expressions
entirely in terms of ξr and ξh, rather than using the perturbation
to the gravitational potential, in order to make the symmetrisation
easier when it comes time to turn these into matrix elements.
'''

import numpy as np
from astropy import units as u, constants as c
from scipy.integrate import trapezoid, cumulative_trapezoid

G = c.G.cgs.value

def K_cs2(ω, r, ρ, cs, ξr, ξh, ell):
	'''
	Sound speed kernel K_{c_s², ρ}, with ρ being the cross term
	(i.e. this is ξ . ∂Λ/∂c² . ξ with the partial derivative taken
	keeping ρ constant, where Λ is the wave operator).

	Kernel satisfies δω/ω = \int K_cs2 δc²/c² \mathrm d r, ignoring δρ/ρ.
	Formula is taken from Gough & Thompson 1991, via Earl's thesis
	'''

	λ = ell * (ell + 1)
	χ = np.nan_to_num(np.gradient(ξr, r) + 2 * ξr/r - λ * ξh/r) # = div ξ

	# we do not necessarily assume unit normalization in the inputs
	E = trapezoid(ρ * r**2 * (ξr**2 + λ * ξh**2), r)

	# kernel is intended to be taken dr, rather than dr/R.
	# Multiply this by R if you want to plot the normalised
	# kernel integrated over x = r/R, or multiply by M/4πr^2ρ
	# if you want to plot this as a function of fractional mass
	# μ = m/M.

	return r**2 * ρ * cs**2 * χ**2 / 2 / ω**2 / E

def K_ρ(ω, r, ρ, cs, ξr, ξh, ell, orth=True):
	'''
	Density kernel K_{ρ, c_s²}, with c_s being the cross term
	(i.e. this is ξ . ∂Λ/∂ρ . ξ with the partial derivative taken
	keeping c_s constant, where Λ is the wave operator).

	Kernel satisfies δω/ω = \int K_ρ δρ/ρ \mathrm d r, ignoring δc/c.
	Formula is taken from Gough & Thompson 1991 (cf. eq 1.86 of Earl's
	PhD thesis.

	I am aware that other implementations, e.g. of InversionKit, use the
	gravitational potential perturbation directly, but this expression
	(with the Green's function integrals of the Coulomb potential)
	is numerically equivalent (up to negligible rounding error).
	'''

	λ = ell * (ell + 1)
	χ = np.nan_to_num(np.gradient(ξr, r) + 2 * ξr/r - λ * ξh/r) # = div ξ
	m = cumulative_trapezoid(4 * np.pi * r**2 * ρ, r, initial=0)
	F = cumulative_trapezoid(np.nan_to_num((χ + ξr * np.gradient(ρ, r)/ρ / 2) * ξr * ρ), r, initial=0)

	# compute gravitational potentials

	div_ρξ = ρ * χ + ξr * np.gradient(ρ, r)
	# Green's function integrals
	G1 = cumulative_trapezoid(np.nan_to_num(div_ρξ * r**(ell + 2)), r, initial=0)
	G2 = cumulative_trapezoid(np.nan_to_num(div_ρξ * r**(1 - ell)), r, initial=0)

	# we do not necessarily assume unit normalization in the inputs
	E = trapezoid(ρ * r**2 * (ξr**2 + λ * ξh**2), r)

	k = (
			- ρ * r**2 * (ξr**2 + λ * ξh**2) / 2 * ω**2
			+ ρ * r**2 * cs**2 * χ**2 / 2
			- G * m * ρ * χ * ξr # a term here cancels a term on line marked (*)
			- 4 * np.pi * G * ρ * r**2 * (F[-1] - F)
			+ G * m * ρ * ξr * np.gradient(ξr, r)
			+ G * (4 * np.pi * r**2 * ρ**2) * ξr**2 / 2 # (*)
			- 4 * np.pi * G / (2 * ell + 1) * ρ * (
				(ell + 1) * (r**(-ell)) * (ξr - ell * ξh) * G1
				- ell * r**(ell + 1) * (ξr + (ell + 1) * ξh) * (G2[-1] - G2)
			)
		)

	# kernel is intended to be taken dr, rather than dr/R.
	# Multiply this by R if you want to plot the normalised
	# kernel integrated over x = r/R, or multiply by M/4πr^2ρ
	# if you want to plot this as a function of fractional mass
	# μ = m/M.

	k = np.nan_to_num(k) / E / ω**2

	if not orth:
		return k

	# HACK: it seems like for some reason Earl and InversionKit are both returning
	# not the kernel itself, but the orthogonal component to the inner product measure
	# in the radial coordinate.

	v = ρ * r**2
	α = trapezoid(k * v, r) / trapezoid(v * v, r)
	return k - α * v

def K_ρ_Γ1(ω, r, ρ, Γ1, cs, ξr, ξh, ell, orth=True):
	'''
	Density kernel where the conjugate quantity is the adiabatic
	index Γ₁, rather than the sound speed. This is eq. 1.94 of Earl's
	thesis, with extra hacks used by InversionKit to make the kernel
	of zero mean??? This isn't super well explained though, and I am
	not very comfortable with it.

	If I have the time I might reimplement using Sasha Kosovichev's
	expression (eqs. 18, 19 of Kosovichev 1999, JCAM, 109, 1) instead.
	'''

	λ = ell * (ell + 1)
	χ = np.nan_to_num(np.gradient(ξr, r) + 2 * ξr/r - λ * ξh/r) # = div ξ
	m = cumulative_trapezoid(4 * np.pi * r**2 * ρ, r, initial=0)

	E = trapezoid(ρ * r**2 * (ξr**2 + λ * ξh**2), r)

	I1 = cumulative_trapezoid(Γ1 * χ**2 * r**2, r, initial=0) / (2 * E * ω**2)
	I2 = cumulative_trapezoid(np.nan_to_num(4 * np.pi * G * ρ / r**2)  * I1, r, initial=0)

	k =  (
		K_ρ(ω, r, ρ, cs, ξr, ξh, ell, orth=False) - K_cs2(ω, r, ρ, cs, ξr, ξh, ell)
		+ np.nan_to_num(G * m * ρ / r**2) * I1
		+ ρ * r**2 * (I2[-1] - I2)
	)

	if not orth:
		return k

	# HACK: it seems like for some reason Earl and InversionKit are both returning
	# not the kernel itself, but the orthogonal component to the inner product measure
	# in the radial coordinate.

	v = ρ * r**2
	α = trapezoid(k * v, r) / trapezoid(v * v, r)
	return k - α * v