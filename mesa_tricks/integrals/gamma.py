#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

r'''
Numerical shooting to derive γ modes
(per isolation scheme of Ong & Basu 2020a).
Boundary conditions adopted are slightly different;
I've found that setting y4 = 0 at the outer boundary will
yield the correct radial order. This is easy to justify post-hoc
but somewhat more difficult to derive ab initio (and I have not been able
to do so). Integration is carried out in terms of the buoyancy radial coordinate
φ = f/F, where f_l = \int N Λ dr/r; this avoids the coordinate singularity that occurs
in the convective envelope, and is normalised so that the integration variable is independent
of degree.
'''

import numpy as np
from scipy.interpolate import UnivariateSpline
from scipy.integrate import solve_ivp, solve_bvp
from astropy import units as u, constants as c
from scipy.optimize import bisect
from tqdm.auto import tqdm
from . import safe_cumulative_trapezoid
from .ε_g import ff
from ..utils import tqdm_parallel_map

class GammaProblem:
    def __init__(self, gyre, **kwargs):

        P = np.array(gyre['P'])
        ρ = np.array(gyre['ρ'])
        r = np.array(gyre['r'])
        m = np.array(gyre['m'])
        g = np.nan_to_num(c.G.cgs.value * m / r**2)
        Γ1 = np.array(gyre['Γ1'])

        R = r[-1]
        M = m[-1]

        # dimensionless quantities
        As = np.nan_to_num(gyre['N2'] * r / g)
        V = np.nan_to_num(ρ * r * g / P)
        U = np.array(4 * np.pi * r**3 * ρ / m)
        c1 = (r/R)**3 / (m/M)

        if r[0] == 0:
            As[0] = 0
            V[0] = 0
            U[0] = 3
            c1[0] = M / R**3 / (4/3 * np.pi * ρ[0])

        #buoyancy coordinate system

        f0 = ff(gyre)
        F0 = np.nanmax(f0)
        φ = f0 / F0
        mask = ~np.isnan(φ)

        # scale factor: x d/dx = N/F0 d/dφ === S(φ) d/dφ
        S = np.array(np.sqrt(gyre['N2']) / F0)

        # Integral measures: \int (ξ_r^2 + Λ^2 ξ_h^2) dm
        # === \int (y_1^2 + Λ^2 ( (y_2 + y_3) / c_1 ω^2 )^2) M_l(φ) / S(φ) dφ
        # M_l = m * U * np.power(r,2 * (1 + l - 2)) * np.power(R, 2 * (2 - l))
        M1 = m * U * np.power(R, 2)

        # Characteristic frequency (squared)
        W02 = (c.G.cgs.value * M / R**3)

        # Display scale factor
        I1 = np.array(np.sqrt(ρ * r**3 * np.sqrt(gyre['N2'])))

        # Scale factor for pressure eigenfunction (to cgs units)
        Q = ρ * r * g / (r/R)**2
        if r[0] == 0:
            Q[0] = ρ[0]**2 * c.G.cgs.value * 4 * np.pi / 3 * R**2

        # interpolants

        inter_kwargs = {**dict(k=3, s=.0), **kwargs}

        
        ρ_ = UnivariateSpline(φ[mask], ρ[mask], **inter_kwargs)
        P_ = UnivariateSpline(φ[mask], P[mask], **inter_kwargs)

        As_ = UnivariateSpline(φ[mask], As[mask], **inter_kwargs)
        U_ = UnivariateSpline(φ[mask], U[mask], **inter_kwargs)
        V_ = UnivariateSpline(φ[mask], V[mask], **inter_kwargs)
        c1_ = UnivariateSpline(φ[mask], c1[mask], **inter_kwargs)
        Γ1_ = UnivariateSpline(φ[mask], Γ1[mask], **inter_kwargs)
        S_ = UnivariateSpline(φ[mask], S[mask], **inter_kwargs)
        I1_ = UnivariateSpline(φ[mask], I1[mask], **inter_kwargs)
        M1_ = UnivariateSpline(φ[mask], M1[mask], **inter_kwargs)
        Q_ = UnivariateSpline(φ[mask], Q[mask], **inter_kwargs)

        r_ = UnivariateSpline(φ[mask], r[mask], **inter_kwargs)
        m_ = UnivariateSpline(φ[mask], m[mask], **inter_kwargs)

        # Inverse coordinate transform
        φ_ = UnivariateSpline(r[mask], φ[mask], **inter_kwargs)

        def I_(φ, l):
            return I1_(φ) * np.power(r_(φ), l - 1)
        def M_(φ, l):
            return M1_(φ) * np.power(r_(φ)/R, 2 * (l - 1))

        def RHS(φ, y, w, l):
            Λ = np.sqrt(l * (l + 1))
            ω = np.float(w)
            return np.array([
                (V_(φ)/Γ1_(φ) - 1 - l) * y[0] + Λ**2 / c1_(φ) / ω**2 * y[1] +  Λ**2 / c1_(φ) / ω**2 * y[2],
                (c1_(φ) * ω**2 - As_(φ)) * y[0] + (3 - U_(φ) + As_(φ) - l) * y[1] - y[3],
                (3 - U_(φ) - l) * y[2] + y[3],
                (As_(φ) * U_(φ)) * y[0] + (V_(φ)/Γ1_(φ) * U_(φ)) * y[1] + Λ**2 * y[2] -(U_(φ) + l - 2) * y[3]
            ]) / S_(φ)

        def BC(ya, yb, ω):
            return np.array([
                ya[0], ya[3],
                yb[0], yb[3],
                yb[1]-1
                # unit pressure at surface
                # for our solve_bvp solutions
                ])

        # Store things to class namespace
        self.RHS = RHS
        self.BC = BC
        self.F0 = F0
        self.W02 = W02
        self.φ = φ
        self.r = r
        self.R = R
        self.M = M
        self.interpolants = {
            'ρ': ρ_, 'P': P_, 'As': As_, 'U': U_, 'V': V_, 'c1': c1_,
            'Γ1': Γ1_, 'S': S_, 'I': I_, 'M': M_, 'Q': Q_, 'r': r_, 'φ': φ_
        }

        # Maximum Brunt-Vaisala frequency scaled to dimensionless units
        self.ω_max = np.nanmax(np.sqrt(As[mask] / c1[mask]))

        # Convert between dimensionless units and microhertz
        self.ν_per_ω = np.sqrt(W02) * 1e6 /2 / np.pi

    def integrate_single(self, ω0, l, φ_match=0.5, z0=1., **kwargs):
        '''
        Integrate the pulsation equations inwards and outwards
        with respect to gravitational boundary conditions: ξ = 0 and Φ₁' = 0
        at both the inner and the outer boundary.
        '''
        def fun(*args):
            return self.RHS(*args, ω0, l)

        ivp_kwargs = {**dict(method='Radau'), **kwargs}
        
        res = []

        φ0 = self.φ[0] if self.r[0] != 0 else self.φ[1]

        res.append(solve_ivp(fun, [φ0, φ_match], [0, z0, 0, 0], **ivp_kwargs))
        res.append(solve_ivp(fun, [φ0, φ_match], [0, 0, z0, 0], **ivp_kwargs))
        res.append(solve_ivp(fun, [1, φ_match], [0, z0, 0, 0], **ivp_kwargs))
        res.append(solve_ivp(fun, [1, φ_match], [0, 0, z0, 0], **ivp_kwargs))
        
        return res

    def bvp_single(self, ω0, l, N_mesh=300, **kwargs):
        def fun(*args):
            return self.RHS(*args, l)

        φ_guess = np.linspace(self.φ[1], np.nanmax(self.φ), N_mesh)
        guess = np.ones((4, N_mesh))

        kwargs = {**dict(tol=1e-6, max_nodes=10000), **kwargs}
        res = solve_bvp(fun, self.BC, φ_guess, guess, [ω0], **kwargs)
        return res

    @staticmethod
    def Dmat(res, **kwargs):
        '''
        Solution matrix at the matching point. Each column corresponds to one
        set of solutions. We want a linear combination of columns such that
        the sum of solutions at the matching point is zero; in that case,
        we have C1i y1i + C2i y2i = - (C1o y1o + C2o y2o) such that the LHS
        describes the solution in the interior, and the RHS describes the solution
        in the exterior. Should such a linear combination exist, det D = 0.

        This is the same method as employed by ADIPLS, except that (see above)
        we are using the boundary conditions y1=0, y4=0 instead of having y2 and
        y4 determined by regularity/other considerations.
        '''
        return np.array([
            _['y'][:, -1] for _ in res
        ]).T

    def D(self, ω, l, **kwargs):
        '''
        Discriminant function det D of the γ-pulsation equations. Eigenvalues
        occur at roots of this function.
        '''
        return np.linalg.det(self.Dmat(self.integrate_single(ω, l, **kwargs)))

    def grid_bracket(self, min_ν, max_ν, l, N_grid=50, N_threads=None, **kwargs):
        '''
        Search for sign changes in the discriminant function $D$ over a specified grid
        of frequencies. min_ν and max_ν are assumed to be in microhertz.
        '''

        ω_grid = (1 / np.linspace(1/min_ν, 1/max_ν, N_grid)) / self.ν_per_ω
        if not N_threads:
            disc = [self.D(ω, l, **kwargs) for ω in tqdm(ω_grid, desc=f"Bracketing grid (l = {int(l)})")]
        else:
            def fun(ω):
                return self.D(ω, l, **kwargs)
            disc = tqdm_parallel_map(fun, ω_grid, nthreads=N_threads, desc=f"Bracketing grid (l = {int(l)})")

        brackets = []
        for i in range(N_grid - 1):
            if disc[i] * disc[i+1] < 0:
                brackets.append((ω_grid[i], ω_grid[i+1]))

        return {
            'ω_grid': ω_grid,
            'disc': disc,
            'brackets': brackets,
            'l': l,
            'kwargs': kwargs
        }

    def bracket_search(self, pair, l, bisect_kwargs=None, **kwargs):
        '''
        Search for root of discriminant function within specified interval.
        bisect_kwargs are passed to bisect(), while kwargs are passed to
        D. `pair` is a tuple of (lower, upper) angular frequency
        in dimensionless units.
        '''

        if bisect_kwargs is None:
            bisect_kwargs = {}

        bisect_kwargs = {**dict(xtol=1e-100), **bisect_kwargs}
        return bisect((lambda ω: self.D(ω, l, **kwargs)), *pair, **bisect_kwargs)

    def grid_search(self, min_ν, max_ν, l, N_grid=50, N_threads=None, bisect_kwargs=None, **kwargs):
        '''
        Convenience function to combine grid_bracket and bracket_search
        '''

        brackets = self.grid_bracket(min_ν, max_ν, l, N_grid, N_threads=N_threads, **kwargs)
        if not N_threads:
            ω0 = [self.bracket_search(pair, l, bisect_kwargs, **kwargs)
                    for pair in tqdm(brackets['brackets'], desc=f"Finding eigenvalues (l = {int(l)})")]
        else:
            def fun(pair):
                return self.bracket_search(pair, l, bisect_kwargs, **kwargs)
            ω0 = tqdm_parallel_map(fun, brackets['brackets'],
                                   nthreads=N_threads, desc=f"Finding eigenvalues (l = {int(l)})")
        brackets['ω'] = ω0
        return brackets

    def grid_bvp(self, min_ν, max_ν, l, N_grid=50, N_threads=None, bracket_kwargs=None, **kwargs):
        if bracket_kwargs is None:
            bracket_kwargs = {}
        bracket_kwargs = {**dict(method='RK45', max_step=1), **bracket_kwargs}
        brackets = self.grid_bracket(min_ν, max_ν, l, N_grid, N_threads=N_threads, **kwargs)
        if not N_threads:
            ω_guess = [np.mean(_) for _ in brackets['brackets']]
            results = [self.bvp_single(ω0, l, **kwargs) for ω0 in ω_guess]
        else:
            def fun(pair):
                ω0 = np.mean(pair)
                return self.bvp_single(ω0, l, **kwargs)
            results = tqdm_parallel_map(fun, brackets['brackets'],
                                   nthreads=N_threads, desc=f"Solving eigensystem (l = {int(l)})")

        # reformat output
        ω = [_['p'][0] for _ in results]
        sols = [_['sol'] for _ in results]
        ξ_r = [self.ξ_r(sol, l) for sol in sols]
        ξ_h = [self.ξ_h(sol, _, l) for _, sol in zip(ω, sols)]
        P1 = [self.P1(sol, l) for sol in sols]
        return {
            'y': sols,
            'ξ_r': ξ_r,
            'ξ_h': ξ_h,
            'P1': P1,
            'ω': ω,
            'status': [_['status'] for _ in results],
            'niter': [_['niter'] for _ in results],
            'success': [_['success'] for _ in results],
            'n_mesh': [len(_['x']) for _ in results]
        }


    @staticmethod
    def y(res, φ_match=0.5, **kwargs):
        '''
        The supplied result from integrate_single *must* be run with
        `dense_output=True`.

        Produces a callable that returns the dynamical variables $y$.
        '''
        λ, C = np.linalg.eig(GammaProblem.Dmat(res))
        i0 = np.argmin(np.abs(np.real(λ)))
        coeffs = C[:, i0]

        def sol(φ):
            return np.where(φ < φ_match,
                            coeffs[0]*res[0]['sol'](φ) + coeffs[1]*res[1]['sol'](φ),
                            -coeffs[2]*res[2]['sol'](φ) - coeffs[3]*res[3]['sol'](φ),
                           )
        return sol

    def ξ_r(self, y, l):
        '''
        Return a callable turning y1 into ξ_r:
        ξ_r = r y1 x^(l - 2)
        '''

        r_ = self.interpolants['r']
        R = self.R

        def fun(φ):
            y0 = y(φ)
            y1 = y0[0]
            return np.power(r_(φ)/R, l-1) * R * y1

        return fun

    def ξ_h(self, y, ω, l, α_γ=1):
        '''
        Return a callable turning y2, y3 into ξ_h:
        ξ_h = r x^(l - 2) ((1 + (1-α_γ) V/Γ1 c1 ω^2 / Λ^2 )y2 + y3) / c1 ω^2

        NOTE: The γ-mode wave operator does NOT change the definition of ξ_h.
        That is to say, the relation ξ_h = 1/rω^2(P/ρ + φ1) is NOT modified
        by the γ-mode wave operator. What does change is the relation
        L(ξ_h) = -ω^2 ξh + R(ξ_h), which is given by the additional term below.
        The requisite matrix elements should therefore be computed with respect to the
        unmodified horizontal components of the eigenfunction.

        By numerical experiment, it is clear that changing the definition of ξ_h
        breaks orthogonality of the returned eigenfunctions (bad).
        '''

        V_ = self.interpolants['V']
        Γ1_ = self.interpolants['Γ1']
        c1_ = self.interpolants['c1']
        Λ2 = l * (l + 1)
        r_ = self.interpolants['r']
        R = self.R
        
        def fun(φ):
            y0 = y(φ)
            y2 = y0[1]
            y3 = y0[2]

            X = c1_(φ) * ω**2
            return np.power(r_(φ)/R, l-1) * R * ((1 + (1 - α_γ) * V_(φ) / Γ1_(φ) * X / Λ2) * y2 + y3) / (X)

        return fun

    def P1(self, y, l):
        '''
        Return a callable turning y2 into P1:
        P1 = P x^(l-2) V y_2
        '''

        Q_ = self.interpolants['Q']
        r_ = self.interpolants['r']
        R = self.R
        
        def fun(φ):
            y0 = y(φ)
            y2 = y0[1]

            return np.power(r_(φ)/R, l) * Q_(φ) * y2

        return fun

    def Φ(self, y, l):
        '''
        Return a callable turning y2 into P1:
        Φ = P x^(l-2) V y_3 ρ
        '''

        Q_ = self.interpolants['Q']
        r_ = self.interpolants['r']
        ρ_ = self.interpolants['ρ']
        R = self.R
        
        def fun(φ):
            y0 = y(φ)
            y3 = y0[2]

            return np.power(r_(φ)/R, l) * Q_(φ) * y3 / ρ_(φ)

        return fun

    def eigenfunctions(self, ω, l, N_threads=None, **kwargs):
        kwargs = {**dict(dense_output=True), **kwargs}
        
        ω = np.array(ω).flatten()
        l = int(l)

        if not N_threads:
            ress = [self.integrate_single(_, l, **kwargs) for _ in ω]
        else:
            def fun(ω):
                return self.integrate_single(ω, l, **kwargs)
            ress = tqdm_parallel_map(fun, ω, nthreads=N_threads, desc="Integrating eigenfunctions")

        sols = [self.y(res, **kwargs) for res in ress]
        ξ_r = [self.ξ_r(sol, l) for sol in sols]
        ξ_h = [self.ξ_h(sol, _, l) for _, sol in zip(ω, sols)]
        P1 = [self.P1(sol, l) for sol in sols]
        Φ = [self.Φ(sol, l) for sol in sols]
        
        return {
            'y': sols,
            'ξ_r': ξ_r,
            'ξ_h': ξ_h,
            'P1': P1,
            'Φ': Φ
        }

def green_function_integral_gamma_1(r, f, l, R=None, grad=False):
    r'''
    Integral against point-source kernel, satisfying the outer boundary condition
    f = 0 at r - R.
    '''

    if R is None:
        R = r[-1]

    x = r / R

    if not grad:
        # integral from 0 to r
        I1 = (np.power(x, -1-l) - np.power(x,l)) * safe_cumulative_trapezoid(f * np.power(x, l+2), x)
        # integral from r to R
        I2 = np.power(x, l) * (lambda y: y[-1] - y)(safe_cumulative_trapezoid(f * (-np.power(x, l+2) + np.power(x, 1-l)), x))

        return 4 * np.pi/ (2 * l + 1) * (I1 + I2) * R**2
    else:
        # integral from 0 to r
        I1 = (-(1+l) * np.power(x, -2-l) - l * np.power(x, l-1)) * safe_cumulative_trapezoid(f * np.power(x, l+2), x)
        # integral from r to R
        I2 = l * np.power(x, l-1) * (lambda y: y[-1] - y)(safe_cumulative_trapezoid(f * (-np.power(x, l+2) + np.power(x, 1-l)), x))

        return 4 * np.pi/ (2 * l + 1) * (I1 + I2) * R

def green_function_integral_gamma_2(r, f, l, R=None, grad=False):
    r'''
    Integral against point-source kernel, satisfying the outer boundary condition
    df/dr = 0 at r - R.
    '''

    if R is None:
        R = r[-1]

    x = r / R

    if not grad:
        # integral from 0 to r
        I1 = (np.power(x, -1-l) + (l+1)/l*np.power(x,l)) * safe_cumulative_trapezoid(f * np.power(x, l+2), x)
        # integral from r to R
        I2 = np.power(x, l) * (lambda y: y[-1] - y)(safe_cumulative_trapezoid(f * ((l+1)/l * np.power(x, l+2) + np.power(x, 1-l)), x))

        return 4 * np.pi/ (2 * l + 1) * (I1 + I2) * R**2
    else:
        # integral from 0 to r
        I1 = (-(1+l) * np.power(x, -2-l) + (l+1) * np.power(x, l-1)) * safe_cumulative_trapezoid(f * np.power(x, l+2), x)
        # integral from r to R
        I2 = l * np.power(x, l-1) * (lambda y: y[-1] - y)(safe_cumulative_trapezoid(f * ((l+1)/l * np.power(x, l+2) + np.power(x, 1-l)), x))

        return 4 * np.pi/ (2 * l + 1) * (I1 + I2) * R