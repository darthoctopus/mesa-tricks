#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Convenience functions for evaluating isochrones,
given an EEP coordinate transformation.

Very rough implementation of methods presented in
Dotter 2016 (ApJS).

Note that I do not actually furnish a means by which
to identify EEP fixed points; this is left as an exercise
for the user. However, I've found empirically that TAHB
to ZAMS is functionally a pretty good interpolation plane.

e.g. in my ε-Δν isochrone paper, I use
$$φ(M, t) = (1 - t/t_TAHB)^(1/3)$$
as a single-phase parameterisation of the EEP plane.
'''

import numpy as np
from scipy.interpolate import griddata, UnivariateSpline
from scipy.optimize import bisect

id = lambda M, x: x

# I/O things: get rid of backtracks

def mask_backtrack(t):
    reference = np.minimum.accumulate(t[::-1])[::-1]
    return t == reference

def fix_backtrack(t, y):
    '''
    Returns t, y with backtrack entries (owing to MESA retries) masked out
    '''
    t = np.array(t)
    m1 = mask_backtrack(t)
    m2 = np.concatenate([[True], np.diff(t[m1]) > 0])
    return t[m1][m2], y[m1][m2]

# math

def transform(M, age, trace=False, EEP=id):
    '''
    Transform mass and age to an appropriate point on the M-EEP plane
    for interpolation, given a function of the kind
    φ: (M, age) ↦ x ∈ U ⊂ R (e.g. unit interval).
    
    Dotter+ 2016 refers to this as a "metric" but we're really using
    the induced metric (i.e. usual metric in M-EEP plane, pulling back under φ).
    
    This function also masks out NaNs, optionally returning the mask.
    '''
    q = EEP(M, age)
    m = ~np.isnan(q)
    if not trace:
        return M[m], q[m]
    return M[m], q[m], m

# isochrone computation object

class IsochroneContainer:
    '''
    Provide isochrone generation utility functions given:
    - masses: 1D array of masses
    - ages: list of 1D arrays of ages (same lemgth of masses)
    '''

    def __init__(self, masses, ages, n_HB=0, EEP=id):
        self._EEP = EEP
        self.ages = ages

        # max age interpolant for mass
        max_ages = [np.max(a) for a in ages]
        self.max_age = UnivariateSpline(masses[n_HB:], max_ages[n_HB:])

        # max mass interpolant for age
        max_mass_int_ = UnivariateSpline(max_ages[n_HB:][::-1], masses[n_HB:][::-1])
        def max_mass_single(age):
            guess = max_mass_int_(age)
            return bisect(lambda m: age - self.max_age(m), guess-2, guess+2)
        self.max_mass = np.vectorize(max_mass_single)

        # min mass for age
        self.min_mass = np.min(masses)

        # grid data for interpolation with griddata
        temp = [fix_backtrack(a, np.ones_like(a)) for a in self.ages]
        self.agegrid = np.concatenate([q[0] for q in temp])
        self.massgrid = np.concatenate([M * q[1] for M, q in zip(masses, temp)]) # I keep this around for record keeping purposes
        
        self.M, self.q, self.m = transform(self.massgrid, self.agegrid, trace=True, EEP=self.EEP)

    @property
    def EEP(self):
        return self._EEP
    
    @EEP.setter
    def EEP(self, EEP):
        self._EEP = EEP
        self.M, self.q, self.m = transform(self.massgrid, self.agegrid, trace=True, EEP=self.EEP)

    def points(self, age, min_mass=None, ε=1e-9, n_points=2000, **kwargs):
        '''
        Given an age, return points on the M-EEP plane corresponding
        to the isochrone.
        
        The safety factor ε is a numerical hack — not entirely sure why this works,
        but for (small) values of ε it ensures that the isochrone reaches all the way to the HB
        (most visually obvious with the log map φ(M, age) ↦ log(TAHB(M) - age))
        '''

        if min_mass is None:
            min_mass = self.min_mass

        masses = np.linspace(min_mass, self.max_mass(age)-ε, n_points)
        return np.asarray([*transform(masses, np.ones_like(masses) * age, EEP=self.EEP)]).T

    def quantity_grid(self, quantity):
        assert np.all([len(a) == len(q) for q,a in zip(quantity, self.ages)]), "Quantity length mismatch"
        return np.concatenate([fix_backtrack(a, q)[1] for a, q in zip(self.ages, quantity)])

    def __call__(self, age, quantity, int_kwargs=None, **kwargs):
        '''
        Returns the isochrone after using (linear) interpolation.
        More sophisticated interpolation schemes can be used,
        but on my machine (32 GB memory) I get a MemoryError when attempting
        to instantiate an RBF interpolation, so no go.

        quantity is assumed to be of the same format as the "ages" variable
        used to instantiate the class.

        Additional keyword arguments are passed to self.points (e.g. n_points, min_mass).
        Interpolation kwargs are issued via passing a dict int_kwargs.
        '''

        if int_kwargs is None:
            int_kwargs = {}

        quantity_grid = self.quantity_grid(quantity)

        #explicitly mask out NaNs
        m = ~np.isnan(quantity_grid[self.m])

        if np.isscalar(age):
            return griddata(np.asarray([self.M[m], self.q[m]]).T, quantity_grid[self.m][m], self.points(age, EEP=self.EEP, **kwargs), **int_kwargs)
        else:
            return [griddata(np.asarray([self.M[m], self.q[m]]).T, quantity_grid[self.m][m], self.points(a, EEP=self.EEP, **kwargs), **int_kwargs) for a in age]
