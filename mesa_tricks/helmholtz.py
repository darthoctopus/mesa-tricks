#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Helmholtz frequency estimator for ν_max,
which does not rely on scaling relations.
'''

import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import solve_ivp, quad
from scipy.optimize import bisect
from astropy import units as u, constants as c

class HelmholtzProblem:

    def __init__(self, info, gyre, f=None, f1=0, f2=0, N_atm=700):
        '''
        Input: gyre dict and dataframe from the io module

        Parameters:
        f: Thickness of Helmholtz boundary layer in units of pressure scale heights at the SAL
        (if None, will use the zero point of the pressure eigenfunction as the outer boundary)
        
        f1, f2: distance (in units of H_p at SAL) by which to move the boundaries of the resonant layer
        radially inwards/outwards with respect to the canonical definition.
        e.g. f1 > 0 moves the inner boundary inwards, and f2 > 0 moves the outer boundary outwards

        N_atm: Number of mesh points to consider to be part of the atmosphere
        '''

        self.info = info
        self.gyre = gyre
        self.N_atm = N_atm

        self.data = self._prepare(info, gyre)#, info_p, profile)
        self.res = self._integrate(self.data, ivp_opts={'max_step': 1})
        
        acc = {}
        
        # Compute Resonator frequency
        
        try:
            s_atm = slice(-N_atm, None)
            self.ξ_, self.P1_ = self._convert_func(self.res, self.info, self.data, N_atm=N_atm)
            self.ρ_ = interp1d(self.data['r'][s_atm], self.data['ρ'][s_atm], kind='cubic')

            r_SAL, r_P1 = self.bounds()

            # compute pressure scale height at the SAL
            g = c.G.cgs.value * gyre['m'] / gyre['r']**2
            H_ = interp1d(self.data['r'][s_atm], np.array(gyre['P'] / (gyre['ρ'] * g))[s_atm])
            H0 = H_(r_SAL)

            if f is not None:
                # Set thickness of resonant layer
                r_P1 = r_SAL + f * H0
            else:
                # Shift boundaries of resonant layer
                r_SAL -= f1 * H0
                r_P1 += f2 * H0

            self.result = self.ν_H(r_SAL, r_P1)

        except ValueError:
            self.result = np.nan

    def bounds(self, N_SAL=2):
        '''
        Integral boundaries for the Helmholtz frequency
        '''

        data = self.data

        #superadiabatic gradient
        SAG = data['SAG']
        
        s_atm = slice(-self.N_atm, None)

        i_SAL = np.argmax(SAG[s_atm])
        r_SAL = float(data['r'][s_atm][i_SAL])
        x_SAL = data['x'][s_atm][i_SAL]
        i_SAL = np.argmin((data['r'] - r_SAL)**2)
        
        r_P1 = bisect(self.P1_, r_SAL, data['r'][-1])
        
        # refine the radius of the SAL by fitting a local parabola
        
        s = slice(i_SAL - N_SAL, i_SAL + N_SAL + 1)
        y = data['SAG'][s]
        x = data['r'][s]
        p = np.polyfit(x, y, 2)
        r_SAL = -p[1] / 2 / p[0]
        
        return r_SAL, r_P1

    def ν_H(self, r1, r2, outside=False):

        P1 = self.P1_
        ρ = self.ρ_
        ξ = self.ξ_

        def Integrand(r):
            return ρ(r) * ξ(r)

        if not outside:
            # turbulence on the inside of the interfacial layer
            return np.sqrt(P1(r1) / quad(Integrand, r1, r2)[0]) * 1e6 / 2 / np.pi
        else:
            # turbulence on the outside of the interfacial layer
            return np.sqrt(- P1(r2) / quad(Integrand, r1, r2)[0]) * 1e6 / 2 / np.pi

    @staticmethod
    def _prepare(info, gyre, 
                 info_p=None, profile=None, superadiabatic=True, inter_opts=None):
        '''
        Prepare everything we need to perform the IVP integration to find
        the pressure and displacement eigenfunctions.

        Inputs
        ======
        info, gyre: output of io.read_gyre
        info_p, profile: output of io.read_profile
        superadiabatic: whether or not to mask out superadiabatic region to N^2 = 0
        inter_opts: dict to pass as kwargs to interp1d (only needed if MESA profile is passed)


        Outputs
        =======
        dict containing various compatible 1D numpy arrays.
        '''

        m = np.array(gyre['m'])
        ρ = np.array(gyre['ρ'])
        P = np.array(gyre['P'])
        r = np.array(gyre['r'])
        Γ1 = np.array(gyre['Γ1'])

        g = c.G.cgs.value * m / r**2
        g[0] = 0
        N2 = np.array(gyre['N2'])
        
        # Dimensionless coordinate
        x = np.array(gyre['r'] / float(info['R']))
        
        # Dimensionless quantities:
        # V_g = V/Γ₁
        
        Vg = np.array(r * ρ * g / P) / Γ1
        Vg[0] = 0
        
        # U
        U = np.array(4 * np.pi * r**3 * ρ / m)
        U[0] = 3
        
        # A (Ledoux discriminant)
        As = np.array(N2 * r / g)
        As[0] = 0

        if not superadiabatic:
            As[As < 0] = 0

        acc = {'m': m, 'r': r, 'ρ': ρ,'P': P, 'g': g, 'x': x,
            'Vg': Vg, 'U': U, 'As': As, 'SAG': np.array(gyre['∇'] - gyre['∇ad'])}

        if info_p is not None and profile is not None:
            if inter_opts is None:
                inter_opts = {}
            inter_opts = {'kind': 'cubic', 'fill_value': 0, 'bounds_error': False, **inter_opts}
            x0 = np.power(10, profile['logR']) / info_p['photosphere_r']
            vc = interp1d(x0, profile['conv_vel'], **inter_opts)(x)
            Pturb = 2/3 * ρ * vc**2

            acc['vc'] = vc
            acc['Pturb'] = Pturb

        return acc

    @staticmethod
    def _integrate(data, y20=1e-5, ε=1e-15, inter_opts=None, ivp_opts=None):
        
        '''
        Integrate IVP to obtain pressure eigenfunction

        Inputs
        ======

        data: output of self._prepare
        y20: value of pressure eigenfunction at r=0 (heuristically)
        ε: initial stepsize over which forward Euler method is applied
           to avoid coordinate singularity at r=0
        inter_opts: dict to pass as kwargs to interp1d
        ivp_opts: dict to pass as kwargs to solve_ivp

        '''
        if inter_opts is None:
            inter_opts = {}
        inter_opts = {'kind': 'linear', **inter_opts}
        if ivp_opts is None:
            ivp_opts = {}
        ivp_opts = {'method': 'Radau', 'dense_output': True, 'max_step': 1e-4, **ivp_opts}

        Vg_ = interp1d(data['x'], data['Vg'], **inter_opts)
        U_ = interp1d(data['x'], data['U'], **inter_opts)
        As_ = interp1d(data['x'], data['As'], **inter_opts)

        def f(x, y):
            return np.array([[Vg_(x) - 1, -Vg_(x)], [U_(x) - As_(x), 3 - U_(x) + As_(x)]]) @ np.array(y) / x

        y10 = (y20 * Vg_(0) / (Vg_(0) - 3))
        return solve_ivp(f, [ε, np.max(data['x'])], [y10, y20], **ivp_opts)

    @staticmethod
    def _convert(res, info, data):
        '''
        Convert IVP solution into physical units

        Inputs
        ======

        res: solve_ivp output
        info: gyre info dict
        data: output from self._prepare

        Outputs:
        Dict of compatible numpy arrays
        '''
        y1, y2 = res['sol'](data['x'])
        r = data['r']
        m = data['m']
        x = data['x']
        ρ = data['ρ']
        g = data['g']
        R = float(info['R'])
        
        ξ = np.nan_to_num(r / x**2 * y1)
        P1 = np.array(r * ρ * g / x**2 * y2)

        if r[0] == 0:
            ξ[0] = 0 # if second derivative vanishes at origin
            P1[0] = float(4/3 * np.pi * c.G.cgs.value * ρ[0]**2 * R**2 * y2[0])

        ρ1 = -1/r**2 * np.gradient(r**2 * ρ * ξ, r) # = - div (ρξ)
        g1 = -c.G.cgs.value * 4 * np.pi * ρ * ξ # = G \int ρ1 dV / r^2 

        return {'ξ': ξ,
                'P1': P1,
                'ρ1': ρ1,
                'g1': g1}

    @staticmethod
    def _convert_func(res, info, data, inter_opts=None, N_atm=700):
        '''
        Returns callable eigenfunctions in cgs units

        Input
        =====

        res: solve_ivp output
        info: gyre info dict
        data: output from self._prepare
        inter_opts: dict to pass as kwargs to interp1d
        N_atm: number of outer points to treat as atmosphere
        '''
        def y1_(x):
            return res['sol'](x)[0]
        def y2_(x):
            return res['sol'](x)[1]

        R = float(info['R'])

        if N_atm:
            s_atm = slice(-N_atm, None)
        else:
            s_atm = slice(None, None)

        if inter_opts is None:
            inter_opts = {}
        inter_opts = {'kind': 'cubic', **inter_opts}
        g_ = interp1d(data['x'][s_atm], data['g'][s_atm], **inter_opts)
        ρ_ = interp1d(data['x'][s_atm], data['ρ'][s_atm], **inter_opts)

        def ξ_(r):
            x = r / R
            return np.where(r == 0, 0, R**2 / r * y1_(x))
        def P1_(r):
            x = r / R
            return np.where(r == 0,
                            4/3 * np.pi * c.G.cgs.value * data['ρ'][0]**2 * R**2 * y2_(0),
                            R**2 * ρ_(x) * g_(x) / r * y2_(x))

        return ξ_, P1_
