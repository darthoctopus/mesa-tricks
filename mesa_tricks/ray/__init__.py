#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Solve eikonal equations for stellar oscillations in the JWKB ray-theory limit.
Based on the dispersion relation
c^2 k_r^2 = c^2 kh^2 (N^2 / ω^2 - 1) + ω^2 - ω_c^2, we obtain the Eikonal quantity
W^2 = ω^2 = 1/2 (c^2 k^2 + ω_c^2 ± sqrt((c^2 k^2 + ω_c^2)^2 - 4 c^2 kh^2 N^2)).

We solve for ray trajectories by integrating from a set of initial conditions
(r, θ, k_r, k_θ). Note that k_φ = 0 because of azimuthal symmetry.
Here k_h is in physical units. However the appropriate conjugate momentum
for Hamilton's form of the eikonal equations must satisfy
S_θ = k_h r dθ === p_θ dθ, i.e. be dimensionless if S is to be a dimensionless phase.

With ω² = W as the Hamiltonian (assuming parameteric path/time-independence), we obtain the equations of motion
dr/dt = ∂W / ∂k_r,
dk_r/dt = -∂W / ∂r
dθ/dt = ∂W / ∂p_θ (note: not k_h)
dp_θ/dt = 0 (by azimuthal symmetry).

For p-modes, we specify l via the inner turning point, i.e. we choose initial conditions such that
r = r_l, θ = 0, k_r = 0, k_θ ~ sqrt(l(l+1))/r.
For mixed modes this is less straightforward. The conservation of angular momentum (i.e. p_θ)
must still be satisfied, so p_θ = sqrt(l(l+1)) is true in general
(not just for p-modes), but the inner turning point is modified (or may not be well-defined).

'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from scipy.optimize import bisect
from scipy.integrate import solve_ivp

from mesa_tricks.io import read_gyre
from astropy import units as u, constants as c

class RayProblem:
    def __init__(self, info, gyre, **kwargs):

        self._info = info
        self._gyre = gyre

        # construct physical quantities
        r = np.array(gyre['r'])
        N2 = np.array(gyre['N2'])

        # pressure scale height

        g = gyre['m'] / gyre['r']**2 * c.G.cgs.value
        Hp = gyre['P'] / gyre['ρ'] / g
        H = -gyre['ρ'] / np.gradient(gyre['ρ'], gyre['r'])

        # Acoustic cutoff frequency

        cs2 = gyre['Γ1'] * gyre['P'] / gyre['ρ']

        ωth2 = cs2 / 4 / Hp**2
        ωac2 = cs2 / 4 / Hp**2 * (1 - 2 * np.gradient(Hp, gyre['r']))

        # construct interpolants

        # N2_ = interp1d(r, N2)
        # ωc2_ = interp1d(r, ωac2)
        # cs2_ = interp1d(r, cs2)

        opts = {'s': 0, **kwargs}

        N2_ = UnivariateSpline(r, N2, **opts)
        m = ~np.isnan(ωth2)
        ωc2_ = UnivariateSpline(r[m], ωth2[m], **opts)
        cs2_ = UnivariateSpline(r, cs2, **opts)

        # we need this to solve for some turning point where k_r == 0

        # discriminant == c_s^2 k_r^2
        def X(l, ω, r):
            L2 = l * (l + 1)
            return cs2_(r) * L2 / r**2 * (N2_(r) / ω**2 - 1) + ω**2 - ωc2_(r)
        #     return ω**2 * (N2_(r) / ω**2 - 1) * (L2 * cs2_(r) / r**2 / ω**2 - 1)

        R = float(info['R'])
        def solve_r0(l, ω, x0, x1):
            f = lambda r: X(l, ω, r)
            return bisect(f, x0 * R, x1 * R)

        # save locals to instance namespace

        self.N2_ = N2_
        self.ωc2_ = ωc2_
        self.cs2_ = cs2_
        self.solve_r0 = solve_r0
        self.X = X

    # The two branches correspond to p-like and g-like behaviour!
    # We choose the convention that the positive branch yields p-like behaviour.
    # Note that the ray-optics construction does NOT admit evanescent coupling,
    # and so cannot describe mixed modes.

    def dWdk(self, l, r, k_r, pm=1): # dr/dt = dW/dk
        L2 = l * (l + 1)
        kh2 = L2 / r**2
        c2k2 = self.cs2_(r) * (k_r**2 + kh2)
        Wp = c2k2 + self.ωc2_(r) # pure-p component
        
        D =  (1 + pm * 1/np.sqrt(1 - 4 / Wp**2 * self.N2_(r) * self.cs2_(r) * kh2))
        return self.cs2_(r) * k_r * np.where(np.isnan(D), np.inf, D)

    def dWdr(self, l, r, k_r, pm=1): # dk/dt = -dW/dr
        L2 = l * (l + 1)
        kh2 = L2 / r**2
        c2k2 = self.cs2_(r) * (k_r**2 + kh2)
        Wp = c2k2 + self.ωc2_(r) # pure-p component
        dWpdr = self.cs2_(r, 1) * (k_r**2 + kh2) - 2 * self.cs2_(r) * L2 / r**3 + self.ωc2_(r, 1)
        
    #     return dWpdr

        return 1/2 * (dWpdr + pm * (Wp * dWpdr - 2 * self.N2_(r, 1) * self.cs2_(r) * kh2
                                    - 2 * self.N2_(r) * self.cs2_(r, 1)  * kh2
                                    + 4 * self.N2_(r) * self.cs2_(r) * kh2 / r
                                   ) / np.sqrt(Wp**2 - 4 * self.N2_(r) * self.cs2_(r) * kh2))

    def dWdkθ(self, l, r, k_r, pm=1): # dθ/dt = dW/dk_θ
        L2 = l * (l + 1)
        kh2 = L2 / r**2
        c2k2 = self.cs2_(r) * (k_r**2 + kh2)
        Wp = c2k2 + self.ωc2_(r) # pure-p component
        dWpdkθ = 2 * self.cs2_(r) * np.sqrt(kh2)

	#     return dWpdkθ / r

        return 1/2 *  dWpdkθ * (1 + pm * (Wp - 2 * self.N2_(r))
        			                 / np.sqrt(Wp**2 - 4 * self.N2_(r) * self.cs2_(r) * kh2)) / r
        

    def solve_ray(self, l, ω, x0, x1, branch=1, t=100, f=None, k_r=0, r0=None, **kwargs):
        R = float(self._info['R'])

        if f is None:
            f = 1 / ω**2

        def fun(t, y):
            r = y[0] * R
            k_r = y[1] / R
            θ = y[2]
            return np.array([self.dWdk(l, r, k_r, pm=branch) / R,
                             -self.dWdr(l, r, k_r, pm=branch) * R,
                             self.dWdkθ(l, r, k_r, pm=branch)]) * f

        if r0 is None:
            r0 = self.solve_r0(l, ω, x0, x1)
        opts = {'method': 'Radau', **kwargs}
        res = solve_ivp(fun, [0, t], [r0 / R, k_r * R, 0], **opts)
        return res