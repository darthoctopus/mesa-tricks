#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Surface term parameterisations and corrections
'''

import numpy as np
from scipy.optimize import least_squares
from ..utils import validate

ΔΝ_FACTOR = 1.0109

def correct_Δν(Δν_model_fit):
	'''
	Surface term correction factor for Δν, from Viani+ 2018,
	calibrated against Kepler main-sequence sample.
	'''

	return Δν_model_fit / ΔΝ_FACTOR

Ε_OFFSET = 0.263
Ε_FACTOR = 0.92

def correct_ε(ε_model):
	'''
	Surface term correction factor for ε_c, from Ong&Basu 2019,
	calibrated against mostly Kepler stars.
	'''

	return ε_model * Ε_FACTOR + Ε_OFFSET

def bg_parameterisation(am1, a3, R=1, inverse=1, cubic=1, max_ν_ac=None, **kwargs):

    def fun(ν_model, E):
        y = ν_model / (max_ν_ac if max_ν_ac is not None else 1)
        y3 = y**3
        return R * ν_model + (am1 * inverse / y + a3 * cubic * y3) / E

    return fun

def bg_matrix(am1, a3, R=1, inverse=1, cubic=1, max_ν_ac=None, **kwargs):

    def fun(ν_model, E):
        ν0 = (max_ν_ac if max_ν_ac is not None else 1)
        y = ν_model / ν0
        y2 = y**2
        return -2 * (2 * np.pi * ν0 / 1e6)**2 * (am1 * inverse + a3 * cubic * y2[:, None] * y2[None, :]) / np.sqrt(E[:, None] * E[None, :])

    return fun

def bg_fit(ν_obs, ν_model, E, direct=False, max_ν_ac=None,
    σ_ν=None, w=None, r=False, trace=False, cubic=1, inverse=1, **kwargs):
    r'''
    Ball & Gizon (2014) propose a two-term surface correction, going as

    E (ν_obs - ν_model) \sim a_{-1} (ν_model/ν_ac)^{-1} + a_{3} (ν_model/ν_ac)^{3}.

    Here ν_ac is the maximum acoustic cutoff frequency of the model. In principle,
    it is not relevant (i.e. it can be omitted, giving the fit coefficients dimensions of
    (none) and frequency^{-2}, respectively, rather than having both have units of frequency)

    ν_obs and ν_model: self-explanatory. Should be array_like with same shape.
    E: normalised inertiae. Same shape as frequencies.

    r: additional fitting parameter, introduced by scaling all model frequencies with an
    additional homology scale-factor. Since this also should scale the max acoustic cutoff
    frequency, we use

    ν_obs \sim r * ν_model + (a_{-1} (ν_model/ν_ac)^{-1} + a_{3} (ν_model/ν_ac)^{3}) / E,

    with the "standard" case reducing to when r is set to zero.

    cubic: Include cubic term (defaults to 1, set to 0 to disable)
    inverse: Include inverse term (defaults to 1, set to 0 to disable)

    '''

    _ = np.array

    assert E is not None, "Cannot do BG14 surface correction without inertiae"
    validate(ν_obs, ν_model, E, w, σ_ν)

    y = ν_model / (max_ν_ac if max_ν_ac is not None else 1)
    y3 = y**3

    if direct:
        
        # It may occasionally be useful to compare the results from
        # numerical least-squares fitting with the analytic result
        # (which can be computed directly since this is a linear
        # problem).
        
        def bracket(z):
            return np.sum(z / (_(σ_ν)**2 if σ_ν is not None else 1) * (w if w is not None else 1))

        D = ν_model - ν_obs

        A = np.array([[bracket(1 / E / y**2), bracket(y**2 / E)],
                      [bracket(y**2 / E), bracket(y**6 / E)]])
        b = -np.array([bracket(D / y), bracket(D * y**3)])

        am1, a3 = np.linalg.inv(A) @ b
        corr = ν_model + (am1/y + a3 * y**3) / E
        return (am1, a3), corr

    def cost(p):
        '''
        Cost function for least_squares
        '''
        if r:
            *q, R = p
        else:
            q = p
            R = 1
        am1, a3 = q

        z = ν_obs - (R * ν_model + (am1 * inverse / y + a3 * cubic * y3) / E)

        return z / (_(σ_ν) if σ_ν is not None else 1) * (np.sqrt(w) if w is not None else 1)

    p0 = [0, 0, 1] if r else [0, 0]
    if trace:
        print('ν_obs:', ν_obs)
        print('ν_model:', ν_model)
        print('E:', E)
        print('σ_ν:', σ_ν)
    j = least_squares(cost, p0, **kwargs)

    def residual(ν_obs, ν_model, E):
        y = ν_model / (max_ν_ac if max_ν_ac is not None else 1)
        y3 = y**3
        if r:
            *q, R = j['x']
        else:
            q = j['x']
            R = 1
        am1, a3 = q

        z = ν_obs - (R * ν_model + (am1 * inverse / y + a3 * cubic * y3) / E)

        return z

    return j, ν_obs - j['fun'] * (_(σ_ν) if σ_ν is not None else 1) * (np.sqrt(w) if w is not None else 1), residual

def ts_fit(ν_obs, ν_model, ν_max, σ_ν=None, w=None, r=False, **kwargs):
    r'''
    Sonoi+ 2015 surface correction (modified Lorentzian):

    δν/ν_max = α (1 - 1 / (1 + (ν_obs / ν_max)^β))

    '''

    _ = np.array

    validate(ν_obs, ν_model, w=w, σ_ν=σ_ν)

    def cost(p):
        '''
        Cost function for least_squares
        '''
        if r:
            *q, R = p
        else:
            q = p
            R = 1

        α, β = q
        z = ν_obs - (R * ν_model + ν_max * α * (1 - 1 / (1 + (ν_obs / ν_max)**β)))

        return z / (_(σ_ν) if σ_ν is not None else 1) * (np.sqrt(w) if w is not None else 1)

    p0 = [0, 0, 1] if r else [0, 0]
    return least_squares(cost, p0, **kwargs)
