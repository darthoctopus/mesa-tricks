import numpy as np

def get_ν(ν, l, n, ll, n_p):
    '''
    Helper function: ll and n_p are arrays of l and n
    values corresponding to values of ν (an array),
    while l and n are scalars.
    '''

    try:
        q = ν[(ll==l) & (n_p == n)].flatten()
        if len(q) == 1:
            return float(q)
        else:
            return np.nan
    except (ValueError, KeyError):
        return np.nan

def r01(ν, ll, n_p, **kwargs):
    '''
    Frequency separation ratio r_01, presumed to be insensitive
    to surface modelling error
    '''

    ν_ = lambda l, n: get_ν(ν, l, n, ll, n_p)

    def d01(n):
        return ν_(0, n) - 1/2 * (ν_(1, n-1) +  ν_(1, n))

    def δ1(n):
        return ν_(1, n) - ν_(1, n-1)

    return np.array([[ν_(0, n), d01(n) / δ1(n)] for n in np.unique(n_p)]).T

def r10(ν, ll, n_p, **kwargs):
    '''
    Frequency separation ratio r_10, also presumed to be insensitive
    to surface modelling error
    '''

    ν_ = lambda l, n: get_ν(ν, l, n, ll, n_p)

    def d10(n):
        return 1/2 * (ν_(0, n+1) + ν_(0, n)) - ν_(1, n)

    def δ1(n):
        return ν_(1, n+1) - ν_(1, n)

    return np.array([[ν_(1, n), d10(n) / δ1(n)] for n in np.unique(n_p)]).T

def r02(ν, ll, n_p, no_l1=False, **kwargs):
    '''
    Frequency separation ratio r_02, also presumed to be insensitive
    to surface modelling error
    '''

    ν_ = lambda l, n: get_ν(ν, l, n, ll, n_p)

    def d02(n):
        return ν_(0, n) - ν_(2, n-1)

    def δ1(n):
        if no_l1:
            return ν_(2, n) - ν_(2, n-1)
        return ν_(1, n) - ν_(1, n-1)

    if no_l1:
        return np.array([[ν_(0, n), d02(n) / δ1(n)] for n in np.unique(n_p)]).T
    return np.array([[(ν_(1, n) + ν_(1, n-1))/2, d02(n) / δ1(n)] for n in np.unique(n_p)]).T

def e_r01(ν, ll, n_p, e_obs, **kwargs):
    '''
    Error in r_01
    '''

    ν_ = lambda l, n: get_ν(ν, l, n, ll, n_p)
    e_ν_ = lambda l, n: get_ν(e_obs, l, n, ll, n_p)

    def d01(n):
        return ν_(0, n) - 1/2 * (ν_(1, n-1) +  ν_(1, n))

    def e_d01(n):
        return np.sqrt(e_ν_(0, n)**2 + 1/4 * (e_ν_(1, n-1)**2 + e_ν_(1, n)**2))

    def δ1(n):
        return ν_(1, n) - ν_(1, n-1)

    def e_δ1(n):
        return np.sqrt(e_ν_(1, n)**2 + e_ν_(1, n-1)**2)

    return np.array([d01(n) / δ1(n) * np.sqrt((e_d01(n)/d01(n))**2 + (e_δ1(n)/δ1(n))**2) for n in np.unique(n_p)])

def e_r10(ν, ll, n_p, e_obs, **kwargs):
    '''
    Error in r_10
    '''

    ν_ = lambda l, n: get_ν(ν, l, n, ll, n_p)
    e_ν_ = lambda l, n: get_ν(e_obs, l, n, ll, n_p)

    def d10(n):
        return 1/2 * (ν_(0, n+1) +  ν_(0, n)) - ν_(1, n)

    def δ1(n):
        return ν_(1, n+1) - ν_(1, n)

    def e_d10(n):
        return np.sqrt(1/4 * (e_ν_(0, n+1)**2 + e_ν_(0, n)**2) + e_ν_(1, n)**2)

    def e_δ1(n):
        return np.sqrt(e_ν_(1, n+1)**2 + e_ν_(1, n)**2)

    return np.array([d10(n) / δ1(n) * np.sqrt((e_d10(n)/d10(n))**2 + (e_δ1(n)/δ1(n))**2) for n in np.unique(n_p)])

def e_r02(ν, ll, n_p, e_obs, no_l1=False, **kwargs):
    '''
    Error in r_02
    '''

    ν_ = lambda l, n: get_ν(ν, l, n, ll, n_p)
    e_ν_ = lambda l, n: get_ν(e_obs, l, n, ll, n_p)

    def d02(n):
        return ν_(0, n) - ν_(2, n-1)

    def δ1(n):
        if no_l1:
            return ν_(2, n) - ν_(2, n-1)
        return ν_(1, n) - ν_(1, n-1)

    def e_d02(n):
        return np.sqrt(e_ν_(0, n)**2 + e_ν_(2, n-1)**2)

    def e_δ1(n):
        if no_l1:
            return np.sqrt(e_ν_(2, n)**2 + e_ν_(2, n-1)**2)
        return np.sqrt(e_ν_(1, n)**2 + e_ν_(1, n-1)**2)

    return np.array([d02(n) / δ1(n) * np.sqrt((e_d02(n)/d02(n))**2 + (e_δ1(n)/δ1(n))**2) for n in np.unique(n_p)])
