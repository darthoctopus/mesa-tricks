#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Asteroseismic analysis convenience functions
'''

import numpy as np
from .surface import *

T_SUN = 5777. # in K
LOG_G_SUN = 4.438067627303133 # in CGS units
ZX_SUN = 0.02293 # GS98

def ν_max(M, R, Teff):
    '''
    Simple scaling relation (Kjeldsen and Bedding 1995)
    M and R are in solar units, Teff in Kelvin

    returns ν_max in μHz
    '''
    return 3090 * M/(R**2 * np.sqrt(Teff/T_SUN))

def Δν_scale(M, R):
    '''
    Solar-calibrated scaling relation
    M, R are provided in solar units

    returns Δν in μHz
    '''
    return np.sqrt(M/R**3) * 135

def gaussianweights(ν, ν_max):
    '''
    Returns Gaussian envelope "visibility" weights
    (empirically determined, not counting mode visibility from e.g.
    multipole/inclination anisotropy)

    '''

    def envelope_FWHM(ν_max):
        '''
        FWHM of acoustic envelope (Mosser+ 2012)

        M and R in solar units
        Teff in Kelvin

        returns FWHM in μHz
        '''
        return 0.66*(ν_max)**(0.88)

    σ = envelope_FWHM(ν_max)/np.sqrt(8*np.log(2))
    return np.exp(- (ν - ν_max)**2/(2*σ**2))

def globs_from_info(M, R, L, **kwargs):
    '''
    M, R, L assumed to be provided cgs
    '''

    from astropy import constants as c, units as u

    m = (M * u.g).to(u.M_sun).value
    r = (R * u.cm).to(u.R_sun).value

    Teff = np.power(L * u.erg / u.s / (4 * np.pi * (r * u.R_sun)**2 * c.sigma_sb), 1/4).to(u.K).value

    return {'nu_max': ν_max(m, r, Teff), 'delta_nu': Δν_scale(m, r)}
