#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Manipulation of the coupling matrices computed by integrals.coupling
'''

import numpy as np
import scipy.linalg as la
from scipy.optimize import minimize
from .selection import merge_closest

ν_to_ω = 2 * np.pi / 1e6

def new_modes(A, D, N_π, *, l=1, ξr=None, ξh=None, M=None):
    r'''
    Given the matrices A and D such that we have eigenvectors

    A cᵢ = -ωᵢ² D cᵢ,

    with ω in Hz, we solve for the frequencies ν (μHz), mode mixing coefficient ζ, and
    (if the vector coefficients ξ_r, ξ_h at a specified radius R are provided) the 
    inertiae E of the modes of the coupled system. The inertiae are evaluated assuming unit
    normalisation of the eigenfunctions, so that

    Eᵢ = 4π \int dm |ξᵢ(m)|² / (M \int dΩ |ξᵢ(R)|²) === 4π / (M \int dΩ |ξᵢ(R)|²).

    Note that this differs by 4π from some other definitions; this is for consistency
    with GYRE. Overall constant factors shouldn't really matter in any case.
    '''

    Λ, U = la.eigh(A, D)
    
    Dγ = np.identity(len(D))
    Dγ[:N_π, :N_π] = 0

    new_ω2 = -Λ
    ζ = np.diag(U.T @ Dγ @ U)
    
    E = None
    if ξr is not None and ξh is not None and M is not None:
        Λ2 = l*l + l
        ηr = ξr.T @ U
        ηh = ξh.T @ U
        E = 1 / (ηr**2 + Λ2 * ηh**2) / M
    return np.sqrt(new_ω2) / ν_to_ω, ζ, E

def new_A_π(A, ν_π):
    '''
    Given a set of π-mode frequencies and the matrix A on the LHS of the
    Hermitian eigenvalue problem, modify said matrix to be consistent with
    this new set of π-mode frequencies.
    '''
    N_π = len(ν_π)
    new_ω2 = -np.diag(A)
    new_ω2[:N_π] = (ν_π * ν_to_ω)**2
    B = np.copy(A)
    B = B - np.diag(np.diag(B)) - np.diag(new_ω2)
    return B

def correct_A_π(A, old_π, corrected_π):
    '''
    Do the same, but rather than overwriting the π-mode diagonal entries,
    preserve the self-interaction term
    '''

    B = np.copy(A)
    for i, ν_o, ν_n in enumerate(zip(old_π, corrected_π)):
        B[i,i] += (ν_o * ν_to_ω)**2
        B[i,i] -= (ν_n * ν_to_ω)**2

    return B

def new_A_γ(A, ν_γ):
    '''
    Given a set of γ-mode frequencies and the matrix A on the LHS of the
    Hermitian eigenvalue problem, modify said matrix to be consistent with
    this new set of γ-mode frequencies.
    '''
    N_π = len(A) - len(ν_γ)
    new_ω2 = -np.diag(A)
    new_ω2[N_π:] = (ν_γ * ν_to_ω)**2
    B = np.copy(A)
    B = B - np.diag(np.diag(B)) - np.diag(new_ω2)
    return B

class GammaCorrProblem:
    '''
    Given a set of mixed-mode frequencies and the coupling matrices associated with
    the π-γ decomposition, find the appropriate correction to the γ-mode diagonal elements
    such that we recover the ideal set of mixed mode frequencies. This is necessary because
    our evaluation scheme for the γ-mode diagonal elements suffers heavily from integration
    error (see comments in integrals.coupling.coupling_matrices for more information).
    '''

    def __init__(self, A, D, N_π, ν_mixed, ζ_weight=1, xvar='period'):

        self.A = A
        self.D = D
        self.N_π = N_π

        self.ν = np.sort(ν_mixed)
        self.ν_γ = np.sqrt(-np.diag(A)[N_π:]) / ν_to_ω

        if xvar == 'period':
            x = 1/(self.ν_γ/1e6)
        elif xvar == 'freq':
            x = self.ν_γ
        else:
            raise NotImplementedError("xvar must be either 'period' or 'freq'")
        
        self.x = x / np.max(x)
        self.ζ_weight=ζ_weight

    def residuals(self, ν_γ):
        νp, ζ, *_ = new_modes(new_A_γ(self.A, ν_γ), self.D, self.N_π)
        a = np.argsort(νp)
        νp = νp[a]
        ζ = ζ[a]
        
        m1, m2 = merge_closest(self.ν, νp)
        return (self.ν[m1] - νp[m2]) * (np.sqrt(ζ[m2]) * self.ζ_weight + (1 if self.ζ_weight <= 0 else 0))
    
    def model(self, *p):
        # return 1/(1/(ν_γ/1e6) + np.polyval(p, x))*1e6
        return self.ν_γ + np.polyval(p, self.x)
    
    def cost(self, model, args):
        ν_trial = model(*args)
        try:
            res = self.residuals(ν_trial)
            return np.sum(res**2) / (len(res) - len(args) - 1)
        except (ValueError, np.linalg.LinAlgError):
            return np.inf
    
    def __call__(self, debug=False, model=None, i0=0):
        p = [0] * (i0 + 1)
        best = np.inf

        if model is None:
            model = self.model

        def cost(args):
            return self.cost(model, args)
        
        for i in range(i0, 100):
            j = minimize(cost, p)
            if j['success'] and j['fun'] < best:
                p = [0, *j['x']]
                best = j['fun']
            else:
                break
        
        ν_γ = self.model(*j['x'])
        B = new_A_γ(self.A, ν_γ)
        
        if debug:
            return B, ν_γ, j
        return new_modes(B, self.D, self.N_π)
