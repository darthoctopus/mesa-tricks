#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
tqdm recipe for use with Pool
'''

from pathos.multiprocessing import ProcessingPool as Pool
from tqdm.auto import tqdm

def tqdm_parallel_map(func, data, *, tqdm=tqdm, nthreads=6, **kwargs):
    with Pool(nthreads) as p:
        max_ = len(data)
        return list(tqdm(p.imap(func, data), total=max_, **kwargs))
