#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Mode selection and matching convenience functions
'''

import numpy as np
import pandas as pd
from scipy.signal import argrelextrema

from ..utils.comb_utils import comb_merge

def select_l2_modes(ν, n_p, E, weak=False):
    '''
    In evolved stars, mode mixing yields more than one
    possible l = 2 mode for each value of n_p.
    In very evolved stars, plotting inertia E against frequency yields a
    squared-Bessel-function-like pattern, with the minima correponding to the
    actually observed modes (usually the most p-dominated mode ["π-mode"] for each n_p).
    For less evolved stars, however, not every π-mode corresponds to a local
    minimum of the inertia; in those cases we force each available
    value of n_p to yield its most p-dominated mode.
    '''
    if not weak:
        m = argrelextrema(E, np.less)
        return ν[m], n_p[m], E[m]

    n_vals = np.unique(n_p)
    E_selected = np.asarray([np.min(E[n_p == ni]) for ni in n_vals])
    ν_selected = np.asarray([ν[n_p == ni][np.argmin(E[n_p == ni])] for ni in n_vals])

    return ν_selected, n_vals, E_selected

def merge_closest_pandas(df_x, df_y, a, b, same_l=False):
    '''
    Merge two data frames on the keys a and b, optionally looking for
    modes with the same n_p or l.

    e.g. df_x contains observational modes frequencies, and df_y
    contains model frequencies; for each observational frequency  I want
    the model frequency closest to it.

    This works by way of a nasty pandas hack. I originally made it in 2016,
    and it may since have been deprecated (at any rate it gives me DeprecationWarnings).
    '''
    df1 = df_x.drop_duplicates([a]).set_index(a, drop=False)
    df2 = df_y.drop_duplicates([b]).set_index(b, drop=False)
    df2 = df2.reindex(df1.index, method='nearest')
    df2[a] = df2.index
    df2.index.name = None

    # deduplication

    candidate = pd.merge(df1, df2, on=['l' if same_l else 'n_p', a], how='inner')
    candidate['merge_err'] = np.where(candidate[a]>candidate[b],
                                      (candidate[a] - candidate[b])**2,
                                      1e100 * np.ones_like(candidate[b]))
    candidate = candidate.sort_values(by='merge_err')
    candidate = candidate.drop_duplicates(b, keep='first')

    return candidate

def merge_closest_fortran(ν1, ν2):
    '''
    comb merge implemented in fortran for no good reason

    returns a pair of ordered boolean masks
    '''

    return [q.astype(bool) for q in comb_merge(ν1, ν2)]

def merge_closest(master, slave, thresh=None):
    '''
    Pairwise merge implemented with numpy broadcasting
    Slower than comb_merge for large data sets
    but otherwise adequate.

    Returns bitmasks for each of master, slave.

    thresh: maximum squared distance between modes before we start discarding things.
            If not specified, use 1/4 the squared distance of spacings in the master list
            (for p-modes, (Δν/2)^2; for g-modes, this depends on ΔΠ and νmax).
    '''
    d = (master[:, None] - slave[None, :])**2
    mmask = np.zeros_like(master, dtype=bool)
    smask = np.zeros_like(slave, dtype=bool)

    if not len(mmask) or not len(smask):
        return mmask, smask
    
    def safenorm(x):
        try:
            return np.max(np.diff(x)**2)
        except ValueError:
            return np.inf

    if thresh is not None:
        max_d = thresh
    else:
        max_d = np.minimum(safenorm(master), safenorm(slave))/4

    if max_d == np.inf:
        return np.array([True]), np.array([True])

    for i in range(len(master)):
        masked_d = d[~mmask][:,~smask]
        mi = np.arange(len(master))[~mmask]
        si = np.arange(len(slave))[~smask]
        
        if len(masked_d.flatten()) == 0:
            # ran out of things to pick
            break

        try:
            if np.min(masked_d) > max_d:
                # ran out of good things to pick
                break
        except:
            break
        
        # n-dimensional argmin
        pair = np.unravel_index(masked_d.argmin(), masked_d.shape)
        mmask[mi[pair[0]]] = True
        smask[si[pair[1]]] = True
        
    return mmask, smask

def match_modes(modes_obs, modes_model, colnames_obs=None,
    colnames_model=None, force_mod=True, maximal=False):

    '''
    Given a DataFrame of observed and model mode information,
    return a dict containing:

    ν_obs, e_obs: observed frequencies and errors
    ν_model, n_p, E, ll: Mode properties and degrees

    in such a fashion that all arrays are the same length and refer
    to corresponding modes with the same ordering. Output can be turned
    directly into a pandas dataframe.

    Column names in the input dataframes are expected to be the following:
    obs: 'ν', 'e_ν', 'l'
    model: 'ν', 'n_p', 'E', 'l'
    These can be overriden with dicts supplied as kwargs
    colnames_obs and colnames_model
    '''

    def_obs = {'ν': 'ν', 'e_ν': 'e_ν', 'l': 'l', 'n': 'n'}
    def_model = {'ν': 'ν', 'n_p': 'n_p', 'E': 'E','l': 'l'}
    
    if colnames_obs is None:
        colnames_obs = def_obs
    else:
        colnames_obs = {**def_obs, **colnames_obs}
        
    if colnames_model is None:
        colnames_model = def_model
    else:
        colnames_model = {**def_model, **colnames_model}

    ν = np.array(modes_obs[colnames_obs['ν']])
    e_ν = np.array(modes_obs[colnames_obs['e_ν']])
    l = np.array(modes_obs[colnames_obs['l']])

    l_max = max(np.max(l), np.max(modes_model[colnames_model['l']]))
    
    ν_obs = {}
    e_obs = {}
    
    for ll in range(l_max+1):
        m = (l == ll)
        ν_obs[ll] = np.array(ν[m])
        e_obs[ll] = np.array(e_ν[m])

    if colnames_obs['n'] in modes_obs:
        n_obs = {}
        for ll in range(l_max+1):
            n_obs[ll] = np.array(modes_obs[colnames_obs['n']])[l == ll]
    else:
        n_obs = None

    ν_model = {}
    n_p = {}
    E = {}

    ν = np.array(modes_model[colnames_model['ν']])
    n = np.array(modes_model[colnames_model['n_p']])
    inertia = np.array(modes_model[colnames_model['E']])
    l = np.array(modes_model[colnames_model['l']])

    for ll in range(l_max+1):
        m = (l == ll)
        ν_model[ll] = np.array(ν[m])
        n_p[ll] = np.array(n[m])
        E[ll] = np.array(inertia[m])

    if force_mod:
        # Disregard provided model n_p in favour of asymptotic relation
        # implied by ε for radial modes
        p = np.polyfit(n_p[0], ν_model[0], 1)
        ε0 = p[1] / p[0]
        for ll in range(1, l_max+1):
            try:
                p = np.polyfit(n_p[ll] + ll/2, ν_model[ll], 1)
            except (ValueError, TypeError): #TypeError thrown if any array empty
                continue
            ε_l = p[1] / p[0]
            n_p[ll] += np.round(ε_l - ε0).astype(int)
    
    mm = {}
    mo = {}

    # If n is supplied for observed modes, respect that

    if n_obs:
        q_model = n_p
        q_obs = n_obs
    else:
        q_model = ν_model
        q_obs = ν_obs

    
    if len(ν_model[0]) > 5: # magic number
        mm[0], mo[0] = merge_closest_fortran(q_model[0], q_obs[0])
    else:
        mm[0], mo[0] = merge_closest(q_model[0], q_obs[0])
    for ll in range(1, l_max+1):
        mm[ll], mo[ll] = merge_closest(q_model[ll], q_obs[ll])
        
    ll = {}
    for _ in range(l_max+1):
        ll[_] = _ * np.ones(len(ν_model[_])).astype(int)
        
    scope = locals()
        
    return {
        **{k: np.concatenate([scope[k][l][mo[l]] for l in range(l_max+1)]) for k in ['ν_obs', 'e_obs']},
        **{k: np.concatenate([scope[k][l][mm[l]] for l in range(l_max+1)]) for k in ['ll', 'ν_model', 'E', 'n_p']}}
