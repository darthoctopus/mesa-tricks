#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Some utility functions
'''
import numpy as np
import pandas as pd
from .custom_tqdm import tqdm_parallel_map
from .selection import *

def validate(ν_obs, ν_model, E=None, w=None, σ_ν=None):
    '''
    I need to do this every time anyway
    '''
    _ = np.array

    assert _(ν_obs).shape == _(ν_model).shape, "Different number of model and observed frequencies"
    
    if E is not None:
        assert _(ν_obs).shape == _(E).shape, "Different number of frequencies and inertiae"

    if σ_ν is not None:
        assert _(ν_obs).shape == _(σ_ν).shape, "Different number of frequencies from errors"

    if w is not None:
        assert _(ν_obs).shape == _(w).shape, "Different number of frequencies from weights"

def memoise(fname='memo.npy'):
    '''
    Wrap function f in memoisation stack stored on disk.
    Requires input function to accept kwargs 'ID' and 'reset'
    for handling callbacks and stack-related side effects.

    Trivial example: if the wrapped function is a MESA
    dispatcher, the ID can be used to generate the name
    of a saved logs directory, which is then returned back
    as the output of the function once the MESA run is
    complete.

    While the argument is called fname, really it can be either
    a callable (if memoise is invoked as a decorator with
    no arguments) or a string.

    Therefore, this supports at least three invocation methods:

    @memoise('fname.npy')
    def thing(...):
        # will use supplied filename

    @memoise()
    def thing(...):
        # will use default filename memo.npy

    @memoise
    def thing(...):
        # will use default filename memo.npy
    '''

    assert isinstance(fname, str) or callable(fname), "Invalid argument {fname} passed to decorator"

    fn = None
    if callable(fname):
        # default filename for DB goes here
        fn = fname
        fname = 'memo.npy'

    def decorator(fn):
        assert callable(fn), "Must provide a callable function"
        def memoised_exec(*args, **kwargs):
            try:
                store = np.load(fname, allow_pickle=True)[()]
            except FileNotFoundError:
                store = {}

            if args not in store.keys():
                ID = len(store.keys())
                store[args] = ID
                np.save(fname, store)
                q = fn(*args, **{**kwargs, 'ID': ID, 'reset': True})
            else:
                ID = store[args]
                q = fn(*args, **{**kwargs, 'ID': ID, 'reset': False})

            return q
        return memoised_exec

    if fn is None:
        return decorator
    else:
        return decorator(fn)

def GM(*args):
    '''
    Geometric mean
    '''
    return np.exp(np.mean(np.log(args)))

def discrete_tern_search(fun, vals):
    '''
    Ternary search over list of values to find the minimum
    of the provided function fun.
    '''

    N = len(vals)
    l = 0
    r = N
    m1 = None
    m2 = None

    best = -1

    for _ in range(N):
        m1 = int(np.round((2*l + r) / 3))
        m2 = int(np.round((l + 2*r) / 3))

        if l == r:
            return vals[l]
        if l == m1 or m1 == m2:
            return vals[m1]
        if r == m2:
            return vals[m2]

        if fun(vals[m1]) > fun(vals[m2]):
            l = m1
        else:
            r = m2

    # failsafe so that I can inspect the wreckage
    return l, r, m1, m2
