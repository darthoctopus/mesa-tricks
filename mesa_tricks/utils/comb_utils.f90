subroutine comb_merge(mmask, smask,  master, slave, m, n)
    implicit none
    integer :: i, j, n, m
    integer:: locs(m), from(n)
    double precision :: master(m), slave(n), threshhold, best
    logical :: bad(m)
    logical, intent(out) :: mmask(m), smask(n)

    mmask = .false.; smask = .false.; bad = .false.

    threshhold = master(2) - master(1)
    ! print *,threshhold

    ! minimum comb spacing

    do i = 3, m
        if (master(i) .eq. master(i-1)) then
            bad(i) = .true.; bad(i-1) = .true.;
            cycle
        end if
        if (master(i) - master(i-1) < threshhold) then
            threshhold = master(i) - master(i-1)
        end if
    end do

    threshhold = threshhold / 2
    j = 1
    locs = 0
    do i = 1, m ! for each master comb line
        if (bad(i)) then
            cycle
        end if
        best = 10 * threshhold
        do while (slave(j) - master(i) < threshhold .and. j <= n)
            if (master(i) .eq. slave(j)) then
                locs(i) = j
                j = j + 1
                exit
            end if
            if (master(i) - slave(j) > threshhold) then
                j = j + 1
                cycle
            end if
            if (abs(master(i) - slave(j)) < best) then
                locs(i) = j
                best = abs(master(i) - slave(j))
                j = j + 1
                cycle
            else
            	exit
            end if 
        end do
    end do

    ! eliminate duplicates

    from = 0

    do i = 1, m-1
        if (locs(i) .ne. 0) then
            if (from(locs(i)) .eq. 0) then
                from(locs(i)) = i
            else
                if(abs(master(i) - slave(locs(i))) < abs(master(from(locs(i))) - slave(locs(i)))) then
                    locs(from(locs(i))) = 0
                    from(locs(i)) = i
                else
                    locs(i) = 0
                end if
            end if
        end if
    end do

    ! construct masks

    do i = 1, m
        if (locs(i) .ne. 0) then
            mmask(i) = .true.
            smask(locs(i)) = .true.
        end if
    end do

end subroutine