#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Convenience wrapper functions for manipulating zip files
'''

from os import system

def store(zipfile, filename):
    '''
    Store a file in a zip file (including directory structure relative to CWD)
    '''
    return system(f'zip -r {zipfile} {filename}')

def extract_single(zipfile, filename):
    '''
    Extract a file in a zip file (including directory structure)
    '''
    return system(f'unzip -o {zipfile} {filename}')

def exists(zipfile, filename):
    '''
    Check if a file exists in a zip file
    '''
    return not system(f'unzip -t {zipfile} {filename} > /dev/null 2>&1')