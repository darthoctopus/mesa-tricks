from glob import glob
import warnings

import numpy as np
import pandas as pd
from scipy.integrate import trapz

from astropy import constants as c

class Eigensystem:
    def __init__(self, pattern, nad_pattern=None, scale=1, gamma=False):

        files = sorted(glob(pattern))
        modes = [pd.read_csv(f, delim_whitespace=True, skiprows=5) for f in files]
        info = [pd.read_csv(f, delim_whitespace=True, nrows=1, skiprows=2) for f in files]

        r = [np.array(m['x']) * float(i['R_star']) for i, m in zip(info, modes)]

        # in practice, all of these will share the same radial coordinate, etc.
        self.r = r[0]
        self.x = np.array(modes[0]['x'])
        self.P0 = np.array(modes[0]['P'])
        self.ρ0 = np.array(modes[0]['rho'])
        # self.T0 = np.array(modes[0]['T'])
        self.Γ1 = np.array(modes[0]['Gamma_1'])

        # frequencies
        self.ω = np.array([float(i['Re(freq)']) for i in info]).astype(np.float128) * 2 * np.pi / 1e6
        self.l = np.array([int(i['l']) for i in info])

        if len(np.array(scale).flatten()) == 1:
            scale = np.ones(len(r)) * float(scale)

        if 'Re(y_1)' not in modes[0].columns:
            s = [np.sign(m['Re(xi_r)']).values[-2] for i, m in zip(info, modes)]
            self.ξ = [np.array(m['Re(xi_r)']) * float(i['R_star']) * scl * sgn
                        for i, m, scl, sgn in zip(info, modes, scale, s)]
            self.ξh = [np.array(m['Re(xi_h)']) * float(i['R_star']) * scl * sgn
                        for i, m, scl, sgn in zip(info, modes, scale, s)]
            self.ρ = [np.array(m['Re(eul_rho)'] * m['rho']) * scl * sgn
                        for m, scl, sgn in zip(modes, scale, s)]
            self.P = [np.array(m['Re(eul_P)'] * m['P']) * scl * sgn
                        for m, scl, sgn in zip(modes, scale, s)]
            try:
                self.φ = [np.array(m['Re(eul_Phi)'] * c.G.cgs.value *
                        float(i['M_star']) / float(i['R_star'])) * scl * sgn
                        for i, m, scl, sgn in zip(info, modes, scale, s)]
            except KeyError:
                self.φ = None

            if gamma:
                self.ξh = [ξh + self.r / self.Γ1 / self.P0 / (l * (l + 1)) * P1
                    for ξh, P1, l in zip(self.ξh, self.P, self.l)]

            self.I = np.array([trapz(4 * np.pi * r0**2 * ((ξi)**2
                                    + l * (l + 1) * (ξh)**2) * self.ρ0, r0)
                    for r0, ξi, ξh, l in zip(r, self.ξ, self.ξh, self.l)])
        else:
            s = [np.sign(m['Re(y_1)']).values[-2] for i, m in zip(info, modes)]
            x = self.x
            R = float(info[0]['R_star'])
            xl1R = [R * np.power(self.x, l - 1) for l in self.l]

            self.omega = [float(i['Re(omega)']) for i in info]
            self.c1 = np.array(modes[0]['c_1'])
            self.V = np.array(modes[0]['V_2'])

            self.y1 = [np.array(mode['Re(y_1)'] * sgn * scl) for mode, sgn, scl in zip(modes, s, scale)]
            self.y2 = [np.array(mode['Re(y_2)'] * sgn * scl) for mode, sgn, scl in zip(modes, s, scale)]
            self.y3 = [np.array(mode['Re(y_3)'] * sgn * scl) for mode, sgn, scl in zip(modes, s, scale)]

            self.ξ = [y1 * X for y1, X in zip(self.y1, xl1R)]
            self.ξh = [(y2 + y3) * X / (self.c1 * ω**2) for y2, y3, X, ω in zip(self.y2, self.y3, xl1R, self.omega)]
            self.P = [y2 * X / (self.c1 * ω**2) * self.r * w**2 / self.ρ0 for y2, X, l, ω, w in zip(self.y2, xl1R, self.l, self.omega, self.ω)]
            self.φ = [y3 * X / (self.c1 * ω**2) * self.r * w**2 for y3, X, l, ω, w in zip(self.y3, xl1R, self.l, self.omega, self.ω)]

            if gamma:
                self.ξh = [(y2 * (1 + (self.c1 * ω**2) / (l * (l + 1)) * self.V/self.Γ1) + y3) * X / (self.c1 * ω**2)
                            for y2, y3, X, ω, l in zip(self.y2, self.y3, xl1R, self.omega, self.l)]

            self.I = np.array([trapz(4 * np.pi * r0**2 * ((ξi)**2
                                    + l * (l + 1) * (ξh)**2) * self.ρ0, r0)
                    for r0, ξi, ξh, l in zip(r, self.ξ, self.ξh, self.l)])

        # divergence — we do not neglect the sphericity term
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.div_ξ = [np.nan_to_num(np.gradient(ξi, r0) + 2/r0 * ξi)
                    for ξi, r0 in zip(self.ξ, r)]
        self.scale = scale
        
        if 'Re(lag_rho)' in modes[0].columns:
            self.δρ = [np.array((m['Re(lag_rho)'] + 1j * m['Im(lag_rho)']) * m['rho']) * scl * sgn
                        for m, scl, sgn in zip(modes, scale, s)]
        if 'Re(lag_P)' in modes[0].columns:
            self.δP = [np.array((m['Re(lag_P)'] + 1j * m['Im(lag_P)']) * m['P']) * scl * sgn
                        for m, scl, sgn in zip(modes, scale, s)]

        # expose the raw pandas dataframes
        self._modes = modes
        self._info = info

        if nad_pattern is not None:
            nad_files = sorted(glob(nad_pattern))
            assert len(nad_files) == len(files), "no one-to-one map between adiabatic and nonadiabatic eigensystem"
            nad_info = [pd.read_csv(f, delim_whitespace=True, nrows=1, skiprows=2) for f in nad_files]
            self.ω_nad = np.array([float(i['Re(freq)']) for i in nad_info]).astype(np.float128) * 2 * np.pi / 1e6
            self.η = np.array([float(i['Im(freq)']) for i in nad_info]).astype(np.float128) * 2 * np.pi / 1e6

            nad_modes = [pd.read_csv(f, delim_whitespace=True, skiprows=5) for f in nad_files]

            self.ξ_nad = [np.array(m['Re(xi_r)'] + 1j * m['Im(xi_r)']) * float(i['R_star']) * scale
                    for i, m in zip(info, nad_modes)]
            self.I_nad = np.array([trapz(4 * np.pi * r0**2 * np.abs(ξi)**2 * m['rho'], r0)
                for r0, ξi, m in zip(r, self.ξ_nad, nad_modes)])
            self.δρ_nad = [np.array((m['Re(lag_rho)'] + 1j * m['Im(lag_rho)']) * m['rho']) * scale
                    for m in nad_modes]
            self.δP_nad = [np.array((m['Re(lag_P)'] + 1j * m['Im(lag_P)']) * m['P']) * scale
                    for m in nad_modes]
            self.δT_nad = [np.array((m['Re(lag_T)'] + 1j * m['Im(lag_T)']) * m['T']) * scale
                    for m in nad_modes]
            cp = (lambda m: m['P'] * m['delta'] / m['T'] / m['nabla_ad'])(nad_modes[0])
            self.δS = [np.array((m['Re(lag_S)'] + 1j * m['Im(lag_S)']) * cp) * scale
                    for m in nad_modes]

            self._nad_modes = nad_modes
            self._nad_info = nad_info
        else:
            self.η = None
