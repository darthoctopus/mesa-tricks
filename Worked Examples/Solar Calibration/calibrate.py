#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy
## Simple solar calibration
## (can be made more complicated with e.g. better optimisation scheme but w/e)

import os
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit, bisect

# solar parameters

NUMAX = 3090
DNU = 135 #interestingly I don't actually have a reference for this

# Gaussian Weights: see Mosser+ 2012 (reference in Viani+ 2018)

def nu_FWHM(numax): #in uHz
    return 0.66*(numax)**(0.88)

def gaussianweights(numax, nu): # NB T must be provided in Kelvin
    sigma = nu_FWHM(numax)/np.sqrt(8*np.log(2))
    return np.exp(- (nu - numax)**2/(2*sigma**2))

def cost(r, Y, alpha):
    Z = r * (1-Y) / (1+r)
    os.system("./create_profiles.sh {} {} {} > /dev/null".format(Z, Y, alpha))
    history = pd.read_table("tracks/LOGS_1/history.data", delim_whitespace=True, skiprows=5)

    l = history.iloc[-1]

    c = np.array([l.log_L, l.log_R])
    print([r, Y, alpha, *c])
    return c

def astero_cost(r):
    """Performs a solar calibration up to a given initial value of Z/X,
    then checks the value of Δν that results for the solar-age model (modulo
    surface correction). Use this in conjunction with a secondary
    optimisation scheme (e.g. bisect)

    Yes we are doing hierarchical optimisation

    Shhh it works
    """

    Gauss_Newton(lambda x: cost(r, *x), [0.27, 1.8],
                       maxiter=10)

    l = pd.read_table("tracks/LOGS_1/history.data", delim_whitespace=True, skiprows=5).iloc[-1]

    #astero

    os.system("./gyre-l02.sh tracks/1.profile.data.GYRE 1500 4500")
    modes = pd.read_table("tracks/1-freqs.dat",
                          delim_whitespace=True, skiprows=5)

    weights = gaussianweights(l.nu_max, modes['Re(freq)'])
    Δν, _ = curve_fit((lambda n, Δ, ε: Δ * (n + ε)), modes['n_p'], modes['Re(freq)'],
                      p0=(100, 0), sigma=1/np.sqrt(weights))[0]

    Δν = Δν / 1.0109 # surface correction: see Viani+ 2018

    print("Tested r = %f; value of %f" % (r, Δν - DNU))
    return Δν - DNU


def met_cost(r, R):
    Gauss_Newton(lambda x: cost(r, *x), [0.27, 1.8],
                       maxiter=10)

    l = pd.read_table("tracks/LOGS_1/history.data", delim_whitespace=True, skiprows=5).iloc[-1]
    d = np.power(10, l.log_surf_cell_z - l.log_surface_h1) - R
    print("Tested r = %f; value of %f" % (r, d))
    return d

def Gauss_Newton(fun, x0, **kwargs):
    """Stateful Gauss-Newton Solver: order of functional
    evaluation is arranged so that last function call leaves
    the `tracks` directory populated with the MESA configuration
    and simulation history/profile files associated with
    formal minimal costs.

    As far as possible I've tried to replicate the output interfaces
    of e.g. scipy.optimize.least_squares.

    One could of course use least_squares without modification
    but empirically it seems setting an absolute rather than relative
    diff_step works better for this problem.
    """
    x = np.array(x0)
    maxiter = kwargs.pop('maxiter', 100)
    D = np.array([kwargs.pop('diff_step', 1e-3)] * len(x0))
    alpha = kwargs.pop('alpha', 1)
    atol = kwargs.pop('atol', 1e-10)

    c = fun(x)
    best = 1/2 * c @ c
    bestres = c
    bestx = x

    for i in range(maxiter):

        # displacement vectors
        V = np.diag(D)

        #Jacobian
        DC = np.array([fun(x+v) - c for v in V])
        J = (np.linalg.pinv(V) @ DC).T
        print(J)
        x = x - alpha * np.linalg.pinv(J) @ c

        c = fun(x)
        if 1/2 * c @ c < best:
            best = 1/2 * c @ c
            bestx = x
            bestres = c

        if best < atol:
            break

    if 1/2 * c @ c > best:
        fun(bestx)

    return {'x': bestx, 'fun': bestres, 'jac': J, 'nfev': i+1}

# Z between 0.017 and 0.023? I guess? idk?

if __name__ == "__main__":
    f = lambda x: met_cost(x, 0.023)
    res = bisect(f, 0.026043, 0.026047, maxiter=6)
    print(res)
    # f = lambda x: cost(0.0265, *x)
    # Gauss_Newton(f, [.24, 2], maxiter=10)
