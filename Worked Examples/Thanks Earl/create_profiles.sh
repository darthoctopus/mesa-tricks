#!/bin/bash 
#### Create a grid of stellar evolutionary tracks varied in an initial parameter

change_param() { 
	# Modifies a parameter in the current inlist. 
	# args: ($1) name of parameter 
	#       ($2) new value 
	#       ($3) filename of inlist where change should occur 
	# Additionally changes the 'inlist_0all' inlist. 
	# example command: change_param initial_mass 1.3 
	# example command: change_param log_directory 'LOGS_MS' 
	# example command: change_param do_element_diffusion .true. 
	param=$1 
	newval=$2 
	filename=$3 
	search="^\s*\!*\s*$param\s*=.+$" 
	replace="      $param = $newval" 
	sed -r -i.bak -e "s/$search/$replace/g" $filename 
	
	if [ ! "$filename" == 'inlist_0all' ]; then 
		change_param $1 $2 "inlist_0all" 
	fi 
} 

set_inlist() { 
	# Changes to a different inlist by modifying where "inlist" file points 
	# args: ($1) filename of new inlist  
	# example command: change_inlists inlist_2ms 
	newinlist=$1 
	echo "Changing to $newinlist" 
	change_param "extra_star_job_inlist2_name" "'$newinlist'" "inlist" 
	change_param "extra_controls_inlist2_name" "'$newinlist'" "inlist" 
}

make_grid() {
	cp inlist tracks
	cp inlist* tracks
	cd tracks
	
	Z=$1
	Y=$2
	ALPHA=$3
	
	for M in 1; do #edit me
	
		LABEL=$M
		
		change_param mixing_length_alpha $ALPHA "inlist_0all"
		change_param write_profile_when_terminate ".false." "inlist_0all"
		
		# create pre-main sequence model 
		inlist="inlist_1pms" 
		set_inlist $inlist 
		change_param initial_y $Y $inlist # fix me!
		change_param new_Y $Y $inlist
		change_param initial_z $Z $inlist
		change_param new_Z $Z $inlist
		change_param Zbase $Z $inlist
		change_param initial_mass $M $inlist
		./rn 
		
		# run until solar age
		inlist="inlist_2ms" 
		set_inlist $inlist 
		# change_param "write_profiles_flag" ".false." $inlist
		# change_param "save_model_filename" "$LABEL.mod" $inlist
		change_param "filename_for_profile_when_terminate" "$LABEL.profile.data" $inlist
		change_param "log_directory" "'LOGS_$LABEL'" $inlist
		./rn
	done

	# done
	cd -
}
make_grid $1 $2 $3

