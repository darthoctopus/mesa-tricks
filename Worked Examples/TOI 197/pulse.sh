#!/bin/bash

#### Converter for .GYRE file to oscillation mode frequencies with GYRE
#### Author: Earl Bellinger ( bellinger@mps.mpg.de ) 
#### Stellar Ages & Galactic Evolution Group 
#### Max-Planck-Institut fur Sonnensystemforschung 

### Parse command line tokens 
## -h and --help will display help instructions 
if [ -z ${1+x} ] || [ $1 == '-h' ] || [ $1 == '--help' ]; then
    echo "Converter for .GYRE to oscillation mode frequencies."
    echo "Usage: ./gyre.sh .GYRE LOWER UPPER"
    echo
    echo "Will compute frequences between LOWER and UPPER in μHz"
    exit
fi

## Check that the first input (GYRE file) exists
if [ ! -e "$1" ]; then
    echo "Error: Cannot locate GYRE file $1"
    exit 1
fi

## Pull out the name of the GYRE file
bname="$(basename $1)"
fname="${bname%%.*}-$4-freqs"
pname="${bname%.*}"

# We now want to use argv to specify lower and upper limits
# ## If the second (OUTPUT) argument doesn't exist, create one from the first
# if [ -z ${2+x} ]; then
path=$(dirname "$1")/"$fname"
#  else
#    path="$2"
# fi

## Create a directory for the results and go there
mkdir -p "$path" 
cp "$1" "$path" 
cd "$path" 

logfile="gyre.log"
#exec > $logfile 2>&1

lower=$2
upper=$3

echo "Searching between $lower and $upper"

echo "
&model
    model_type = 'EVOL'
    file = '../$bname'
    file_format = 'MESA'
    ! file_format = 'FGONG'
    ! data_format = '(1P5E16.9,x)'
/

&constants
    G_GRAVITY = 6.67408d-8
/
" >| "l02.in"

for iter in $4; do
echo "
&mode
    l = $iter          ! Harmonic degree
/
" >> "l02.in";
done

echo "
&osc
    outer_bound = 'JCD'
    variables_set = 'JCD'
    inertia_norm = 'BOTH'
    !reduce_order = .FALSE.
/

&num
    diff_scheme = 'MAGNUS_GL2'
/

&scan
    grid_type = 'LINEAR'
    freq_min_units = 'UHZ'
    freq_max_units = 'UHZ'
    freq_min = $lower
    freq_max = $upper
    n_freq = 1000
/

&grid
    alpha_osc = 10            ! At least alpha points per oscillatory wavelength
    alpha_exp = 10            ! At least alpha points per exponential 'wavelength'
    n_inner = 20                ! At least n points in the evanescent region
/

&ad_output
    summary_file = '$fname.dat'
    summary_file_format = 'TXT'
    summary_item_list = 'l,n_pg,n_p,n_g,freq,E_norm'
    freq_units = 'UHZ'
/

&nad_output
/

" >> "l02.in"

## Run GYRE
# Earl put in a timeout of 1hr for some reason
# sometimes this is bad
timeout 3600 $GYRE_DIR/bin/gyre l02.in &>l02.out

RETVAL=$?
if [ $RETVAL -eq 124 ]; then
    echo "l=0,2 search failed"
    exit 1
fi


### Hooray!
cp "$fname.dat" ..
rm -rf *
currdir=$(pwd)
cd ..
rm -rf "$currdir"
echo "Conversion complete. Results can be found in $fname.dat"
exit 0

