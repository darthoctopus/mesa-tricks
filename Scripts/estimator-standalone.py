#! env python3
# Estimator for large frequency separation Δν given MESA models
# Reference: Ong & Basu (2019), ApJ, 870, 41, doi: 10.3847/1538-4357/aaf1b5
# Author: Joel Ong, Yale University <joel.ong@yale.edu>

# Much of this code is duplicated from Joel's MESA-tricks repository
# at http://gitlab.com/darthoctopus/mesa-tricks

import argparse
from os.path import basename, isfile

import numpy as np
import pandas as pd
from scipy.interpolate import interp1d, UnivariateSpline
from scipy.integrate import quad, trapz, cumtrapz
from scipy.optimize import bisect
from astropy import units as u, constants as c

# i/o: read GYRE file

def read_gyre(f, **kwargs):
    '''
    Read GYRE model file produced by MESA

    Input: filename (e.g. "profile\\d+.data.GYRE")
    '''
    # GYRE accepts CGS units for its MESA input format; see documentation
    # Mesh points go from centre outwards (opposite from usual MESA profile files)
    info = pd.read_csv(f, delim_whitespace=True, nrows=1, names=[
        "nrows", "M", "R", "L", "version"
    ], **kwargs)
    profiles = pd.read_csv(f, delim_whitespace=True, skiprows=1, names=[
        "k", "r", "m", "L", "P", "T", "ρ", "∇", "N2", "Γ1", "∇ad", "δ",
        "κ", "κκ_T", "κκ_ρ", "εnuc", "εε_T", "εε_ρ", "Ω"
    ], **kwargs)
    return info, profiles

# Preliminaries: define a bunch of physical quantities
# in terms of MESA output columns

def cs(p):
    """Adiabatic Sound Speed"""
    return np.sqrt(p['Γ1'] * p['P'] / p['ρ'])

def H(p, obs):
    """
    Scale height of an observable obs from numerical differentiation
    """
    return -1/np.gradient(np.log(p[obs]), np.array(p['r']))

def νac_isothermal(p):
    """
    Acoustic cutoff frequency: I use the simplified expression
    (which assumes an isothermal atmosphere)
    rather than the full one.

    Moreover, we can also use the analytic expression for H if we're
    assuming an isothermal atmosphere, as assert that H = H_p = P / (ρ g)

    Units of microhertz; We assume inputs are in cgs
    """
    g = np.nan_to_num(c.G.cgs *  p['m']/p['r']**2)
    Hρ = np.array(p['P'] / (g * p['ρ']))
    return 1e6 * np.array(cs(p) / (4 * np.pi * Hρ)) + 0j

def simple_Δν_quad(ν, profile, info, **kwargs):
    '''
    Compute the l=0 asymptotic estimator in Ong & Basu 2019
    using an adaptive Gauss-Kronrod scheme (to handle integrable
    singularities properly at the endpoints)
    '''
    R = np.float(info['R'])
    c = np.array(cs(profile), dtype=np.float128)
    νaclist = np.nan_to_num(np.array(νac_isothermal(profile), dtype=np.float128))
    r = np.array(profile['r'], dtype=np.float128)

    ω = 2 * np.pi * ν / 1e6 #in cgs units

    c_ = interp1d(r, c, **kwargs)
    νac_ = interp1d(r, νaclist, **kwargs)

    def Radicand1(r):
        return np.nan_to_num(1 - νac_(r)**2/ν**2) + 0j

    def Integrand1(r):
        return np.nan_to_num(1 / np.sqrt(Radicand1(r)) / c_(r))

    rmin = np.min(r)

    MID = np.nanargmax(Radicand1(r[(r/R) < 1])[1:])+1

    try:
        outerlim = bisect(Radicand1, r[MID], R)
    except:
        raise

    innerlim = rmin

    mm = np.nanargmin(Radicand1(r[:MID]))
    if np.sign(Radicand1(r[mm])) != np.sign(Radicand1(r[MID])):
        innerlim = bisect(Radicand1, r[mm], r[MID])

    T = quad(Integrand1, innerlim, outerlim, limit=1000)
    Δν1 = (1 / 2 / T[0]) * 1e6

    return Δν1

def Δν(file, ν_max=None, **kwargs):
    """
    I wrap most of the I/O stuff into this function.
    Inputs:
        file (string): filename of GYRE file from MESA
        ν_max (float): Frequency of maximum acoustic power, in units of μHz.
                        Will be estimated from scaling relation if not provided.
    Returns:
        Δν (float): Integral estimator in units of μHz
    """

    assert isfile(file), "Target file does not exist"
    info, profiles = read_gyre(file)

    if ν_max is None:
        M = float(info['M']) * u.g
        R = float(info['R']) * u.cm
        L = float(info['L']) * u.erg / u.s

        # Compute Teff from L
        Teff = np.power(L / (4 * np.pi * R**2 * c.sigma_sb), 1/4).to(u.K)

        # Scaling relation value in μHz
        ν_max = 3090 * ((M / c.M_sun) / (R / c.R_sun)**2 / np.sqrt(Teff / (5777 * u.K))).to(1).value

    Δν_est = simple_Δν_quad(ν_max, profiles, info, **kwargs)
    return Δν_est


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Compute the integral estimator from Ong & Basu (2019) in units of μHz, given a MESA model profile (in GYRE format)")
    parser.add_argument("file", help="Filename (of the form <x>.data.GYRE")
    parser.add_argument("--numax", dest='ν_max', default=None, help="Asteroseismic ν_max in μHz (default: infer from GYRE file using scaling relation)")

    args = parser.parse_args()
    print(Δν(args.file, args.ν_max))
