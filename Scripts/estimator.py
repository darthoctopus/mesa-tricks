#! env python3

import pandas as pd
from os.path import isdir, isfile
from mesa_tricks.io import read_track, read_gyre
from mesa_tricks.integrals.Δν import simple_Δν_quad
from expres.utils import tqdm_parallel_map

import argparse
parser = argparse.ArgumentParser(description='Compute integral estimator for Δν')
parser.add_argument('directory', help='MESA log directory')
args = vars(parser.parse_args())

TRACK = args['directory']

assert isdir(TRACK), "Must supply a MESA log directory"
assert isfile(f"{TRACK}/profiles.index"), "Logs directory must contain profile index"
assert isfile(f"{TRACK}/history.data"), "Logs directory must contain history file"

df = read_track(TRACK)
OUT = f"{TRACK}/integrals.csv"

def job(q):
	_, r = q
	info, p = read_gyre(f"{TRACK}/profile{int(r.profile)}.data.GYRE")
	return simple_Δν_quad(r.nu_max, p, info)

if not isfile(OUT):
	out = pd.DataFrame()
	out['model_number'] = df['model_number']
	out['Δν_est'] = tqdm_parallel_map(job, list(df.iterrows()))

	out.to_csv(OUT)