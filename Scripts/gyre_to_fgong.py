#! env python3
# Converts GYRE to FGONG files

import argparse
from os.path import isfile
from mesa_tricks.io import read_gyre, gyre_to_fgong, write_fgong
import pandas as pd

def convert(filename):
    i, p = read_gyre(filename)
    kwargs = {}
    if isfile(filename.replace('.data.GYRE', '.data')):
    	kwargs['mesa_profiles'] = pd.read_csv(filename.replace('.data.GYRE', '.data'),
    		skiprows=5, delim_whitespace=True)
    g, c = gyre_to_fgong(i, p, **kwargs)
    with open(filename.replace('.GYRE', '.FGONG'), 'w') as f:
        f.write(write_fgong(g, c))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert MESA-format GYRE input files into FGONG files (but with lots of missing data)")
    parser.add_argument("file", help="Filename (of the form <x>.data.GYRE")

    args = parser.parse_args()
    assert isfile(args.file), f"File {args.file} does not exist!"
    convert(args.file)
