from mesa_tricks.io import read_gyre, write_gyre
from os.path import isfile, basename
import numpy as np

import argparse
N_PAD = 1

def truncate(file, retain_center=False, trim_outer=True):
	info, gyre = read_gyre(file)

	if not retain_center:
		info['nrows'] -= 1
		gyre = gyre.iloc[1:].reset_index(drop=True)
		gyre['k'] -= 1

	if trim_outer:
		N2 = gyre['N2'].values
		for i in range(10, int(info['nrows'])):
			if N2[i] < 0:
				break
		info['nrows'] = i + N_PAD
		gyre = gyre.iloc[:i + N_PAD]

	write_gyre(file, info, gyre)

if __name__ == '__main__':

	a =argparse.ArgumentParser(description='Remove the central point of a MESA/GYRE model')
	a.add_argument('file')
	a.add_argument('--trim_outer', action="store_false")
	a.add_argument('--retain_center', action="store_true")
	p = a.parse_args()

	assert isfile(p.file), "Supply a GYRE output file from MESA"

	truncate(p.file, p.retain_center, p.trim_outer)