from os import system, chdir, getcwd
from os.path import basename, abspath, dirname, isfile, isdir
from jinja2 import Environment, Template, FileSystemLoader
from os.path import isfile, isdir, basename

from scipy.integrate import cumtrapz
import matplotlib.pyplot as plt

import numpy as np
from astropy import units as u, constants as c
from mesa_tricks.io import read_gyre, read_freqs
from mesa_tricks.io.eigensystem import Eigensystem
from mesa_tricks.integrals.coupling import *
from mesa_tricks.utils import discrete_tern_search
from mesa_tricks.utils.coupling import GammaCorrProblem
from mesa_tricks.integrals.ΔΠ import ΔΠ0_as

from truncate import truncate

WHERE_AM_I = abspath(dirname(__file__))
env = Environment(loader=FileSystemLoader(f"{WHERE_AM_I}/templates"))

NUMAX_SUN = 3090
DELTA_NU_SUN = 135
GAMMA_H = False

def rot_kernel(ξr1, ξh1, ξr2, ξh2, l, m):
    Λ2 = l * (l + 1)
    return 2*m * (ξr1 * ξr2 + (Λ2 - 1) * (ξh1 * ξh2) - ξr1 * ξh2 - ξh1 * ξr2)

def cent_kernel(ξr1, ξh1, ξr2, ξh2, l, m):
    Λ2 = l * (l + 1)
    Q = (Λ2 - 3*m**2)/(2*l-1)/(2*l+3)
    return (ξr1*ξr2*(2/3)*(1-Q) + ξh1*ξh2*(Λ2 - 2/3*(Λ2-3)*(1-Q) + m**2) - Q*(ξh1*ξr2 + ξr1*ξh2))

def rotation_matrices(info, gyre, l, π, γ, contrast, x_atm=0.9, simple_γ=10, inter_opts=None):

    if inter_opts is None:
        inter_opts = {}

    inter_opts = {**dict(k=3, s=0, ext='zeros'), **inter_opts}

    # set up required interpolants

    Λ2 = l * (l + 1)
    G = c.G.cgs.value

    r0 = prune_double_points(np.array(gyre.r))
    R = float(info['R'])
    N2 = np.array(gyre.N2)
    N2_ = UnivariateSpline(r0, N2, **inter_opts)
    g_ = UnivariateSpline(r0, np.where(r0 > 0, G * gyre['m']/gyre['r']**2, 0), **inter_opts)

    # set up infrastructure for matrices
    N_π = len(π.ξ)
    Fππ = np.mean(π.I)

    if γ is not None:
        N_γ = len(γ.ξ)
        Fγγ = γ.I
        Fπγ = np.sqrt(Fππ * Fγγ)
    else:
        N_γ = 0

    Ntot = N_π + N_γ

    K = np.zeros((Ntot, Ntot))
    V = {}
    for m in range(l+1):
        V[m] = np.zeros((Ntot, Ntot))
    β = np.identity(Ntot)
    r = π.r
    ρ = π.ρ0

    # π self-interaction

    N2 = N2_(r)
    s = ((r / R) < x_atm) & (N2 > 0)

    Ω = r * 0 + 1
    Ω[s] *= contrast

    for i in range(N_π):
        for j in range(N_π):
            K[i,j] = trapz((4 * np.pi * r**2 * ρ * Ω * rot_kernel(π.ξ[i], π.ξh[i], π.ξ[j], π.ξh[j], l, 1)), r) / Fππ * (-1)**(i+j)
            β[i,j] = trapz((4 * np.pi * r**2 * ρ * rot_kernel(π.ξ[i], π.ξh[i], π.ξ[j], π.ξh[j], l, 1)), r) / Fππ * (-1)**(i+j)
            for m in range(l+1):
                V[m][i,j] = trapz((4 * np.pi * r**2 * ρ * Ω**2 * cent_kernel(π.ξ[i], π.ξh[i], π.ξ[j], π.ξh[j], l, m)), r) / Fππ * (-1)**(i+j)


    # γ self-interaction
    if γ is not None:

        r = γ.r
        ρ = γ.ρ0

        f0 = γ.ρ0 / γ.Γ1 / γ.P0
        g = g_(r)
        N2 = N2_(r)
        N2g = N2/g
        N2g[0] = 0

        s = (r > 0) & (N2 > 0)
        s_atm = ((r/R) < x_atm) & (N2 > 0)

        Ω = r * 0 + 1
        Ω[s_atm] *= contrast

        for i in range(N_γ):
            for j in range(N_γ):
                K[N_π+i,N_π+j] = trapz((4 * np.pi * r**2 * ρ * Ω * rot_kernel(γ.ξ[i], γ.ξh[i], γ.ξ[j], γ.ξh[j], l, 1)), r) / np.sqrt(Fγγ[i] * Fγγ[j])
                β[N_π+i,N_π+j] = trapz((4 * np.pi * r**2 * ρ * rot_kernel(γ.ξ[i], γ.ξh[i], γ.ξ[j], γ.ξh[j], l, 1)), r) / np.sqrt(Fγγ[i] * Fγγ[j])
                for m in range(l+1):
                    V[m][N_π+i,N_π+j] = trapz((4 * np.pi * r**2 * ρ * Ω**2 * cent_kernel(γ.ξ[i], γ.ξh[i], γ.ξ[j], γ.ξh[j], l, m)), r) / np.sqrt(Fγγ[i] * Fγγ[j])

    # cross terms

    k = np.zeros((N_π, N_γ))
    b = np.zeros((N_π, N_γ))
    v = {}
    for m in range(l+1):
        v[m] = np.zeros((N_π, N_γ))

    # s = ((r / R) < x_atm) & (N2g > 0) & (r > 0)
    s = (r > 0)
    # s = slice(None, None)

    for i in range(N_π):
        ξπr = UnivariateSpline(prune_double_points(π.r), π.ξ[i], **inter_opts)
        ξπh = UnivariateSpline(prune_double_points(π.r), π.ξh[i], **inter_opts)

        ξπr = ξπr(r)
        ξπh = ξπh(r)

        for j in range(N_γ):
            k[i,j] = trapz((4 * np.pi * r**2 * ρ * Ω * rot_kernel(ξπr, ξπh, γ.ξ[j], γ.ξh[j], l, 1)), r) / Fπγ[j] * (-1)**(i)
            b[i,j] = trapz((4 * np.pi * r**2 * ρ * rot_kernel(ξπr, ξπh, γ.ξ[j], γ.ξh[j], l, 1)), r) / Fπγ[j] * (-1)**(i)
            for m in range(l+1):
                v[m][i,j] = trapz((4 * np.pi * r**2 * ρ * Ω**2 * cent_kernel(ξπr, ξπh, γ.ξ[j], γ.ξh[j], l, m)), r) / Fπγ[j] * (-1)**(i)

    K[:N_π, N_π:] = k
    K[N_π:, :N_π] = k.T

    β[:N_π, N_π:] = b
    β[N_π:, :N_π] = b.T
    β /= 2

    for m in range(l+1):
        V[m][:N_π, N_π:] = v[m]
        V[m][N_π:, :N_π] = v[m].T

    # explicit symmetrisation

    # K = 1/2*(K + K.T)
    β = 1/2*(β + β.T)

    return {'R': K, 'β': β, 'V': V}

def pulse(input, output, degree, lower, upper, pfx='work', π_only=False,
            max_ν_γ=None, ΔΠ0=None, Δν=None):
    if not isdir(pfx):
        system(f'mkdir "{pfx}"')
    system(f"cp '{input}' '{pfx}'")
    for job in ['π', 'mixed']:
        template = env.get_template(f'{job}.template')
        with open(f'{pfx}/{job}.in', 'w') as f:
            f.write(template.render(input=basename(input),
                degree=degree, output=output, lower=lower, upper=upper))

    template = env.get_template(f'γ.template')
    n_freq = None
    if max_ν_γ is not None:
        upper = np.maximum(upper, max_ν_γ)
    else:
        upper = 2 * upper
    if Δν is not None:
        lower -= Δν
    if ΔΠ0 is not None:
        ΔΠ = ΔΠ0 / np.sqrt(degree * (degree + 1))
        Π_max = 1/(lower / 1e6)
        Π_min = 1/(upper / 1e6)

        print(Π_max, Π_min, ΔΠ)
        n_freq = 2 * int(np.round((Π_max - Π_min) / ΔΠ)) + 5

    with open(f'{pfx}/γ.in', 'w') as f:
            f.write(template.render(input=basename(input),
                degree=degree, output=output, lower=lower, upper=upper, n_freq=n_freq))
    cwd = getcwd()
    chdir(pfx)
    system('$GYRE_DIR/bin/gyre π.in')
    if not π_only:
        system('$GYRE_DIR/bin/gyre mixed.in')
        # truncate(basename(input), trim_outer=True, retain_center=True)
        system('$GYRE_DIR/bin/gyre γ.in')
    chdir(cwd)

def compute_eigensystem(fname, l, N_MODES=10, max_γ=False, **kwargs):
    outdir = fname.split('.data.')[0] + f"-{l:d}"

    info, gyre = read_gyre(fname)
    Teff = np.power(info['L'] / (4 * np.pi * info['R']**2 * c.sigma_sb).cgs.value, 1/4)
    numax = NUMAX_SUN * info['M'] / c.M_sun.cgs.value / (info['R'] / c.R_sun.cgs.value)**2 / np.sqrt(Teff/5777)
    delta_nu = DELTA_NU_SUN * np.sqrt(info['M'] / c.M_sun.cgs.value / (info['R'] / c.R_sun.cgs.value)**3)

    lower = max(numax - N_MODES * delta_nu, delta_nu)
    upper = numax + N_MODES * delta_nu

    if max_γ:
        max_ν_γ = np.sqrt(np.max(gyre['N2'][gyre['r']/info['R'] < 0.9])) / (2 * np.pi) * 1e6
    else:
        max_ν_γ = None

    ΔΠ0 = ΔΠ0_as(gyre, (gyre['N2'] > 0) & (gyre['r'] < 0.9 * info['R']))
    pulse(fname, "freqs.dat", l, lower, upper, pfx=outdir,
          max_ν_γ=max_ν_γ, ΔΠ0=ΔΠ0, Δν=delta_nu, **kwargs)

def compute_matrices(file, l, contrast=5, x_atm=0.9, clear=True, corr=True,
                     recompute_eigensystem=False, **kwargs):

    # 0. read GYRE file

    info, gyre = read_gyre(file)

    outname = file.split('.data.')[0] + f"-{l:d}"

    # 1. compute π and γ eigensystems
    if recompute_eigensystem or not isdir(outname):
        compute_eigensystem(file, l, **kwargs) 

    # 2. set up eigensystem objects
    try:
        π = Eigensystem(f"{outname}/π*.txt")
        try:
            γ = Eigensystem(f"{outname}/γ*.txt", gamma=GAMMA_H)
        except IndexError:
            γ = None
    except IndexError:
        print("Cannot find eigensystem")
        pass
    else:
        res = rotation_matrices(info, gyre, l, π, γ, contrast=contrast, x_atm=x_atm)

        # save these matrices
        np.save(f"{outname}-rotation.npy", res)

    # delete eigensystem
    if clear:
        system(f"rm -rf {outname}")

from glob import glob
from tqdm.auto import tqdm
from mesa_tricks.utils import tqdm_parallel_map

if __name__ == '__main__':
    import argparse

    parse = argparse.ArgumentParser(description="Compute π/γ mode coupling matrices given a GYRE input file")

    parse.add_argument("filename")
    parse.add_argument("contrast", type=float)
    parse.add_argument("--degree", type=int, default=1, help="Mode degree $l$ (default: 1)")
    parse.add_argument("--modes", type=int, default=5, help="$n$, where we evaluate the nearest $2n+1$ p-modes to nu_max (default: $n=5$)")
    parse.add_argument("--clear", action="store_true", help="Do not retain eigensystem after evaluation")
    parse.add_argument("--no_corr", action="store_true", help="Do not attempt to correct for γ-mode integration error")
    parse.add_argument("--recompute", action="store_true", help="Recompute eigensystem instead of reusing existing one")
    parse.add_argument("--no_max_gamma", action="store_true", help="Evaluate γ modes up to upper limit specified by $n$, instead of maximum BV frequency")
    parse.add_argument("--pi_only", action="store_true", help="Only perform computations for π subspace")

    v = parse.parse_args()

    compute_matrices(v.filename, v.degree, contrast=v.contrast,
        N_MODES=v.modes, clear=v.clear, max_γ=(not v.no_max_gamma),
        recompute_eigensystem=v.recompute, π_only=v.pi_only, corr=(not v.no_corr))
