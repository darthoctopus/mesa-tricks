from os import system, chdir, getcwd
from os.path import basename, abspath, dirname, isfile, isdir
from jinja2 import Environment, Template, FileSystemLoader
from os.path import isfile, isdir, basename

import numpy as np
from astropy import units as u, constants as c
from mesa_tricks.io import read_gyre, read_freqs
from mesa_tricks.io.eigensystem import Eigensystem
from mesa_tricks.integrals.coupling import coupling_matrices
from mesa_tricks.utils import discrete_tern_search
from mesa_tricks.utils.coupling import GammaCorrProblem
from mesa_tricks.integrals.ΔΠ import ΔΠ0_as

from truncate import truncate

WHERE_AM_I = abspath(dirname(__file__))
env = Environment(loader=FileSystemLoader(f"{WHERE_AM_I}/templates"))

NUMAX_SUN = 3090
DELTA_NU_SUN = 135
GAMMA_H = False

def pulse(input, output, degree, lower, upper, pfx='work', π_only=False,
            max_ν_γ=None, ΔΠ0=None, Δν=None, min_n_γ=5):
    if not isdir(pfx):
        system(f'mkdir "{pfx}"')
    system(f"cp '{input}' '{pfx}'")
    for job in ['π', 'mixed']:
        template = env.get_template(f'{job}.template')
        with open(f'{pfx}/{job}.in', 'w') as f:
            f.write(template.render(input=basename(input),
                degree=degree, output=output, lower=lower, upper=upper))

    template = env.get_template(f'γ.template')
    n_freq = None
    if max_ν_γ is not None:
        upper = np.maximum(upper, max_ν_γ)
    else:
        upper = 2 * upper
    if Δν is not None:
        lower -= Δν
    if ΔΠ0 is not None:
        ΔΠ = ΔΠ0 / np.sqrt(degree * (degree + 1))
        Π_min = 1/(upper / 1e6)
        Π_max = max(1/(lower / 1e6), Π_min + min_n_γ * ΔΠ)
        lower = 1 / Π_max * 1e6

        print(1/(lower / 1e6), Π_min + min_n_γ * ΔΠ, min_n_γ)
        print(Π_max, Π_min, ΔΠ)
        n_freq = 2 * int(np.round((Π_max - Π_min) / ΔΠ)) + 5 + min_n_γ

    with open(f'{pfx}/γ.in', 'w') as f:
            f.write(template.render(input=basename(input),
                degree=degree, output=output, lower=lower, upper=upper, n_freq=n_freq))
    cwd = getcwd()
    chdir(pfx)
    system('$GYRE_DIR/bin/gyre π.in')
    if not π_only:
        system('$GYRE_DIR/bin/gyre mixed.in')
        # truncate(basename(input), trim_outer=True, retain_center=True)
        system('$GYRE_DIR/bin/gyre γ.in')
    chdir(cwd)

def compute_eigensystem(fname, l, N_MODES=10, max_γ=False, **kwargs):
    outdir = fname.split('.data.')[0] + f"-{l:d}"

    info, gyre = read_gyre(fname)
    Teff = np.power(info['L'] / (4 * np.pi * info['R']**2 * c.sigma_sb).cgs.value, 1/4)
    numax = NUMAX_SUN * info['M'] / c.M_sun.cgs.value / (info['R'] / c.R_sun.cgs.value)**2 / np.sqrt(Teff/5777)
    delta_nu = DELTA_NU_SUN * np.sqrt(info['M'] / c.M_sun.cgs.value / (info['R'] / c.R_sun.cgs.value)**3)

    lower = max(numax - N_MODES * delta_nu, delta_nu)
    upper = numax + N_MODES * delta_nu

    if max_γ:
        max_ν_γ = np.sqrt(np.max(gyre['N2'][gyre['r']/info['R'] < 0.9])) / (2 * np.pi) * 1e6
    else:
        max_ν_γ = None

    ΔΠ0 = ΔΠ0_as(gyre, (gyre['N2'] > 0) & (gyre['r'] < 0.9 * info['R']))
    pulse(fname, "freqs.dat", l, lower, upper, pfx=outdir,
          max_ν_γ=max_ν_γ, ΔΠ0=ΔΠ0, Δν=delta_nu, **kwargs)

def compute_matrices(file, l, x_atm=0.9, clear=True, corr=True,
                     recompute_eigensystem=False, **kwargs):

    # 0. read GYRE file

    info, gyre = read_gyre(file)

    outname = file.split('.data.')[0] + f"-{l:d}"

    # 1. compute π and γ eigensystems
    if recompute_eigensystem or not isdir(outname):
        compute_eigensystem(file, l, **kwargs) 

    # 2. set up eigensystem objects
    try:
        π = Eigensystem(f"{outname}/π*.txt")
        try:
            γ = Eigensystem(f"{outname}/γ*.txt", gamma=GAMMA_H)
        except IndexError:
            γ = None
    except IndexError:
        print("Cannot find eigensystem")
        pass
    else:
        res = coupling_matrices(info, gyre, l, π, γ, x_atm=x_atm)

        # modes files

        modes_π = read_freqs(f"{outname}/freqs.dat-π")
        try:
            modes_γ = read_freqs(f"{outname}/freqs.dat-γ")
            modes_mixed = read_freqs(f"{outname}/freqs.dat")
        except FileNotFoundError:
            modes_γ = None
            modes_mixed = None

        if (γ is not None) and corr:
            # we now attempt to construct an "optimal" finite approximation to the
            # (infinite) coupling matrix. If we retained all g-modes up to n_g = 1, then
            # we will underestimate the p-dominated mixed mode frequencies, while if we
            # used only g-mode frequencies up to max(n_π), we will overestimate the p-dominated
            # mixed mode frequencies. We will choose to truncate the matrix from above by removing
            # N modes where N minimises the sum of squared errors.

            try:
                N_π = len(modes_π['ν'])
                A = res['A']
                ω2_γ = -np.diag(A[N_π:, N_π:])
                ν_γ = modes_γ['ν']
                for i, (_, ν) in enumerate(zip(ω2_γ, ν_γ)):
                    if np.isnan(_):
                        A[N_π+i, N_π+i] = -(2 * np.pi * ν / 1e6)**2
                prob = GammaCorrProblem(A, res['D'], len(π.ξ), modes_mixed['ν'])
                B, ν_γ, j = prob(debug=True)
                prob.ζ_weight = -1

                if np.max(modes_γ['ν']) > np.max(modes_π['ν']):
                    def cost(n):
                        s = slice(None, -n)
                        prob.A = B[s, s]
                        prob.D = res['D'][s,s]
                        try:
                            r = prob.residuals(ν_γ[s])
                            return np.sum(r**2) / (len(r) - len(j['x']) - 1)
                        except (ValueError, np.linalg.LinAlgError):
                            return np.inf

                    # max number of modes to truncate

                    nmax = sum(modes_γ['ν'] > np.max(modes_π['ν']))
                    nn = np.arange(nmax)
                    n0 = discrete_tern_search(cost, nn)
                else:
                    n0 = 0

            except ValueError:
                B = None
                n0 = None
        else:
            B = None
            n0 = None

        # save these matrices
        np.save(f"{outname}.npy", {**res,
            'B': B, 'n0': n0,
            'modes': modes_mixed, 'π': modes_π, 'γ': modes_γ})

    # delete eigensystem
    if clear:
        system(f"rm -rf {outname}")

from glob import glob
from tqdm.auto import tqdm
from mesa_tricks.utils import tqdm_parallel_map

if __name__ == '__main__':
    import argparse

    parse = argparse.ArgumentParser(description="Compute π/γ mode coupling matrices given a GYRE input file")

    parse.add_argument("filename")
    parse.add_argument("--degree", type=int, default=1, help="Mode degree $l$ (default: 1)")
    parse.add_argument("--modes", type=int, default=5, help="$n$, where we evaluate the nearest $2n+1$ p-modes to nu_max (default: $n=5$)")
    parse.add_argument("--min_n_gamma", type=int, default=5, help="Calculate at least this many γ modes")
    parse.add_argument("--clear", action="store_true", help="Do not retain eigensystem after evaluation")
    parse.add_argument("--no_corr", action="store_true", help="Do not attempt to correct for γ-mode integration error")
    parse.add_argument("--recompute", action="store_true", help="Recompute eigensystem instead of reusing existing one")
    parse.add_argument("--no_max_gamma", action="store_true", help="Evaluate γ modes up to upper limit specified by $n$, instead of maximum BV frequency")
    parse.add_argument("--pi_only", action="store_true", help="Only perform computations for π subspace")

    v = parse.parse_args()

    compute_matrices(v.filename, v.degree,
        N_MODES=v.modes, clear=v.clear, max_γ=(not v.no_max_gamma),
        recompute_eigensystem=v.recompute, π_only=v.pi_only, corr=(not v.no_corr), min_n_γ=v.min_n_gamma)
