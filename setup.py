from setuptools import setup, find_packages

if __name__ == '__main__':
    setup(
            name='mesa_tricks',
            author='Joel Ong',
            email='joel.ong@yale.edu',
            description='MESA tricks',
            packages=find_packages(),
            install_requires=['tqdm', 'yabox', 'pandas', 'pathos']
         )
